#!/usr/bin/env python

import sys
import predators    # cats
import paramsPredators
import optparse
#from manipCat import PredatorTrapData

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--basicdata", dest="basicdata", help="Input basicdata pkl")
        p.add_option("--predatordata", dest="predatordata", help="Input predatordata pkl")
        p.add_option("--checkingdata", dest="checkingdata", help="Input checkingdata pkl")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()
print(cmdargs.basicdata)
print(cmdargs.predatordata)
print(cmdargs.checkingdata)
# an instance of the Class params.Params()
params = paramsPredators.Params()

if len(sys.argv) == 1:
    # no basicdata
    predators.main(params)

else:
    #then passed the name of the pickle file on the command line
    predators.main(params, cmdargs.basicdata, cmdargs.predatordata, cmdargs.checkingdata)



