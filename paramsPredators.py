#!/usr/bin/env python

#import sys
import numpy as np
import os


class Params(object):
    def __init__(self):
        """
        Object to set initial parameters. Default values for some parameters
        can be set here; others will be left unset as they must be set in the
        configuration file.
        """
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
#        self.ngibbs = 2500    # 3000       # number of estimates to save for each parameter
#        self.thinrate = 2   # 25         # thin rate
#        self.burnin = 110     # 15000             # burn in number of iterations
#        totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
#        self.interval = totalIterations # 20000   # totalIterations
#        self.checkpointfreq = self.interval
#                                    # array of iteration id to keep
#        self.keepseq = np.arange(self.burnin, totalIterations,
#            self.thinrate)

        ###################################################
        # name of species trap-data name: ( 'Cats', 'Ferrets', 'Hedgehogs', 'Stowea')

        self.species = 'Ferrets'

        # set path and output file names 
        self.predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
        # filename to pickle basic data from present run to be used to initiate new runs
        self.outBasicdataFname = os.path.join(self.predatorpath,'out_basicdata250' + self.species + '.pkl')
        # pickle predator data from present run to be used to initiate new runs
        self.outPredatordataFname = os.path.join(self.predatorpath,'out_predatordata250' + self.species + '.pkl')
        # pickle mcmc results for post processing in gibbsProcessing.py
        self.outGibbsFname = os.path.join(self.predatorpath,'out_gibbs250' + self.species + '.pkl')
        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.predatorpath,'out_checking250' + self.species + '.pkl')
        # text file for hi and low g0 values
        self.g0_HiLowFname = os.path.join(self.predatorpath,'g0_HiLow_' + self.species + '.txt')
        # plot legend label
        self.plotLegendName = self.species + ' removed'
        # plot name 
        self.removeRecruitPlotFname = os.path.join(self.predatorpath, 'N_remove_recruit_' + self.species + '.png')
        # Results summary table names
        self.summaryTableFname = os.path.join(self.predatorpath, 'summaryTable_' + self.species + '.txt')



        ##############
        ###############      Initial values parameters for updating
        ############################################################################
        ############################################################################
        #############################   CATS
        if self.species == 'Cats':

            ###################################################
            # Set number of MCMC iterations, thin rate and burn in
            self.ngibbs = 3000       # number of estimates to save for each parameter
            self.thinrate = 25         # thin rate
            self.burnin = 15000             # burn in number of iterations
            totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
            self.interval =  20000   # totalIterations
            self.checkpointfreq = self.interval
            # array of iteration id to keep
            self.keepseq = np.arange(self.burnin, totalIterations,
                self.thinrate)
            ###################################################

            self.maxN =  200
            self.sigma = 310.0
            self.sigma_mean = 300
            self.sigma_sd = np.sqrt(500)
            self.sigma_search_sd = 10
            self.g0_alpha = 1.1  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
            self.g0_beta = 3.4   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
            self.g0Sd = 0.075          # search parameter for g0
            self.g0Multiplier = np.array([0, -.3, .11]) 
            self.g0MultiPrior = np.array([0, 5.0])
            self.g0MultiSearch = 0.2
            self.nTopTraps = 26.0      # limit prob of capt by this number of traps

            self.ig = 250.      #/365.0*7.0
            # uniform prior between 20 and 300
            self.immDistr = 'nor' #  'uni' or 'nor'
            self.immUniform = np.array([20, 300])
            self.immSearch = 16.0
            self.imm_mean = 250         # 5  shape
            self.imm_sd = 50         # 15 scale
            ###################
            ##################      # Reproduction parameters
            self.rg = 1.50
            self.r_shape = 4.0  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
            self.r_scale = 0.5  # 2.0  #4.0        #0.1  # 4.0
            self.reproSearch = 0.7
            # latent initial number of recruits ~ uniform(10, 100)
            self.initialReproPop = 34.0
            self.IRR_priors = np.array([4, 120])
            self.initialReproSearch = 4.0

            # wrp cauchy parameters for distributing recruits
            self.rpara = np.array([np.pi*2/3, .85])                # real numbers: normally distributed
            # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
            self.mu_wrpc_Priors = np.array([3.0, 20.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
            self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    
            self.rparaSearch = np.array([.015, .015])

            # set days on which to base population growth rate
            self.reproDaysBack = np.array([62, 31])
            self.reproDays = range(365-self.reproDaysBack[0], 365-self.reproDaysBack[1])                 # 1 Sept - 30 Sept
            # set days for low and high g0 values
            self.g0LowDays = np.array([365-116, 365])         # days >= 5 Sept to 31 Dec
            self.g0HighDays = np.array([58, 160])                # March - 10 June

        ############################################################################
        ############################################################################
        #############################  FERRETS
        if self.species == 'Ferrets':

            ###################################################
            # Set number of MCMC iterations, thin rate and burn in
            self.ngibbs = 3000       # number of estimates to save for each parameter
            self.thinrate = 25         # thin rate
            self.burnin = 15000             # burn in number of iterations
            totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
            self.interval =  20000   # totalIterations
            self.checkpointfreq = self.interval
            # array of iteration id to keep
            self.keepseq = np.arange(self.burnin, totalIterations,
                self.thinrate)
            ###################################################
            self.maxN =  200
            self.sigma = 310.0
            self.sigma_mean = 310.0
            self.sigma_sd = np.sqrt(500.0)
            self.sigma_search_sd = 10.0
            self.g0_alpha = 1.1  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
            self.g0_beta = 3.4   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
            self.g0Sd = 0.03          # search parameter for g0
            self.g0Multiplier = np.array([0.0, -0.3, 0.11]) 
            self.g0MultiPrior = np.array([0.0, 5.0])
            self.g0MultiSearch = 0.075
            self.nTopTraps = 26.0      # limit prob of capt by this number of traps

            self.ig = 200.0      #/365.0*7.0
            # uniform prior between 20 and 300
            self.immDistr = 'nor' # or 'uni'
            self.immUniform = np.array([4.0, 300.0])
            self.immSearch = 7.0
            self.imm_mean = 300.         # 5  shape
            self.imm_sd = 20.0         # 15 scale
            ##################      # Reproduction parameters
            self.rg = .750
            self.r_shape = 4.0  # 3.5  # 2.875  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
            self.r_scale = 0.5  # 0.6  # 0.8  # 2.0  #4.0        #0.1  # 4.0
            self.reproSearch = 0.7
            # latent initial number of recruits ~ uniform(10, 100)
            self.initialReproPop = 50.0
            self.IRR_priors = np.array([4.0, 120.0])
            self.initialReproSearch = 1.0

            # wrp cauchy parameters for distributing recruits
            self.rpara = np.array([np.pi*2.0/3.0, 1.0])                # real numbers: normally distributed
            # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
            self.mu_wrpc_Priors = np.array([3.0, 20.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
            self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    
            self.rparaSearch = np.array([0.015, 0.015])

            # set days on which to base population growth rate
            self.reproDaysBack = np.array([122, 92])
            self.reproDays = range(365-self.reproDaysBack[0], 365-self.reproDaysBack[1])                 # 1 Sept - 30 Sept
            # set days for low and high g0 values
            self.g0LowDays = np.array([365-122, 365 - 31])          # 1 Aug to 30 Nov
            self.g0HighDays = np.array([32, 151])                   # Feb - May 

        
        #############################  HEDGEHOGS
        if self.species == 'Hedgehogs':    

            ###################################################
            # Set number of MCMC iterations, thin rate and burn in
            self.ngibbs = 2500       # number of estimates to save for each parameter
            self.thinrate = 20         # thin rate
            self.burnin = 15000             # burn in number of iterations
            totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
            self.interval =  20000   # totalIterations
            self.checkpointfreq = self.interval
            # array of iteration id to keep
            self.keepseq = np.arange(self.burnin, totalIterations,
                self.thinrate)
            ###################################################
            self.maxN =  200
            self.sigma = 141.0
            self.sigma_mean = 141.0
            self.sigma_sd = np.sqrt(15.0)
            self.sigma_search_sd = 8.0
            self.g0_alpha = 1.4  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
            self.g0_beta = 10.6   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
            self.g0Sd = 0.03          # search parameter for g0
            self.g0Multiplier = np.array([0, -.1, .3]) 
            self.g0MultiPrior = np.array([0, 5.0])
            self.g0MultiSearch = 0.06
            self.nTopTraps = 26.0      # limit prob of capt by this number of traps

            self.ig = 380.0      #/365.0*7.0
            # uniform prior between 20 and 300
            self.immDistr = 'nor'       ####'nor' # or 'uni'
            self.immUniform = np.array([4.0, 1200.0])
            self.immSearch = 7.0
            self.imm_mean = 400.0         # 5  shape
            self.imm_sd = 50.0         # 15 scale
            ###################
            ##################      # Reproduction parameters
            self.rg = 1.10
            self.r_shape = 6.0      # 4.3333       # 2.0  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
            self.r_scale = 0.2      # 0.6      # 0.8  # 2.0  #4.0        #0.1  # 4.0
            self.reproSearch = 0.7
            # latent initial number of recruits ~ uniform(10, 100)
            self.initialReproPop = 200.0
            self.IRR_priors = np.array([4.0, 120.0])
            self.initialReproSearch = 7.0

            # wrp cauchy parameters for distributing recruits
            self.rpara = np.array([.5, .1])                # real numbers: normally distributed
            # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
            self.mu_wrpc_Priors = np.array([0.0, 10.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
            self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    
            self.rparaSearch = np.array([.015, .015])

            # set days on which to base population growth rate
            self.reproDaysBack = np.array([107, 75])
            self.reproDays = range(365-self.reproDaysBack[0], 365-self.reproDaysBack[1])    # 15 Sept - 15 Oct
            # set days for low and high g0 values
            self.g0LowDays = np.array([107, 365 - 107])          # 15 apr to 15 sept
            self.g0HighDays = np.array([0, 107])                   # Jan to mid April 

        ############################################################################
        ############################################################################
        #############################  STOATS AND WEASELS
        if self.species == 'Stowea':

            ###################################################
            # Set number of MCMC iterations, thin rate and burn in
            self.ngibbs = 11       # number of estimates to save for each parameter
            self.thinrate = 1         # thin rate
            self.burnin = 0             # burn in number of iterations
            totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
            self.interval = totalIterations # 20000   # totalIterations
            self.checkpointfreq = self.interval
            # array of iteration id to keep
            self.keepseq = np.arange(self.burnin, totalIterations,
                self.thinrate)
            ###################################################
            self.maxN =  300
            self.sigma = 310.0
            self.sigma_mean = 300
            self.sigma_sd = np.sqrt(500)
            self.sigma_search_sd = 6
            self.g0_alpha = 1.1  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
            self.g0_beta = 3.4   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
            self.g0Sd = 0.01          # search parameter for g0
            self.g0Multiplier = np.array([0, -.3, .11]) 
            self.g0MultiPrior = np.array([0, 5.0])
            self.g0MultiSearch = 0.1
            self.randomG0 = 8
            self.nTopTraps = 26.0      # limit prob of capt by this number of traps

            self.ig = 75      #/365.0*7.0
            # uniform prior between 20 and 300
            self.immDistr = 'nor' # or 'uni'
            self.immSearch = 3.0
            self.immUniform = np.array([20, 300])
            self.imm_mean = 100         # 5  shape
            self.imm_sd = 50         # 15 scale
            ###################
            ##################      # Reproduction parameters
            self.rg = .650
            self.r_shape = 2.0  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
            self.r_scale = 0.8  # 2.0  #4.0        #0.1  # 4.0
            self.reproSearch = 0.5
            # latent initial number of recruits ~ uniform(10, 100)
            self.initialReproPop = 34.0
            self.IRR_priors = np.array([4, 120])
            self.initialReproSearch = 7.0

            # wrp cauchy parameters for distributing recruits
            self.rpara = np.array([np.pi*2/3, .1])                # real numbers: normally distributed
            # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
            self.mu_wrpc_Priors = np.array([3.0, 10.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
            self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    

            # set days on which to base population growth rate
            self.reproDaysBack = np.array([122, 92])
            self.reproDays = range(365-self.reproDaysBack[0], 365-self.reproDaysBack[1])                 # 1 Sept - 30 Sept
            # set days for low and high g0 values
            self.g0LowDays = np.array([365-122, 365 - 31])          # 1 Aug to 30 Nov
            self.g0HighDays = np.array([32, 151])                   # Feb - May 
        
        
        ######################################
        ######################################
        # modify  variables used for habitat model
        self.xdatDictionary = {'scaleEast' : 0, 'scaleNorth' : 1, 'scaleTuss' : 2}
        self.scaleEast = self.xdatDictionary['scaleEast']
        self.scaleNorth = self.xdatDictionary['scaleNorth']
        self.scaleTuss = self.xdatDictionary['scaleTuss']
        self.scaleCatFerrets = 0
        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.scaleEast, self.scaleNorth, self.scaleTuss], dtype = int)    
        ######################################
        ######################################



        self.b = np.array([.004, .06, -.10])
        # beta priors on habitat coefficients
        self.bPrior = 0.0
        self.bPriorSD = np.sqrt(1000)
        self.nbcov = len(self.b)
 

