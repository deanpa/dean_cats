#!/usr/bin/env python


#from cats import Gibbs # need this because of pickled results from mcmc
import params
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))




class ResultsProcessing(object):
    def __init__(self, gibbsobj, params, predatorpath):

        self.params = params
        self.predatorpath = predatorpath

        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
        self.results = np.hstack([self.gibbsobj.Ngibbs,
                                self.gibbsobj.g0gibbs,
                                self.gibbsobj.g0Multigibbs,
                                np.expand_dims(self.gibbsobj.siggibbs, 1),
                                np.expand_dims(self.gibbsobj.rgibbs, 1),
                                self.gibbsobj.rparagibbs,
                                np.expand_dims(self.gibbsobj.initialRepPopgibbs, 1), 
                                np.expand_dims(self.gibbsobj.igibbs, 1),
                                self.gibbsobj.bgibbs])
        self.npara = self.results.shape[1]
        print('g0 shp', np.shape(self.gibbsobj.g0gibbs))
        print('Ngibbs shp', np.shape(self.gibbsobj.Ngibbs))

    def getReproductivePopAllYears(self):
        """
        Get reproductive pop for all years
        """
        self.meanN = np.round(np.mean(self.gibbsobj.Ngibbs, axis = 0), 5)
        Npop = self.meanN.copy()
        self.reproPop = np.zeros(self.gibbsobj.nYear)

        ###################################################
        ## repro pop in year 0 - update this
        self.reproPop[0] = np.mean(self.gibbsobj.initialRepPopgibbs)
        ###################################################
        for i in self.gibbsobj.uYearIndx[1:]:
            nPopi = Npop[self.gibbsobj.reproPeriodIndx == i]
            rPop = np.mean(nPopi)
            self.reproPop[i] = rPop

    @staticmethod
    def dwrpcauchy(th, mu, rho):
        """
        wrapped cauchy pdf: direction is mu, and focus is rho.
        mu is real, and rho > 0
        """
        e_num = np.exp(-2*rho)
        e_denom = 2 * np.exp(-rho)
        sinh_rho = (1 - e_num) / e_denom
        cosh_rho = (1 + e_num) / e_denom
        cos_mu_th = np.cos(th - mu)
        dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
        return dwrpc

    def calcRelWrapCauchy(self):
        """
        Calc the rel cauchy value for all sessions for distributing recruits
        """
        self.meanRPara = np.round(np.mean(self.gibbsobj.rparagibbs, axis = 0), 5)
        self.relWrpCauchy = np.zeros(self.gibbsobj.nsession)
#        recruitWinMask = self.getRecruitPeriodMask()
        for i in self.gibbsobj.uYearIndx:
            daypiTmp = self.gibbsobj.daypi[self.gibbsobj.yearRecruitIndx == i]
            dc = self.dwrpcauchy(daypiTmp, self.meanRPara[0], self.meanRPara[1])
            # mask of recruit window in year i
#            windowMask = recruitWinMask[self.gibbsobj.yearRecruitIndx == i]
            reldc = dc / np.sum(dc)         #[windowMask])     #  dc.sum()
            self.relWrpCauchy[self.gibbsobj.yearRecruitIndx == i] = reldc
#        self.relWrpCauchy[recruitWinMask == 0] = 0
 

    def reproDataAllYears(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        self.meanI = np.round(np.mean(self.gibbsobj.igibbs, axis = 0), 5)
        self.getReproductivePopAllYears()
        self.meanR = np.round(np.mean(self.gibbsobj.rgibbs, axis = 0), 5)
        self.totalRecruits = np.zeros(self.gibbsobj.nsession)
        self.sessionRecruits = np.zeros(self.gibbsobj.nsession)
        self.calcRelWrapCauchy()
        for i in self.gibbsobj.uYearIndx:
            self.totalRecruits[i] = (self.reproPop[i] * self.meanR) + self.meanI
            relwc = self.relWrpCauchy[self.gibbsobj.yearRecruitIndx == i]
            sr = self.totalRecruits[i] * relwc
            self.sessionRecruits[self.gibbsobj.yearRecruitIndx == i] = sr

    def NpredAll(self):
        """
        calc Npred for all sessions
        """
        self.reproDataAllYears()
        self.nPred = np.zeros(self.npara)
        ns = self.gibbsobj.nsession
        self.nPred[0] = self.meanN[0]
        self.nPred[1 : ns] = self.meanN[0 : ns - 1] - self.gibbsobj.removeDat[0 : ns - 1]
        self.nPred[1 : ns] = self.nPred[1 : ns] + self.sessionRecruits[1 : ns]
        self.nPred[self.nPred < 0] = 0


    @staticmethod    
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)

    def getPopNames(self):
        """
        get strings to id session population size in table
        """
        nPop = np.shape(self.gibbsobj.Ngibbs)[1]    
        pop = np.arange(nPop)
        popName = 'N0'
        for i in range(1,nPop):
            aa = 'N'+str(i)
            popName = np.append(popName,aa)
        self.popName = popName.copy()

    def makeG0Table(self):
        """
        table with adjusted g0 by trap and season
        """
        meanHiLowG0 = np.mean(self.gibbsobj.g0Multigibbs, axis = 0)
        meanG0 = np.mean(self.gibbsobj.g0gibbs, axis = 0)
#        print('meanHiLow', meanHiLowG0, meanHiLowG0.shape)
#        print('meanG0', meanG0)
        g0Table = np.zeros(shape = (2, meanG0.shape[0])) 

        meanLogitg0 = logit(meanG0)
#        print('meanHiLowG0[2]', meanHiLowG0[2])
        HiG0 = inv_logit(meanHiLowG0[2] + meanLogitg0)
        LowG0 = inv_logit(meanHiLowG0[1] + meanLogitg0)
        g0Table[0] = LowG0
        g0Table[1] = HiG0
        g0Table = g0Table.transpose()
#        print('table', g0Table)
        m = np.shape(g0Table)
        g0Names = np.array(['g0_cage_f', 'g0_cage_rm','g0_con_e', 'g0_con_f', 'g0_con_rm',
                        'g0_doc150_e', 'g0_doc150_rm', 'g0_doc250_e', 'g0_doc250_f', 'g0_doc250_rm',
                        'g0_fenn_e', 'g0_fenn_rm', 'g0_timms_f', 'g0_timms_rm', 'g0_victor_f',
                        'g0_victor_rm', 'g0_Vic2_f', 'g0_Vic2_rm'])

        # create new structured array with columns of different types
        structured = np.empty((m[0],), dtype=[('Names', 'U12'), ('Low g0', np.float),
                    ('High g0', np.float)])
        # copy data over
        structured['Low g0'] = g0Table[:, 0]
        structured['High g0'] = g0Table[:, 1]
        structured['Names'] = g0Names
        np.savetxt('g0_HiLow_M1.txt', structured, fmt=['%s', '%.4f', '%.4f'],
                    header='Names Low_g0 High_g0')

    def getTrapNightsPerSession(self):
        """
        calc number of trap nights per session
        """
        self.TNSession = np.zeros(self.gibbsobj.nsession, dtype = int)
        self.trapCountSession = np.zeros(self.gibbsobj.nsession, dtype = int)
        for i in range(self.gibbsobj.nsession):
            sumTN_i = np.sum(self.gibbsobj.trapNightsAvail_2D[self.gibbsobj.trapSession == i])
            if self.gibbsobj.sessionIntervalMask[i]:
                sumTN_i = sumTN_i / 2
            self.TNSession[i] = sumTN_i
            ### get number of traps per session
            traptotal = self.gibbsobj.trapNightsAvail_2D[self.gibbsobj.trapSession == i]
            mask = traptotal > 0
            self.trapCountSession[i] = np.sum(mask)
#        print('trapcount', self.trapCountSession)
#        print('TNSession', self.TNSession)

    def makeTableFX(self):
        self.getTrapNightsPerSession()
        self.NpredAll()
        self.nPred = np.round(self.nPred, 1)
        resultTable = np.zeros(shape = (10, self.npara))
        resultTable[0:3, :] = np.round(self.quantileFX(self.results), 3)
        resultTable[3, :] = np.round(np.mean(self.results, axis = 0), 3)
        resultTable[4, :] = np.round(self.nPred,2)
        resultTable[5, :self.gibbsobj.nsession] = np.round(self.sessionRecruits,2)
        resultTable[6, :self.gibbsobj.nsession] = self.gibbsobj.removeDat 
        resultTable[7, :self.gibbsobj.nsession] = self.TNSession
        resultTable[8, :self.gibbsobj.nsession] = self.gibbsobj.sessionMonth
        resultTable[9, :self.gibbsobj.nsession] = self.gibbsobj.sessionYear
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean', 
                                    'N Pop Mod', 'Recruit', 'Removed',
                                    'TrapNight', 'Mon', 'Year'])
        otherPara = np.array(['g0_cage_f', 'g0_cage_rm','g0_con_e', 'g0_con_f', 'g0_con_rm',
                        'g0_doc150_e', 'g0_doc150_rm', 'g0_doc250_e', 'g0_doc250_f', 'g0_doc250_rm',
                        'g0_fenn_e', 'g0_fenn_rm', 'g0_timms_f', 'g0_timms_rm', 'g0_vic_f', 
                        'g0_vic_rm', 'g0_Vic2_f', 'g0_Vic2_rm', 'g0_mid', 
                        'g0_Add_low', 'g0_Add_hi', 'sigma', 'Repro', 'wrpC1', 'wrpC2', 
                        'InitRepPop', 'Ann_Imm', 'hab_East', 'hab_nor', 'hab_tuss'])
        self.names = np.append(self.popName, otherPara)
        for i in range(self.npara):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()

    def plotFX(self):
        popIndx = np.array([25, 146, 202, 290, 316, 340])
        remainIndx = np.arange(self.gibbsobj.nsession, self.npara)
        plotIndx = np.append(popIndx, remainIndx)  
        nplots = len(plotIndx)  # + 1
        cc = 0
        nfigures = np.int(np.ceil(nplots/6.0))
        ng = np.arange(self.ndat, dtype=int)
#        print(ng)
        #print nfigures, self.ncov
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            #print lastFigure
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < nplots:
                    P.plot(ng, self.results[0:self.ndat, plotIndx[cc]])
                    plotnameindx = plotIndx[cc]
                    P.title(self.names[plotnameindx])
                cc = cc + 1
#                if cc == nplots:
#                    P.subplot(2,3, j+2)
#                    P.scatter(self.meanN, self.nPred[0:self.gibbsobj.nsession])
#                    P.ylim(0, 90)
#                    P.xlim(0, 90)
            P.show(block=lastFigure)


    def plotPop_Remove(self):
        """
        plot mean pred population size, recruits, removed and TN for each year
        """
        dates = []
        minDate = datetime.date(2005, 12, 1)
        maxDate = datetime.date(2014, 12, 1)
        for year, month, day in zip(self.gibbsobj.sessionYear, 
                            self.gibbsobj.sessionMonth, self.gibbsobj.sessionDay):
            date = datetime.date(int(year), int(month), int(day))
            dates.append(date)
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(dates, self.nPred[:self.gibbsobj.nsession], label = 'Pop size', color = 'k', linewidth = 3)
        lns2 = ax.plot(dates, self.gibbsobj.removeDat, label = 'Cats removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(dates, self.sessionRecruits, label = 'New recruits', color = 'b', linewidth = 3)
        ax2 = ax.twinx()
        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', color = 'y', linewidth = 3)
        lns = lns1 + lns2 + lns3 + lns4
#        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 80])
        ax.set_xlim(minDate, maxDate)
        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        plotFname = os.path.join(self.predatorpath, 'N_removed_recruit_trapnight.png')
        P.savefig(plotFname, format='png')
        P.show()




########            Write data to file
########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float),
                    ('N_Pop_Pred', np.float), ('Mean_Recruits', np.float),
                    ('Number Removed', np.float), ('Trap nights', np.float), 
                    ('Month', np.float), ('Year', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
        structured['N_Pop_Pred'] = self.summaryTable[:, 4]
        structured['Mean_Recruits'] = self.summaryTable[:, 5]
        structured['Number Removed'] = self.summaryTable[:, 6] 
        structured['Trap nights'] = self.summaryTable[:, 7]
        structured['Month'] = self.summaryTable[:, 8]
        structured['Year'] = self.summaryTable[:, 9]
        structured['Names'] = self.names
        tableFname = os.path.join(self.predatorpath, 'summaryTable_cats_M1.txt')
        np.savetxt(tableFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f',
                    '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f' ],
                    header='Names Low_CI Median High_CI Mean N_Pop_Pred Recruits Number_removed Trapnights Month Year')

def main(params):

    # paths and data to read in
    predatorpath = os.getenv('PREDATORPROJDIR', default='.')

    inputGibbs = os.path.join(predatorpath, 'out_gibbs.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, params, predatorpath)

    resultsobj.getPopNames()

    resultsobj.makeTableFX()

    resultsobj.makeG0Table()
                    
    resultsobj.plotFX()

    resultsobj.plotPop_Remove() 
   
    resultsobj.writeToFileFX()


if __name__ == '__main__':
    p = params.Params()
    main(p)


