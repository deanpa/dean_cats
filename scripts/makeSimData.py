#!/usr/bin/env python

import os
import numpy as np
from numba import jit
import pickle
import datetime
import pandas as pd

class SimData(object):
    def __init__(self, macraesdata, outFName):
        """
        MAKE TRAP AND LOCATION DATA FOR SIMULATIONS
        """
        self.readData(macraesdata)
        self.selectData()
        self.writeData(outFName)

    def readData(self, macraesdata):
        # data associated with trap location data
        self.trapX = macraesdata.trapX
        self.trapY = macraesdata.trapY
        self.trapID = macraesdata.trapID
        self.trapBaitIndx = macraesdata.trapBaitIndx
        self.nTraps = macraesdata.nTraps 
        self.sessionYear = macraesdata.sessionYear
        self.sessionMonth = macraesdata.sessionMonth
        self.sessionDay = macraesdata.sessionDay
        self.session = macraesdata.session
        self.nsession = macraesdata.nsession
        self.trapNightsAvail = macraesdata.trapNightsAvail
        self.trapSession = macraesdata.trapSession

#        for i in range(1119):
#             print('x', self.trapX[i], 'tbt', self.trapBaitIndx[i], 
#                self.trapID[i])

#        for i in range(455):
#            print('i', i, 'mon', self.sessionMonth[i], self.sessionYear[i])


        
        print('len x', len(self.trapX), 'len mon', len(self.sessionMonth),
            'year', len(self.sessionYear), 'ntrap', self.nTraps,
            'id', len(self.trapID), 'tbID', len(self.trapBaitIndx),
            'sess', len(self.session), 'nsess', self.nsession,
            'self.trapNightsAvail', len(self.trapNightsAvail), 455*1119,
            'trapsess', len(self.trapSession), 'maxSess', np.max(self.trapSession))

    def selectData(self):
        keepSessMask = (self.trapSession >= 418) #  & (self.trapSession <= 440) 
        tnMask = self.trapNightsAvail > 0

        keepTNMask = keepSessMask & tnMask

        trapIDFull = np.tile(self.trapID, self.nsession)
        
        trapIDKeep = trapIDFull[keepTNMask]
        uTrapID = np.unique(trapIDKeep)
        print('len uID', len(uTrapID))

        idMask = np.in1d(self.trapID, uTrapID)
        self.keepID = self.trapID[idMask]
        print('len keepID', len(self.keepID))        
        self.keepX = self.trapX[idMask]
        self.keepY = self.trapY[idMask]
        self.keepTBIndx = self.trapBaitIndx[idMask]
        self.scenario = np.zeros(len(self.keepID), dtype=int)

        print('unique tbt', np.unique(self.keepTBIndx))



    def writeData(self, outFName):
        pdf = pd.DataFrame({'trapid':self.keepID, 
                            'trapbaittype': self.keepTBIndx,
                            'xmg': self.keepX,
                            'ymg': self.keepY,
                            'scenario': self.scenario})
        pdf.to_csv(outFName, sep=',', header=True, index = False)        




########            Main function
#######
def main():

    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    basicFile = os.path.join(predatorpath,'out_basicdataCatsMod2.pkl')
    fileobj = open(basicFile, 'rb')
    macraesdata = pickle.load(fileobj)
    fileobj.close()

    outFName = os.path.join(predatorpath, 'simMod2Data.csv')


    print('basicFile', basicFile)


    simdata = SimData(macraesdata, outFName)



if __name__ == '__main__':
    main()

