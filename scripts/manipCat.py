#!/usr/bin/env python

import os
import numpy as np
import pickle
import datetime
from basicsModule import PredatorTrapData

# special codes for bait type
CODE_E = 0
CODE_F = 1
CODE_RM = 2

# special codes for trap type
CODE_CAGE = 0
CODE_CON = 1
CODE_DOC150 = 2
CODE_DOC250 = 3
CODE_FENN6 = 4
CODE_SX = 1
CODE_TIMMS = 5
CODE_VICTOR = 6

# special codes for months
CODE_jan = 1
CODE_feb = 2
CODE_mar = 3
CODE_apr = 4
CODE_may = 5
CODE_jun = 6
CODE_jul = 7
CODE_aug = 8
CODE_sep = 9
CODE_oct = 10
CODE_nov = 11
CODE_dec = 12

class RawData(object):
    def __init__(self, captFname, pickletrapdata, picklecaptdata):
        """
        Object to read in cat, trap data
        """
        #################################################
        #   new capture data from patrick 7/12 to 12/14 
        #   created and pickled "mergeCaptData.py"
        # then  'c' prefix indicates new capture data from Patrick
        #################################################

        self.picklecaptdata = picklecaptdata
        self.cDay = self.picklecaptdata.cDay
        self.cMonth = self.picklecaptdata.cMonth
        self.cYear = self.picklecaptdata.cYear
        self.cTrap = self.picklecaptdata.cTrap
        self.cTTYPE = self.picklecaptdata.cTTYPE
        self.cBAIT = self.picklecaptdata.cBAIT
#        Boolean arrays
        self.cCat_Caught = self.picklecaptdata.cCat_Caught
        self.cFerret_Caught = self.picklecaptdata.cFerret_Caught
        self.cStoat_Caught = self.picklecaptdata.cStoat_Caught
        self.cWeasel_Caught = self.picklecaptdata.cWeasel_Caught
        self.cHedgehog_Caught = self.picklecaptdata.cHedgehog_Caught
#        self.cShutdown = self.picklecaptdata.cShutdown
        self.cSE_BT = self.picklecaptdata.cSE_BT
        self.cTrapAvail = self.picklecaptdata.cTrapAvail  # availability includes SE BT and non-target at .5
        self.cJulStart = self.picklecaptdata.cJulStart
        self.cJulYear = self.picklecaptdata.cJulYear
        self.c_ndat = len(self.cJulYear)
        # convert new-capture data strings of captures numbers
        self.cTrapped = self.cCat_Caught * 1
        self.cTrappedFerret = self.cFerret_Caught * 1
        self.cTrappedHedgehog = self.cHedgehog_Caught * 1
        self.cTrappedStowea = (self.cStoat_Caught * 1) + (self.cWeasel_Caught * 1)
        # Trap result for all species
        self.cTrappedAllSpp = (self.cTrapped + self.cTrappedFerret + self.cTrappedHedgehog +
                self.cTrappedStowea)

        #################################################
        # old capture data from Andy 
        # cat capture data, trap id, week, avail etc
        #################################################
        self.cat = np.genfromtxt(captFname,  delimiter=',', names=True,
            dtype=['i8', 'S10', 'i8', 'i8', 'S10', 'S10', 'i8','i8','S10', 'S10', 'S10', 'S10','S10', 'S10'])
        self.MONTH = self.cat['MONTH']
        self.DAY = self.cat['DAY']
        self.julianStart = self.cat['ord_day_from_start']
        self.julianYear = self.cat['ord_day_in_year']
        self.nFull = len(self.DAY)
        self.TTYPE = self.cat['TTYPE']
        self.BAIT = self.cat['BAIT']
        self.TRAP = self.cat['TRAP']
        self.week = np.ceil(self.julianStart / 7)        
        self.year = self.cat['YEAR']
        # cat trap data
        trappedTmp = self.cat['cat_caught']
        # convert strings of captures numbers
        self.trapped = np.zeros(self.nFull, dtype = int)
        self.trapped[trappedTmp == b'TRUE'] = 1
        # Ferret trap data
        trappedTmp = self.cat['ferret_caught']
        self.trappedFerret = np.zeros(self.nFull, dtype = int)
        self.trappedFerret[trappedTmp == b'TRUE'] = 1
        # Hedgehog trap data
        trappedTmp = self.cat['hedgehog_caught']
        self.trappedHedgehog = np.zeros(self.nFull, dtype = int)
        self.trappedHedgehog[trappedTmp == b'TRUE'] = 1
        # Stoat and Weasel trap data
        trappedTmp = self.cat['stoat_caught']
        self.trappedStowea = np.zeros(self.nFull, dtype = int)
        self.trappedStowea[trappedTmp == b'TRUE'] = 1
        trappedTmp = self.cat['weasel_caught']
        self.trappedStowea[trappedTmp == b'TRUE'] = 1
        # Trap result for all species
        self.trappedAllSpp = (self.trapped + self.trappedFerret + self.trappedHedgehog +
                self.trappedStowea)
        ##########################        
        #  nights trap is available
        tavail = self.cat['trap_available']
        self.tavail = np.zeros(self.nFull)
        self.tavail[tavail == b'TRUE'] = 1.0
#        self.tavail[self.trapped == 1] = 1.0
        self.tavail[self.trappedAllSpp > 0] = 0.5
        self.tavail[tavail == b'FALSE'] = 0.5 # because of SE, BT, NT
        ###########################################################
        # Trap data - location  : old merge with new
        # pickled in mergeTrapData.py
        ###########################################################
        self.pickletrapdata = pickletrapdata

        self.trapTRAPPre = self.pickletrapdata.trapdatTID
        self.trapXPre = self.pickletrapdata.trapDatX
        self.trapYPre = self.pickletrapdata.trapDatY
        self.nTrapsPre = len(self.trapTRAPPre)          # = 1013
#        print('self.nTrapsPre', self.nTrapsPre)

        ###########################################################
        # Run rawdata functions
        # new data functions

        self.make14DayMask()
        self.makeNewWeekData()

        self.getBaitNumeric()        
        self.getNumericMonth()

        self.getNumericTrapType()
        self.getImprovedVictors()
        self.appendOldNewData()

        ## test function to look at trap data
        self.writetrapdata()

        self.assignWeekToCaptData()
        self.getWeekInterval()
        self.removeDataNotInTrapData()
        self.getTrapBaitType()
        self.getCaptTrapid()
        self.getTrapTrapid()
        self.removeDups()
        self.dataRemoveDups()
        self.getOneEntryWeek()
        self.oneWeekVariable()
        self.getTrapBaitIndx()
        self.getSessionYearMonth()

        self.realignCaptSessData()

        ## TEST FUNCTION - NOT NEEDED FOR DATA
#        self.textCaptAlign()

    ###############################################################
    ###############################################################
    ################# Functions


    def make14DayMask(self):
        """
        for unique julDay, make mask of when have fortnight rebait
        # fortnight checks 11-12/2013 and 7-12/2014
        """
        self.uJulDay = np.arange(np.max(self.cJulStart)) + 1
        self.uJulDay = self.uJulDay.astype(int)
        self.nJulDay = len(self.uJulDay)
        self.fortnightIndx = np.zeros(self.nJulDay, dtype = int)
        # first start date of fortnight re baiting.
        self.day1_date = datetime.date(2005, 12, 1)      # start date
        Nov1_2013 = datetime.date(2013, 11, 1)
        dateDiff = Nov1_2013 - self.day1_date
        dayDiff = dateDiff.days         # difference in days
        cutNov1_2013 = dayDiff + 1     # cut day; 1 is first julian day
        # stop date of fortnight in 2013
        Jan1_2014 = datetime.date(2014, 1, 1)
        dateDiff = Jan1_2014 - self.day1_date
        dayDiff = dateDiff.days         # difference in days
        cutJan1_2014 = dayDiff + 1     # cut day
        self.Mask2013 = (self.uJulDay >= cutNov1_2013) & (self.uJulDay < cutJan1_2014)
        ### create 2014 mask
        Jul1_2014 = datetime.date(2014, 7, 1)
        dateDiff = Jul1_2014 - self.day1_date
        dayDiff = dateDiff.days         # difference in days
        cutJul1_2014 = dayDiff + 1     # cut day
        # mask
        self.Mask2014 = self.uJulDay >= cutJul1_2014
        self.fortnightMask = self.Mask2013 + self.Mask2014
        

    def makeNewWeekData(self):
        """
        # make session and date data
        # current trapping regime
        """
        self.newWeek = np.zeros(self.nJulDay, dtype = int)
#        self.fortNightIndxByUJulian = np.zeros(self.nsession, dtype = int)
        ww = 1
        for i in range(self.nJulDay):
            if self.fortnightMask[i]:
                if self.newWeek[i] == 0:
                    if (i + 14) > self.nJulDay:
                        fillWW = np.repeat(ww, (self.nJulDay - i))
                        endSeq = self.nJulDay 
                    else:
                        fillWW = np.repeat(ww, 14)
                        endSeq = i + 14
                    self.newWeek[i : endSeq] = fillWW
                    ww += 1
            else:
                if self.newWeek[i] == 0:
                    fillWW = np.repeat(ww, 7)
                    endSeq = i + 7
                    self.newWeek[i : endSeq] = fillWW
                    ww += 1
        self.uWeek = np.unique(self.newWeek)
        self.nWeek = len(self.uWeek)
#        print('len newweek', len(self.newWeek), 'newweek', self.newWeek[0:30])
#        print('newweek', self.newWeek[self.nJulDay - 100 : ])
#        print('nJulDay', self.nJulDay, 'nWeek', self.nWeek)
#        print('uWeek', self.uWeek, 'len uWeek', len(self.uWeek))
        self.nDaysInSession = np.zeros(self.nWeek, dtype = int)
        for i in range(1, self.nWeek + 1):
            dayInSess = len(self.newWeek[self.newWeek == i])
            self.nDaysInSession[i - 1] = dayInSess
            ### TESTING LOOP SHOWS DAYS IN SESS ARE CORRECT!!!
#            print('i', i, 'dayInSess', dayInSess)
#        print('sum of ndaysinsess', np.sum(self.nDaysInSession))


    def getNumericMonth(self):
        """
        # convert string months to numeric array old data - Andy
        """
        nn = len(self.MONTH)
        self.month = np.zeros(nn)                           #
        self.month[self.MONTH == b'"JANUARY"'] = CODE_jan
        self.month[self.MONTH == b'"FEBRUARY"'] = CODE_feb
        self.month[self.MONTH == b'"MARCH"'] = CODE_mar
        self.month[self.MONTH == b'"APRIL"'] = CODE_apr
        self.month[self.MONTH == b'"MAY"'] = CODE_may
        self.month[self.MONTH == b'"JUNE"'] = CODE_jun
        self.month[self.MONTH == b'"JULY"'] = CODE_jul
        self.month[self.MONTH == b'"AUGUST"'] = CODE_aug
        self.month[self.MONTH == b'"SEPTEMBER'] = CODE_sep
        self.month[self.MONTH == b'"OCTOBER"'] = CODE_oct
        self.month[self.MONTH == b'"NOVEMBER"'] = CODE_nov
        self.month[self.MONTH == b'"DECEMBER"'] = CODE_dec

    def getBaitNumeric(self):
        """
        get a numeric array for bait type E, F, and RM
        """
        # defaults to 0 = 'E'
        self.baittype = np.zeros(self.nFull, dtype = int)
        self.baittype[self.BAIT == b'"F"'] = CODE_F
        self.baittype[self.BAIT == b'"RM"'] = CODE_RM
#        print(self.BAIT[0:50])
#        print(self.baittype[0:50])

    def getNumericTrapType(self):
        """
        get numeric trap type for all data in old cat capture data
        """
        # convert string trap types to integers
        self.ttype = np.zeros(self.nFull, dtype = int)
        self.ttype[self.TTYPE == b'"CON"'] = CODE_CON
        self.ttype[self.TTYPE == b'"DOC150"'] = CODE_DOC150
        self.ttype[self.TTYPE == b'"DOC250"'] = CODE_DOC250
        self.ttype[self.TTYPE == b'"FENN6"'] = CODE_FENN6
        self.ttype[self.TTYPE == b'"SX"'] = CODE_SX
        self.ttype[self.TTYPE == b'"TIMMS"'] = CODE_TIMMS
        self.ttype[self.TTYPE == b'"VICTOR"'] = CODE_VICTOR
        ## Correct error in data
        sxErrorMask = (self.ttype == 1) & (self.baittype == 0)
        self.ttype[sxErrorMask] = CODE_DOC250



    def writetrapdata(self):
        """
        temp fx to write some trap type data to directory
        """
        trapid = np.empty(0, dtype = np.uint16)
#        traptype = []
#        baittype = []
        year = np.empty(0, dtype = np.uint16)
        month = np.empty(0, dtype = np.uint16)
        day = np.empty(0, dtype = np.uint16)
        ndat = len(self.year)
        for i in range(ndat):
            if (self.ttype[i] == 1) & (self.baittype[i] == 0):
                date_i = datetime.date(np.int(self.year[i]), np.int(self.month[i]), np.int(self.day[i]))
                print('trapID', self.TRAP[i], self.ttype[i], self.baittype[i], 'Date', date_i)




    def getImprovedVictors(self):
        """
        # update capt and trapping data to include improved Victors
        # instituted in Jan 2014 - data from Patrick
        """
        maskVictorData = (self.cYear == 2014) & (self.cTTYPE == 6)
        oldTrapID = self.cTrap[maskVictorData]
        uTrapID = np.unique(oldTrapID)
        newTrapID = uTrapID + 2000
        nuTrapID = len(uTrapID)
        newX = np.zeros(nuTrapID)
        newY = np.zeros(nuTrapID)
        # loop thru victor traps in 2014 to get location
        for i in range(nuTrapID):
            iTID = uTrapID[i]
            newX[i] = self.trapXPre[self.trapTRAPPre == iTID]
            newY[i] = self.trapYPre[self.trapTRAPPre == iTID]        
        # update trap data
        self.trapTRAPPre = np.append(self.trapTRAPPre, newTrapID)
        self.trapTRAPPre = self.trapTRAPPre.astype(int)
        self.trapXPre = np.append(self.trapXPre, newX)
        self.trapYPre = np.append(self.trapYPre, newY)
        # update capture data
        self.cTrap[maskVictorData] = self.cTrap[maskVictorData] + 2000
        self.cTTYPE[maskVictorData] = 7
        ntt = len(self.trapTRAPPre)
#        print('n updated traps in pats data', len(np.unique(self.cTrap)))   # 1012
#        print('nuTrapID', nuTrapID)                                         # 87
#        print('lenght self.trapTrapPre', ntt)                               # 1100
#        print('length self.trapXPre', len(self.trapXPre)) 

        
    def appendOldNewData(self):
        """
        # append old capt data with new capture data from Patrick
        """
#        self.week = np.append(self.week, self.cWeek)
#        self.week = self.week.astype(int)
        self.TRAP = np.append(self.TRAP, self.cTrap)
        self.julianStart = np.append(self.julianStart, self.cJulStart)
        self.julianYear = np.append(self.julianYear, self.cJulYear)
        self.trapped = np.append(self.trapped, self.cTrapped)
        self.trappedAllSpp = np.append(self.trappedAllSpp, self.cTrappedAllSpp)  
        self.trappedFerret = np.append(self.trappedFerret, self.cTrappedFerret)
        self.trappedHedgehog = np.append(self.trappedHedgehog, self.cTrappedHedgehog)
        self.trappedStowea = np.append(self.trappedStowea, self.cTrappedStowea)
        self.ttype = np.append(self.ttype, self.cTTYPE)
        self.baittype = np.append(self.baittype, self.cBAIT)
        self.tavail = np.append(self.tavail, self.cTrapAvail)
        self.year = np.append(self.year, self.cYear)
        self.month = np.append(self.month, self.cMonth)
        self.day = np.append(self.DAY, self.cDay)
        self.nFull = len(self.month)
        self.TRAP = self.TRAP.astype(int)
        self.uTRAP = np.unique(self.TRAP)
#        print('n total capt traps', len(self.uTRAP))                        # 1086
#        print('self.trapped', np.sum(self.trapped), len(self.trapped),
#            self.trapped[0:1000])


    def assignWeekToCaptData(self):
        """
        # use the self.newWeek template to assign each datum a week (early name for session)
        # template created above.
        """
        self.weekFull = np.zeros(self.nFull, dtype = int)
#        print(np.min(self.week), np.max(self.week))
#        print(np.min(self.newWeek), np.max(self.newWeek))
        for i in range(self.nFull):
            jul_i = self.julianStart[i]
            self.weekFull[i] = self.newWeek[self.uJulDay == jul_i]
            
        self.week = self.weekFull.copy()
        print('len ujulDay', len(self.uJulDay), 'week', self.julianStart[:100], self.julianStart[-100:], 
            self.week[:100], self.week[-100:],
            'nFull', self.nFull,
            'len week', len(self.week),
            'len newweek', len(self.newWeek),
            'min and max capt jul', np.min(self.julianStart), np.max(self.julianStart))


    def getWeekInterval(self):
        """
        get intereval for each session
        """
        weekIntervalIndx = np.zeros(self.nWeek, dtype = int)
        for i in self.uWeek:
            sumFortnight = np.sum(self.fortnightMask[self.newWeek == i])
            if sumFortnight > 0:
                weekIntervalIndx[(i-1)] = 1
#            print('sess', i, 'weekIntervalindx', weekIntervalIndx[(i-1)])

        self.sessionIntervalMask = weekIntervalIndx == 1

#        print('sessionIntervalMask', self.sessionIntervalMask[:50], self.sessionIntervalMask[-50:],
#            'len sessIntmask', len(self.sessionIntervalMask))


    def removeDataNotInTrapData(self):
        """
        remove capture data not in trap data
        """
        rmTraps = np.array([880, 1026, 1029, 1040, 1045, 1045])
        rmMask = np.in1d(self.TRAP, rmTraps)
        keepMask = rmMask == 0
        # update arrays with keepMask
        self.week = self.week[keepMask]
        self.TRAP = self.TRAP[keepMask]
        self.julianStart = self.julianStart[keepMask]
        self.julianYear = self.julianYear[keepMask]
        self.trapped = self.trapped[keepMask]
        self.trappedAllSpp = self.trappedAllSpp[keepMask] 
        self.trappedFerret = self.trappedFerret[keepMask]
        self.trappedHedgehog = self.trappedHedgehog[keepMask]
        self.trappedStowea = self.trappedStowea[keepMask]
        self.ttype = self.ttype[keepMask]
        self.baittype = self.baittype[keepMask]
        self.tavail = self.tavail[keepMask]
        self.year = self.year[keepMask]
        self.month = self.month[keepMask]
        self.day = self.day[keepMask]
        self.nFull = len(self.month)
        self.uTRAP = np.unique(self.TRAP)
        ##########
        traptmp = self.TRAP[self.TRAP < 2000]
        print('n trap in capt', len(np.unique(traptmp)))


    def getTrapBaitType(self):
        """
        get trap-bait type for all data in cat capture data
        """
        self.DSEQ = np.arange(self.nFull, dtype = int)
        self.ttypebait = np.arange(self.nFull, dtype = int)
        # cycle thru the 7 trap types to get trap-bait types
        self.utraptype = np.unique(self.ttype)      
        self.nutraptype = len(self.utraptype)
        print('utraptype', self.utraptype)
#        print('ttype', self.ttype[0:50])
        tt = 0
        for i in range(self.nutraptype):
            dseq = self.DSEQ[self.ttype == self.utraptype[i]]
            baitSamp = self.baittype[dseq]
            uBAIT = np.unique(baitSamp)
            nuBAIT = len(uBAIT)
            print('iiiiiiiii', i, 'self.utraptype[i]', self.utraptype[i])
            print('uBAIT', uBAIT)
            # cycle thru bait types in traptype i
            for j in range(nuBAIT):                
                bseq = dseq[baitSamp == uBAIT[j]]
                self.ttypebait[bseq] = tt
                tt += 1
                print('jjjjjjjjjjjjj', j)
        print('unique ttypebait', np.unique(self.ttypebait))
#        print('ttypebait[0:50]', self.ttypebait[0:50])

            
    def getCaptTrapid(self):
        """
        Get unique trap id by location and ttypebait.
        Make trapid link to the trapdat and captdat
        Will increase the number of trap id 
        """
        capt_tid = 0
        self.captTrapID = np.zeros(self.nFull, dtype = int)
        # loop thru traps in capt data - length = 1086
        for i in self.uTRAP:
            # for a given trap (or loc) the trap bait type could differ temporally
            uTBT = np.unique(self.ttypebait[self.TRAP == i])
            # cycle thru tbt for each trap
            for j in uTBT:
                dseq2 = self.DSEQ[(self.TRAP == i) & (self.ttypebait == j)]
                self.captTrapID[dseq2] = capt_tid
                capt_tid += 1
#        print('capt trap n after tbt fix', len(np.unique(self.captTrapID))) # n = 1121
#        print('self.TRAP', self.TRAP[0:50])

    def getTrapTrapid(self):
        """
        get unique trapid for trap data that corresponds to capture data (self.captTrapID)
        """
        self.uTrapid = np.unique(self.captTrapID)       # updated unique traps location combos
        self.nuTrapid = len(self.uTrapid)               # updated number of traps
        self.nuTrap = len(self.uTRAP)                   # number of original traps from data
#        print('self.nuTrap', self.nuTrap)
#        print('self.nuTrapid', self.nuTrapid)           # 1121
        # build new trapdata with following variables
        self.trapTRAP = np.empty(1, dtype = int)
        self.trapTrapid = np.empty(1, dtype = int)
        self.trapX = np.empty(1)
        self.trapY = np.empty(1)
        # test overlap of trapid data
###        OverLap = np.in1d(self.uTRAP, self.trapTRAPPre)
###        NonOverLap = 1 - OverLap
###        print('non overlap utrap in traptrapPre', np.sum(NonOverLap))
###        OverLap = np.in1d(self.trapTRAPPre, self.uTRAP)
###        NonOverLap = 1 - OverLap
###        print('non overlap traptrapPre in utrap', np.sum(NonOverLap))

        trap_tid = 0
        # cycle thru old trap ids from appended capture data
        for i in self.uTRAP:
            ########## Test for inclusion of uTRAP in trapTRAPPre
###            In = np.in1d(i, self.trapTRAPPre)
###            if In == 0:
###                sumCapt = np.sum(self.trapped[self.TRAP == i])
###                print('ERROR : not included in traptrapdata', i, 'ncapts:', sumCapt)
            # unique new tid assoc with old tid (self.TRAP)
            utid = np.unique(self.captTrapID[self.TRAP == i])        
#            if i < 15:
#                print('self.TRAP', i)
#                print('unique capttrapid', np.unique(self.captTrapID[self.TRAP == i]))
            # loop thru new trap ids in TRAP i
            for j in utid:
                if trap_tid == 0:
                    self.trapTRAP[0] = i
                    self.trapTrapid[0] = trap_tid
                    self.trapX[0] = self.trapXPre[self.trapTRAPPre == i]
                    self.trapY[0] = self.trapYPre[self.trapTRAPPre == i]
                else:
                    self.trapTRAP = np.append(self.trapTRAP,i)
                    self.trapTrapid = np.append(self.trapTrapid, trap_tid)
                    self.trapX = np.append(self.trapX, self.trapXPre[self.trapTRAPPre == i])
                    self.trapY = np.append(self.trapY, self.trapYPre[self.trapTRAPPre == i])
                trap_tid += 1
        self.nTraps = len(self.trapX)
#        print('ntraps', self.nTraps)                                        # 1121
#        print('n trapTraps after TBT fix', len(np.unique(self.trapTrapid))) # 1121
#        print('len trapTrapid', len(self.trapTrapid))                       # 1121
#        print('len unique x', len(self.trapX))                              # 1121


    def removeDups(self):
        """
        Remove duplicate entries for some days 
        """
        self.keepdat = np.ones(self.nFull, dtype = int)
        # cycle thru traps in capture data
        for i in self.uTrapid:
            tday = self.julianStart[self.captTrapID == i]
            uday = np.unique(tday)
            tseq = self.DSEQ[self.captTrapID == i]
            for j in uday:
                dseq = tseq[tday == j]
#                if j == 92 & i == 861:
#                    print('dseq', dseq)                            
                # if duplicate entries per day
                if not np.isscalar(dseq):
                    ndups = len(dseq)
                    arr1 = np.zeros(ndups, dtype = int)
                    trappedSeq = self.trappedAllSpp[dseq]
                    ntrapped = np.sum(trappedSeq)
                    if ntrapped > 0:
                        arrSeq = np.arange(ndups)
                        arrSeq = arrSeq[trappedSeq > 0]
                        arrSeq = arrSeq[0]  
                        arr1[arrSeq] = 1
                    else:
                        arr1[0] = 1
                    self.keepdat[dseq] = arr1


    def dataRemoveDups(self):
        """
        remove duplicates from relevant variables
        """
        keepmask = self.keepdat == 1
        self.week = self.week[keepmask]
        self.captTrapID = self.captTrapID[keepmask]
        self.julianStart = self.julianStart[keepmask]
        self.julianYear = self.julianYear[keepmask]
        self.trapped2 = self.trapped[keepmask]
        self.trappedFerret2 = self.trappedFerret[keepmask]
        self.trappedHedgehog2 = self.trappedHedgehog[keepmask]
        self.trappedStowea2 = self.trappedStowea[keepmask]
        self.ttypebait = self.ttypebait[keepmask]
        self.ttype = self.ttype[keepmask]
        self.tavail = self.tavail[keepmask]
        self.year = self.year[keepmask]
        self.month = self.month[keepmask]
        self.day = self.day[keepmask]

        print('len trapdat', len(self.week))




    def getOneEntryWeek(self):
        """
        get a single entry for each trapid for each week
        """
        nn = len(self.week)
        cattrap = np.zeros(nn, dtype = int)
        ferrettrap = np.zeros(nn, dtype = int)
        hhtrap = np.zeros(nn, dtype = int)
        stoweatrap = np.zeros(nn, dtype = int)
        ndays = np.zeros(nn)
        keepseq = np.zeros(nn, dtype = int)
        dSeq = np.arange(len(self.week), dtype = int)
        cc = 0
        # cycle thru trap id - 966
        for i in self.uTrapid:
            # id 1 of 
            tt = self.ttype[self.captTrapID == i]
            tt = tt[0]
            tSeq = dSeq[self.captTrapID == i]
            weekdat = self.week[self.captTrapID == i]
            uweek = np.unique(weekdat)
            for j in uweek:
                twSeq = tSeq[weekdat == j]
#                trapDataAll = self.trappedAllSpp[twSeq]                             ###### change to trappedAllSpp
#                trapData = self.trapped2[twSeq]                             ###### change to trappedAllSpp
                tavailData = self.tavail[twSeq]
                tavailWeek = np.sum(tavailData)
                # Cat data
                trapDataCats = self.trapped2[twSeq]                             ###### change to trappedAllSpp
                cattrapWeek = np.sum(trapDataCats)
                # Ferret data
                trapDataFerrets = self.trappedFerret2[twSeq] 
                ferretTrapWeek = np.sum(trapDataFerrets)
                # Hedgehog data
                trapDataHedgehogs = self.trappedHedgehog2[twSeq] 
                hedgehogTrapWeek = np.sum(trapDataHedgehogs)
                # Stoat-weasel data
                trapDataStoweas = self.trappedStowea2[twSeq] 
                stoweaTrapWeek = np.sum(trapDataStoweas)
#                # if kill trap, follow sequence
#                if (tt > CODE_CAGE) & (tt < CODE_VICTOR):
#                    # if no animals were caught 
#                    if cattrapWeek == 0:
#                        ndayWeek = tavailWeek
##                    # if animals caught, reduce availability
#                    else:
#                        ndayWeek = tavailWeek/2.0/cattrapWeek
#                # if it was a live trap, reduce availability if animals were caught
#                else:
#                    ndayWeek = tavailWeek - (.5 * cattrapWeek)
                # populate arrays for trap-week combos
                cattrap[cc] = cattrapWeek
                ferrettrap[cc] = ferretTrapWeek 
                hhtrap[cc] = hedgehogTrapWeek
                stoweatrap[cc] = stoweaTrapWeek
                ndays[cc] = tavailWeek
#                ndays[cc] = ndayWeek
                keepseq[cc] = twSeq[0]
                cc += 1
        self.cattrap = cattrap[:cc]
        self.ferretTrap = ferrettrap[:cc]
        self.hedgehogTrap = hhtrap[:cc]
        self.stoweaTrap = stoweatrap[:cc]
        self.nDays = ndays[:cc]
        self.keepseq = keepseq[:cc]

    def oneWeekVariable(self):
        """
        remove duplicates from relevant variables
        """
        self.week = self.week[self.keepseq]
        self.captTrapID = self.captTrapID[self.keepseq]
        self.julianStart = self.julianStart[self.keepseq]
        self.julianYear = self.julianYear[self.keepseq]
        self.ttypebait = self.ttypebait[self.keepseq]
        self.ttype = self.ttype[self.keepseq]
        self.year = self.year[self.keepseq]
        self.month = self.month[self.keepseq]
        self.day = self.day[self.keepseq]

    def getSessionYearMonth(self):
        """
        get month and year for each session
        """
        uweek = np.unique(self.week)
        nSess = len(uweek)
        print('uweek', nSess)
        self.sessionYear = np.zeros(nSess, dtype = int)
        self.sessionMonth = np.zeros(nSess, dtype = int)
        self.sessionDay = np.zeros(nSess, dtype = int)
        self.sessionJul = np.zeros(nSess, dtype = int)
#        # TEST ELEMENTS
#        date0 = dateMax
        for i in range(nSess):
            maxJul_i = np.int(np.max(self.uJulDay[self.newWeek == (i + 1)]))
            self.sessionJul[i] = maxJul_i
            d_j = self.day1_date + datetime.timedelta(days = maxJul_i)
            self.sessionYear[i] = d_j.year
            self.sessionMonth[i] = d_j.month
            self.sessionDay[i] = d_j.day

            # TEST DATES AND DAYS IN SESSIONS -- LOOKS GOOD HERE
#            dateDiff = d_j - date0
#            dayDiff = dateDiff.days
#            print('sess', i, 'date', d_j, 'jul', maxJul_i, 'days', dayDiff)
#            date0 = d_j
        print('len self.week', len(self.week), 'len(self.year)', len(self.year))
        print('len self.day', len(self.day), 'len sess day', len(self.sessionDay))


    def textCaptAlign(self):
        for i in np.linspace(0, 300000, 30000, dtype = int):
            sess_i = self.week[i]
            print('capt sess', self.week[i], 'capt Julstart', self.julianStart[i],
                'jul year', self.julianYear[i], 'sess jul', self.sessionJul[self.uWeek == sess_i],
                'year', self.year[i], 'mon', self.month[i], 'day', self.day[i])

    def realignCaptSessData(self):
        """
        make julian days and dates consistent between sessions and capture data
        """
        for i in np.arange(len(self.month)):
            sess_i = self.week[i]
            sessMask = self.uWeek == sess_i
            jul_i = self.sessionJul[sessMask]
            d_i = self.sessionDay[sessMask]
            m_i = self.sessionMonth[sessMask]
            y_i = self.sessionYear[sessMask]
            date_i = datetime.date(y_i, m_i, d_i)
            # make tuple of date info to extract julian day in year
            timetuple = date_i.timetuple()
            self.julianYear[i] = timetuple.tm_yday
            self.julianStart[i] = jul_i
            self.year[i] = y_i
            self.day[i] = d_i
            self.month[i] = m_i


    def getTrapBaitIndx(self):
        """
        make bait-trap type indexing array for g0 updating
        Length is number of traps (self.nTraps)
        """
        # create trapBaitIndx for applying g0
        # g0 are organised in order of unique trapbait combos
        # indices vary from 0 to nTrapBait, not trapbait numbers
        utid = np.unique(self.trapTrapid) 
        self.trapBaitIndx = np.zeros(self.nTraps, dtype = int)
        # cycle through the traps in trap data
        for i in range(self.nTraps):
            arr1 = self.ttypebait[self.captTrapID == self.trapTrapid[i]]
            self.trapBaitIndx[i] = arr1[0]             # len number of rows in trap-cell matrix
#        print('trapTrapid', self.trapTrapid)
#        print('trapBaitIndx', self.trapBaitIndx)        
#        print('cattrap', np.sum(self.cattrap))
#        print('max ndays and len', np.max(self.ndays), len(self.ndays))
#        print('unique self.ttypebait', np.unique(self.ttypebait))
#        print('len self.ttypebait', len(self.ttypebait))
#        print('len self.trapTrapid', len(self.trapTrapid))

###################################################################################
##########################################
######################  pickle structures created in mergeTrapData.py and mergeCaptData.py
class PickleCaptData(object):
    def __init__(self, rawcaptdata):
        """
        Object pickelled in merge new capture data from patrick
        """
        self.cDay = rawcaptdata.cDay
        self.cMonth = rawcaptdata.cMonth
        self.cYear = rawcaptdata.cYear
        self.cTrap = rawcaptdata.cTrap
        self.cTTYPE = rawcaptdata.cTTYPE
        self.cBAIT = rawcaptdata.cBAIT
#        Boolean arrays
        self.cCat_Caught = rawcaptdata.cCat_Caught
        self.cFerret_Caught = rawcaptdata.cFerret_Caught
        self.cStoat_Caught = rawcaptdata.cStoat_Caught
        self.cWeasel_Caught = rawcaptdata.cWeasel_Caught
        self.cHedgehog_Caught = rawcaptdata.cHedgehog_Caught
        self.cShutdown = rawcaptdata.cShutdown
        self.cSE_BT = rawcaptdata.cSE_BT
        self.cTrapAvail = rawcaptdata.cTrapAvail        # availability includes SE BT and non-target
        self.cJulStart = rawcaptdata.cJulStart
        self.cJulYear = rawcaptdata.cJulYear

class PickleTrapData(object):
    def __init__(self, rawtrapdata):
        """
        pickel object from merge trap data
        """
        self.trapdatTID = rawtrapdata.trapdatTID.copy()
        self.trapDatX = rawtrapdata.trapDatX.copy()
        self.trapDatY = rawtrapdata.trapDatY.copy()




########            Pickle results to directory
########
                
########            Main function
#######
def main():
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    predatorDatFile = os.path.join(predatorpath,'GAOS_data2.csv')

    # read in pickles from merged trap and capture data sets
    mergedTrapFile = os.path.join(predatorpath,'out_newTrapData.pkl')
    fileobj = open(mergedTrapFile, 'rb')
    pickletrapdata = pickle.load(fileobj)
    fileobj.close()

    mergedCaptFile = os.path.join(predatorpath,'out_newCaptData.pkl')
    fileobj = open(mergedCaptFile, 'rb')
    picklecaptdata = pickle.load(fileobj)
    fileobj.close()

    # initiate rawdata class and object 
    rawdata = RawData(predatorDatFile, pickletrapdata, picklecaptdata)

    predatortrapdata = PredatorTrapData(rawdata)

    # pickle basic data to be used in cats.py
    outManipdata = os.path.join(predatorpath,'out_manipdata2.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(predatortrapdata, fileobj)
#    fileobj.close()

if __name__ == '__main__':
    main()

