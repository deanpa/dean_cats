#!/usr/bin/env python

#import sys
import numpy as np
import os
import pickle
import empiricalPredator
import datetime

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ###################################################
        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 2500    # 3000       # number of estimates to save for each parameter
        self.thinrate = 10   #10         # thin rate
        self.burnin = 500     # 15000             # burn in number of iterations
        totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
        self.interval = 7500 #2500 x 3   #totalIterations    # 2000 was first interval   # totalIterations
        self.checkpointfreq = self.interval
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, totalIterations,
            self.thinrate)

        ## Run Conditions
        self.initialRun = False
        self.useCheckedData = True      # gibbs arrays not full from previous runs

        ###################################################
        ###################################################
        ###################################################
        ###################################################
        # name of species trap-data name: ( 'Cats', 'Ferrets', 'Hedgehogs', 'Stowea')

        self.species = 'Hedgehogs'

        # set path and output file names 
        self.predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
        # filename to pickle basic data from present run to be used to initiate new runs
        self.outBasicdataFname = os.path.join(self.predatorpath,'out_basicdata' + self.species + '.pkl')
        # pickle predator data from present run to be used to initiate new runs
        self.outPredatordataFname = os.path.join(self.predatorpath,'out_predatordata' + self.species + '.pkl')
        # pickle mcmc results for post processing in gibbsProcessing.py
        self.outGibbsFname = os.path.join(self.predatorpath,'out_gibbs' + self.species + '.pkl')
        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.predatorpath,'out_checking' + self.species + '.pkl')
        # text file for hi and low g0 values
        self.g0_HiLowFname = os.path.join(self.predatorpath,'g0_HiLow_' + self.species + '.txt')
        # plot legend label
        self.plotLegendName = self.species + ' removed'
        # plot name 
        self.removeRecruitPlotFname = os.path.join(self.predatorpath, 'N_remove_recruit_' + self.species + '.png')
        # Results summary table names
        self.summaryTableFname = os.path.join(self.predatorpath, 'summaryTable_' + self.species + '.txt')
        # pickle name
        self.ParameterFname = os.path.join(self.predatorpath, 'pickle' + self.species + '.pkl')




        ##############
        ###############      Initial values parameters for updating
        ############################################################################
        ############################################################################
        #############################   HEDGEHOG PARAMETERS

        ###################################################
        self.maxN =  190
        self.sigma = 141.0
        self.sigma_mean = 141.0
        self.sigma_sd = np.sqrt(800.0)
        self.sigma_search_sd = 8.0
        self.g0_alpha = 0.8972299    #0.92  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
        self.g0_beta = 8.0751     # 22.1   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
        self.g0Sd = 0.03          # search parameter for g0
        self.g0Multiplier = np.array([0, -.3, .1]) 
        self.g0MultiPrior = np.array([0, 1.5])
        self.g0MultiSearch = 0.06
        self.nTopTraps = 26      # limit prob of capt by this number of traps

        self.ig = 380.0      #/365.0*7.0
        # uniform prior between 20 and 300
        self.immDistr = 'nor'       ####'nor' # or 'uni'
        self.immUniform = np.array([4.0, 1200.0])
        self.immSearch = 7.0
        self.imm_mean = 450.0         # 5  shape
        self.imm_sd = 30.0         # 15 scale
        ###################
        ##################      # Reproduction parameters
        self.rg = 1.0
        self.r_shape = 11.0      # 4.3333       # 2.0  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
        self.r_scale = 0.1      # 0.6      # 0.8  # 2.0  #4.0        #0.1  # 4.0
        self.reproSearch = 0.7
        # latent initial number of recruits ~ uniform(10, 100)
        self.initialReproPop = 90.0
        self.IRR_priors = np.array([4.0, 120.0])
        self.initialReproSearch = 7.0

        # wrp cauchy parameters for distributing recruits
        self.rpara = np.array([.5, .1])                # real numbers: normally distributed
        # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_wrpc_Priors = np.array([0.0, 10.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
        self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    
        self.rparaSearch = np.array([.015, .015])

        # set days on which to base population growth rate
        self.reproDaysBack = np.array([107, 75])
        self.reproDays = range(365-self.reproDaysBack[0], 365-self.reproDaysBack[1])    # 15 Sept - 15 Oct
        # set days for low and high g0 values
        date_0 = datetime.date(2007, 6, 2)
        timetuple = date_0.timetuple()
        julYr_0 = timetuple.tm_yday
        date_1 = datetime.date(2007, 8, 28)
        timetuple = date_1.timetuple()
        julYr_1 = timetuple.tm_yday
        self.g0LowDays = np.array([julYr_0, julYr_1]) #158, 233 (7 jun, 21 aug)         # 15 apr to 15 sept
#        self.g0LowDays = np.array([107, 365 - 107]) #158, 233 (7 jun, 21 aug)         # 15 apr to 15 sept
        self.g0HighDays = np.array([0, 107])                   # Jan to mid April 


        ######################################
        ######################################
        # modify  variables used for habitat model
        self.xdatDictionary = {'scaleEast' : 0, 'scaleNorth' : 1, 'scaleTuss' : 2,
                            'scaleCatFerret' : 3}
        self.scaleEast = self.xdatDictionary['scaleEast']
        self.scaleNorth = self.xdatDictionary['scaleNorth']
        self.scaleTuss = self.xdatDictionary['scaleTuss']
        self.scaleCatFerret = self.xdatDictionary['scaleCatFerret']
        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.scaleEast, self.scaleNorth, self.scaleTuss], dtype = int)    
        ######################################
        ######################################

        self.b = np.array([.004, .06, -.10])
        # beta priors on habitat coefficients
        self.bPrior = 0.0
        self.bPriorSD = np.sqrt(1.0)
        self.nbcov = len(self.b)
#        # sd for [N | Npred]
#        self.sdN = .2
#        self.prior_sdN = np.array([.1, .1])         # gamma priors
 
