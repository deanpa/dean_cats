#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import datetime


class Results(object):
    def __init__(self, params, basicdata, simulate, resultsQuantile):
        """
        Class and functions to process results
        """
         ################
        # Call functions
        self.wrapperResults(params, basicdata, simulate, resultsQuantile)

        ################
    def wrapperResults(self, params, basicdata, simulate, resultsQuantile):
        """
        wrapper function to call simulate functions
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        self.simulate = simulate
        self.calcProbSuccess()
        self.calcSummaryStat(resultsQuantile)
        self.plotPop_Remove()
        self.plot_1_iter()

    def calcProbSuccess(self):
        """
        calc prob of success keeping pop below threshold
        """
        self.failMask = self.simulate.nStorage > self.params.popThreshold
        self.failArray = np.sum(self.failMask, axis = 1)
        self.successMask = self.failArray == 0
        self.probSuccess = np.sum(self.successMask) / self.params.iter
        print('prob success', self.probSuccess)
#        print('array', self.failArray) 
#        print('nstorage', self.simulate.nStorage[:2])
#        print('trapnights', self.simulate.visitStorage[:, 0], 
#            'nVisits', self.simulate.visitStorage[:, 1])



    def calcSummaryStat(self, resultsQuantile):
        """
        get mean n, removed, recruit
        """
#        self.meanN = np.mean(self.simulate.nStorage, axis = 0)
        self.meanN = np.squeeze(mquantiles(self.simulate.nStorage, axis = 0, prob=0.5))
        self.meanRemove = np.squeeze(mquantiles(self.simulate.removeStorage, 
            axis = 0, prob=0.5))               
#        self.meanRemove = np.mean(self.simulate.removeStorage, axis = 0)               
        self.meanRecruit = np.squeeze(mquantiles(self.simulate.recruitStorage, 
            axis = 0, prob=0.5))
#        self.meanRecruit = np.mean(self.simulate.recruitStorage, axis = 0)
        ## GET RESULTS QUANTILES
        allQuants = mquantiles(self.simulate.nStorage, axis = 1, 
            prob = resultsQuantile)

        summaryQuants = mquantiles(allQuants, prob = [.975, 0.5, 0.025])
        print('Results quantile', resultsQuantile, ':', summaryQuants)
        
        np.savetxt(self.params.nDatFName, self.simulate.nStorage, 
            delimiter=',', comments='')

        ## TO LOAD TO ANALYSE DATA
        ## file = open("sample.csv")
        ## numpy_array = np.loadtxt(file, delimiter=",")


        print('shp meanN', self.meanN.shape)



    def plotPop_Remove(self):
        """
        plot mean pred population size, recruits, removed and TN 
        """
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(self.basicdata.dateArray, self.meanN, label = 'Pop size', 
            color = 'k', linewidth = 3)
        lns2 = ax.plot(self.basicdata.dateArray, self.meanRemove, 
            label = self.params.species + ' removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(self.basicdata.dateArray, self.meanRecruit, 
            label = 'New recruits', color = 'b', linewidth = 3)
        P.axhline(y=self.params.popThreshold, color = 'k', linestyle = '--')
#        ax2 = ax.twinx()
#        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', 
#            color = 'y', linewidth = 3)
#        lns = lns1 + lns2 + lns3 + lns4
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        if self.params.species == 'Hedgehogs':
            minDate = datetime.date(2024, 11, 25)
            maxDate = datetime.date(2034, 12, 5)
            ax.set_ylim([0, 300])
        else:
            minDate = datetime.date(2014, 11, 25)
            maxDate = datetime.date(2024, 12, 5)
            ax.set_ylim([0, 80])

        ax.set_xlim(minDate, maxDate)
#        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
#        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        fileName = 'sim' + self.params.species + str(self.params.scenario) + '.png'
        plotFname = os.path.join(self.params.resultsPath, fileName)
        P.savefig(plotFname, format='png')
#        P.show()





    def plot_1_iter(self):
        """
        plot mean pred population size, recruits, removed and TN
        """
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(self.basicdata.dateArray, self.simulate.nStorage[0], 
            label = 'Pop size', color = 'k', linewidth = 3)
        lns2 = ax.plot(self.basicdata.dateArray, self.simulate.removeStorage[0], 
            label = self.params.species + ' removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(self.basicdata.dateArray, self.simulate.recruitStorage[0], 
            label = 'New recruits', color = 'b', linewidth = 3)
        P.axhline(y=self.params.popThreshold, color = 'k', linestyle = '--')
#        ax2 = ax.twinx()
#        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', 
#             color = 'y', linewidth = 3)
#        lns = lns1 + lns2 + lns3 + lns4
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        if self.params.species == 'Hedgehogs':
            minDate = datetime.date(2024, 11, 25)
            maxDate = datetime.date(2034, 12, 5)
            ax.set_ylim([0, 300])
        else:
            minDate = datetime.date(2014, 11, 25)
            maxDate = datetime.date(2024, 12, 5)
            ax.set_ylim([0, 80])
        ax.set_xlim(minDate, maxDate)
#        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
#        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        fileName = 'iter1' + self.params.species + str(self.params.scenario) + '.png'
        plotFname = os.path.join(self.params.resultsPath, fileName)
        P.savefig(plotFname, format='png')
#        P.show()

