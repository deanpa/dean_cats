#!/usr/bin/env python

import os
import sys
import importlib
import optparse


class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--species", dest="species", help="Set species")
        p.add_option("--scenario", dest="scenario", help="Set scenario")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()
print('Species results start', cmdargs.species)
print('Scenario results start', int(cmdargs.scenario), type(int(cmdargs.scenario)))


#####################################################
######################
##  RESULT DIRECTORY    -    USER DIRECTORY FOR RESULTS (SPECIES AND SCENARIO)
scen = cmdargs.scenario      #'1'
species = cmdargs.species                      #'Cats'
######################
#####################################################

## MAKE NAME OF PARAMS MODULE TO IMPORT
resDir = 'Scen' + scen + species
paramsMod = 'params' + species + 'Scen' + scen
print('paramsMod', paramsMod)

## APPEND SYS DIRECTORY TO IMPORT PARAMS MODULE
basepath = os.getenv('PREDATORPROJDIR', default = '.')
if basepath == '.':
    pathDir = os.path.join('/home/dean/workfolder/projects/dean_cats/scripts/simulationResults',
        resDir)
else:
    pathDir = os.path.join(basepath, 'simulationResults', resDir)
sys.path.append(pathDir)

print('path to params', pathDir)

##  IMPORT PARAMS MODULE   -   USER MODIFY MODULE NAME
scenParams = importlib.import_module(paramsMod)

## IMPORT THE MAIN MODULE TO RUN FUNCTIONS
import resultsMain

params = scenParams.Params(cmdargs.species, int(cmdargs.scenario))
#params = scenParams.Params()

resultsMain.main(params)


##
## ./resultsStart.py --species='Cats' --scenario=1






