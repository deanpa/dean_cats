#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
#import datetime
import pandas as pd



def flattenList(nestedList):
    flatList = []
    for subList in nestedList:
        for element in subList:
            flatList.append(element)
    return(flatList)
        

class Params(object):
    def __init__(self):
        self.species = ['Cats', 'Ferrets', 'Stowea', 'Hedgehogs']
        self.scenario = [1, 2, 3, 4]
        self.iter = 400
        self.resultsQuantile = 0.9
        self.basePath = os.path.join(os.getenv('PREDATORPROJDIR', 
            default = '.'), 'simulationResults')

#        self.xdatDict = {'Intercept':0, 'Scen2':1, 'Scen3':2}

        self.xdatDict = {'Intercept':0, 'Scen2':1, 'Scen3':2, 'Scen4':3, 'nVisits':4, 
            'CatsScen1':5, 'CatsScen2':6, 'CatsScen3':7, 'CatsScen4':8, 
            'FerretsScen1':9, 'FerretsScen2':10, 'FerretsScen3':11, 'FerretsScen4':12, 
            'StoweaScen1':13, 'StoweaScen2':14, 'StoweaScen3':15, 'StoweaScen4':16,
            'HedgehogsScen1':17, 'HedgehogsScen2':18, 'HedgehogsScen3':19,
            'HedgehogsScen4':20}

        # Priors
#        mask = np.array([0, 14,15,16])
#        mask = ~np.in1d(np.arange(17),[4,9,11,12,13,15,16]) 
#        mask = ~np.in1d(np.arange(9),[4]) 
        self.keepVar = np.arange(4)        #np.array([0,1,2,3,5])
#        self.keepVar = np.arange(17)[mask]        #np.array([0,1,2,3,5])

        print('keepVar', self.keepVar)

        self.npara = len(self.keepVar)


#        self.npara = 17
        self.diag = np.diagflat(np.tile(100.0, self.npara))
        self.vinvert = np.linalg.inv(self.diag)
        self.s1 = 0.1
        self.s2 = 0.1
        self.ppart = np.dot(self.vinvert, np.zeros(self.npara))
        # Initial values
        self.b = np.random.normal(0.0, 0.1, self.npara)
        self.sg = 1.0
        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 1             # number of estimates to save for each parameter
        self.thinrate = 1           # thin rate
        self.burnin = 1             # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################
        print('ngibbs', self.ngibbs)





class SimStats(object):
    def __init__(self, params):
        """
        Class and functions to process results
        """
        self.params = params
        ################
        # Call functions
        self.makeDataArray()
        self.dataWrapper()
        self.scaleSpp()
        self.writeToFile()

        ################


    def makeDataArray(self):
        nSims = len(self.params.species) * len(self.params.scenario)

        self.ndat = nSims * self.params.iter
        self.predatorPop = np.zeros(self.ndat)
        self.nVisit = np.zeros(self.ndat)

        self.quantileSummary = np.zeros((16,3))

        self.summaryQuantsName = []
        self.catQuants = []
        self.ferretQuants = []
        self.stoweaQuants = []
        self.hedgehogQuants = []
        self.sppDatArray = np.empty(self.ndat, dtype = 'U16')
        self.scenDatArray = np.empty(self.ndat, dtype = 'U8')
        ## MAKE DATA DICTIONARY
        self.xdatDict = self.params.xdatDict
#        self.xdatDict = {'Intercept':0, 'Scen2':1, 'Scen3':2, 'Scen4':3, 'nVisits':4, 'CatsScen1':5,
#            'CatsScen2':6, 'CatsScen3':7, 'CatsScen4':8, 'FerretsScen1':9, 'FerretsScen2':10,
#            'FerretsScen3':11, 'FerretsScen4':12, 'StoweaScen1':13, 'StoweaScen2':14, 'StoweaScen3':15,
#            'StoweaScen4':16}
        self.nCovar = len(self.xdatDict)
        ## MAKE EMPTY ARRAY
        self.covarDat = np.zeros((self.ndat, self.nCovar)) 
        self.covarDat[:, 0] = 1.0
        self.visitIndx = self.xdatDict['nVisits']



    def dataWrapper(self):
        sim = 0
        for spp in self.params.species:
            self.spp = spp
            for scen in self.params.scenario:

#                if self.spp == 'Hedgehogs' and scen == 3:
#                    continue
                sppScen = 'Scen' + str(scen) + spp
                self.resultPath = os.path.join(self.params.basePath, sppScen)
                self.scen = scen
                self.rowIndx = [(sim * self.params.iter), 
                    (sim * self.params.iter + self.params.iter)]
                print('sim', sim, 'spp', self.spp, 'scen', self.scen, 'rowIndx', self.rowIndx)
                ## MAKE NAME
                self.makeName()
                self.getSimulationResults(sim)
                sim += 1

        print('quantileSummary', self.quantileSummary, 'quantsNames', self.summaryQuantsName)

    def makeName(self):
        self.scenName = 'Scen' + str(self.scen)
        self.sppScenName = self.spp + self.scenName
        print('scenname', self.scenName, 'sppscen name', self.sppScenName)
        ## POPULATE COVARIATE ARRAY
        if self.scen > 1:
            colIndx = self.xdatDict[self.scenName]
            print('scen col indx', colIndx)
            self.covarDat[self.rowIndx[0]:self.rowIndx[1], colIndx] = 1
        colSppScen = self.xdatDict[self.sppScenName] 
        self.covarDat[self.rowIndx[0]:self.rowIndx[1], colSppScen] = 1 
        print('colSppScen', colSppScen)

    def getSimulationResults(self, sim):
        simFName = 'simulate' + self.spp + str(self.scen) +'.pkl'
        simulateFName = os.path.join(self.resultPath, simFName)
#        print('sim fname', simulateFName)
        fileobj = open(simulateFName, 'rb')
        simulate = pickle.load(fileobj)
        fileobj.close()
        self.simulate = simulate
        allQuants = np.squeeze(mquantiles(self.simulate.nStorage, axis = 1, 
            prob = self.params.resultsQuantile))        
        self.predatorPop[self.rowIndx[0]:self.rowIndx[1]] = allQuants
        self.nVisit[self.rowIndx[0]:self.rowIndx[1]] = self.simulate.visitStorage[:, 1]
        self.covarDat[self.rowIndx[0]:self.rowIndx[1], self.visitIndx] = self.simulate.visitStorage[:, 1]

        if self.spp == 'Hedgehogs':
            print('############## Hedgehogs ', self.spp)

        self.sppDatArray[self.rowIndx[0]:self.rowIndx[1]] = self.spp
        self.scenDatArray[self.rowIndx[0]:self.rowIndx[1]] = self.scen

        print('n and nvisit', self.predatorPop[self.rowIndx[0]:(self.rowIndx[0] + 5)],
            self.simulate.visitStorage[:5, 1])

        summaryQuants = mquantiles(allQuants, prob = [0.025, 0.5, 0.975])
        self.quantileSummary[sim] = np.round(summaryQuants, 2)
        self.summaryQuantsName.append(self.sppScenName)

        if self.spp == 'Cats':
            self.catQuants.append(allQuants)
        elif self.spp == 'Ferrets':
            self.ferretQuants.append(allQuants)
        elif self.spp == 'Stowea':
            self.stoweaQuants.append(allQuants)
        else:
            self.hedgehogQuants.append(allQuants)

    def scaleSpp(self):
        ## SCALE PRED POP WITHIN A SPECIES QUANTILE TO MEAN = 0 AND STD = 1
        ## FLATTEN THE LIST FIRST
        self.catQuantsArr = np.array(flattenList(self.catQuants))
        self.ferretQuantsArr = np.array(flattenList(self.ferretQuants))
        self.stoweaQuantsArr = np.array(flattenList(self.stoweaQuants))
        self.hedgehogQuantsArr = np.array(flattenList(self.hedgehogQuants))
        ## SCALE THE QUANTS FOR EACH SPP
        catScale = (self.catQuantsArr - np.mean(self.catQuantsArr)) / np.std(self.catQuantsArr)
        ferretScale = ((self.ferretQuantsArr - np.mean(self.ferretQuantsArr)) / 
            np.std(self.ferretQuantsArr))
        stoweaScale = ((self.stoweaQuantsArr - np.mean(self.stoweaQuantsArr)) / 
            np.std(self.stoweaQuantsArr))
        hedgehogScale = ((self.hedgehogQuantsArr - np.mean(self.hedgehogQuantsArr)) / 
            np.std(self.hedgehogQuantsArr))
        ## APPEND TOGETHER
        scaleQuants = []
        scaleQuants.append(catScale)
        scaleQuants.append(ferretScale)
        scaleQuants.append(stoweaScale)
        scaleQuants.append(hedgehogScale)
        ## FLATTEN THE LIST AND CONVERT TO NP ARRAY
        self.scaleQuants = np.array(flattenList(scaleQuants))


    def writeToFile(self):
        print('SHAPE covarDat', np.shape(self.covarDat), 
            np.shape(np.expand_dims(self.scaleQuants, 1)), 
            np.shape(np.expand_dims(self.predatorPop, 1)))
        self.covarDat = np.hstack([self.covarDat, 
                                np.expand_dims(self.scaleQuants, 1),
                                np.expand_dims(self.predatorPop, 1)])
#        self.covarDat = np.hstack([self.covarDat, np.expand_dims(self.predatorPop, 1)])
        ## MAKE NAMES FOR DATABASE
        covNames = list(self.xdatDict.keys())
        covNames.append('predPopScale')
        covNames.append('predPop')
        print('Names', covNames)

        ## CONVERT COVARIATE AND POP DATA TO DB
        self.covarDat = pd.DataFrame(self.covarDat)
        self.covarDat.columns = covNames
        self.covarDat['Species'] = self.sppDatArray
        self.covarDat['Scenario'] = self.scenDatArray
#        print('covDat',self.covarDat.iloc[:5, :7])
        simDataFName = os.path.join(self.params.basePath, 'simulationData.csv')
        print('simDataFName', simDataFName)
        self.covarDat.to_csv(simDataFName, sep=',', header=True, index_label = 'did')

        ## WRITE SUMMARY QUANTILE DATA TO DIRECTORY
        summaryCol = ['0.025 CL', 'Median', '0.975 CL']        
        self.quantileSummary = pd.DataFrame(self.quantileSummary)
        self.quantileSummary.columns = summaryCol
        self.quantileSummary.insert(loc=0, column = 'Simulation', value = self.summaryQuantsName)
#        self.quantileSummary.loc[:, 'Simulation'] = self.summaryQuantsName
        quantFName = os.path.join(self.params.basePath, 'quantileSummary.csv')
        self.quantileSummary.to_csv(quantFName, sep=',', header=True, index_label = 'did')

class AnalyseData(object):
    def __init__(self, params):
        """
        Class and functions to analyse data
        """
        self.params = params
        self.b = self.params.b
        self.sg = self.params.sg

        self.covNames = np.array(list(self.params.xdatDict.keys()))[self.params.keepVar]
#        self.covNames = np.array(list(self.params.xdatDict.keys()))


        print('covNames', self.covNames, 'type', type(self.covNames))


        ### RUN FUNCTIONS
        self.readSimResults()
###        self.mcmcFX()
###        print('#######  Finished mcmc  #######')

###        self.makeTableFX()
#        self.tracePlot()
        self.plotSummary()
###        self.probOfDiff()

    def readSimResults(self):
        simDataFName = os.path.join(self.params.basePath, 'simulationData.csv')
        self.covPredDat = pd.read_csv(simDataFName, delimiter=',')

#        self.species = np.array(self.covPredDat['Species']).flatten()

#        #### ADJUST SPECIES
#        self.covPredDat = self.covPredDat.loc[self.covPredDat['Species'] == 'Stowea']
#        ###################

        self.predPopScale = np.array(self.covPredDat['predPopScale']).flatten()
        self.predPop = self.covPredDat['predPop']

        self.nVisitPop = np.array(self.covPredDat['nVisits']).flatten()
        scaleNVisit = (self.nVisitPop - np.mean(self.nVisitPop)) / np.std(self.nVisitPop)
        self.covPredDat['nVisits'] = scaleNVisit
        self.xDat = self.covPredDat.to_numpy()


        self.nDat = len(self.predPopScale)


        self.xDat = self.xDat[:, (1 + self.params.keepVar)]
#        self.xDat = self.xDat[:, 1:-4]
        self.xDat = self.xDat.astype(float)

        print('xDat', self.xDat[:2])        
        self.y = self.predPopScale.copy()
        print('y', self.y[:5])

        self.ResultsGibbs = np.zeros([self.params.ngibbs, (self.params.npara + 1)]) 


    def bupdate(self):
        """
        This is my function
        """
        xTranspose = np.transpose(self.xDat)
        xCrossProd = np.dot(xTranspose, self.xDat)
        sinv = 1.0/self.sg
        sx = np.multiply(xCrossProd, (1./self.sg))
        xyCrossProd = np.dot(xTranspose, self.y)
        sy = np.multiply(xyCrossProd, sinv)
        bigv = np.linalg.inv(sx + self.params.vinvert)
        smallv = sy + self.params.ppart
        meanVariate = np.dot(bigv, smallv)
        b = np.random.multivariate_normal(meanVariate, bigv)
        self.b =  np.transpose(b)
#        self.params.mu = np.dot(self.basicdata.xdat, self.b)

    def vupdate(self):
        pred = np.dot(self.xDat, self.b)
#        pred = pred.transpose()
        predDiff = self.y - pred
#        mufx = self.y - pred
#        muTranspose = mufx.transpose()
#        sx = np.dot(mufx, muTranspose)
        sx = np.sum(predDiff**2.0)
        u1 = self.params.s1 + np.multiply(.5, self.nDat)
        u2 = self.params.s2 + np.multiply(.5, sx)               # rate parameter    
        isg = np.random.gamma(u1, 1./u2, size = None)            # formulation using shape and scale
        self.sg = 1.0/isg


########            Main mcmc function
########
    def mcmcFX(self):
        """
        Run gibbs sampler
        """
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

            self.vupdate()

            self.bupdate()


            if g in self.params.keepseq:
                self.ResultsGibbs[cc, 0:self.params.npara] = self.b
                self.ResultsGibbs[cc, -1] = np.sqrt(self.sg)

                cc = cc + 1

#        print('gibbs', self.ResultsGibbs[:300])



    ################
    ################ Functions to make table
    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

    def makeTableFX(self):
        """
        Function to print table of results
        """
        resultTable = np.zeros(shape = (4, (self.params.npara+1)))

        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.ResultsGibbs)

        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.ResultsGibbs)
        resultTable = np.round(resultTable.transpose(), 3)
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        tableShp = np.shape(resultTable)
        print('tableShp', tableShp, 'len names', len(self.covNames),
            'self.covNames', self.covNames)
        self.rowNames = np.append(self.covNames, 'SD')
        self.nParams = len(self.rowNames)

        for i in range(self.nParams):
            name = self.rowNames[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def tracePlot(self):
        cc = 0
        nfigures = np.int32(np.ceil(self.nParams/6.0))
        ng = np.arange(np.shape(self.ResultsGibbs)[0], dtype=int)
#        print(ng)
        #print nfigures, self.ncov
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            #print lastFigure
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < self.nParams:
                    P.plot(ng, self.ResultsGibbs[:, cc])
                    P.title(self.rowNames[cc])
                cc = cc + 1
#                if cc == nplots:
#                    P.subplot(2,3, j+2)
#                    P.scatter(self.meanN, self.nPred[0:self.gibbsobj.nsession])
#                    P.ylim(0, 90)
#                    P.xlim(0, 90)
            P.show(block=lastFigure)





    def plotSummary(self):
        x = [4, 10, 16, 22]
        deltaX = [-.75, -.25, .25, .75]
        colour = ['k', 'b', 'r', 'y']
        for i in range(2):
            P.figure(figsize = (11,9))
            ax = P.gca()
            for spp in range(len(self.params.species)):
                self.spp = self.params.species[spp]
                for scen in range(len(self.params.scenario)):
                    self.scen = self.params.scenario[scen]

 #                   if self.spp == 'Hedgehogs' and self.scen == 3:
 #                       continue


                    keepMask = ((self.covPredDat['Species'] == self.spp) &
                        (self.covPredDat['Scenario'] == self.scen))
                    scenMask = self.covPredDat['Scenario'] == self.scen

                    if i == 0:
                        popSppScen = self.predPop[keepMask]
                    else:
                        popSppScen = self.predPopScale[keepMask]


                    print('spp', self.spp, 'scen', self.scen, popSppScen[:10])



                    meanPop = np.mean(popSppScen)
                    sdPop = np.std(popSppScen)
                    meanVisit = np.int32(np.round(np.mean(self.nVisitPop[scenMask]), 0))

                    print('spp', self.spp, 'scen', scen+1, 'x', x[spp] + deltaX[scen],
                        'pop', meanPop)

                    if spp == 0:
                        scenLabel = 'Scen ' + str(scen + 1) + ', mean visits ' + str(meanVisit)
                        lns1 = ax.errorbar((x[spp] + deltaX[scen]), meanPop, yerr = sdPop,
                            ecolor = colour[scen], color = colour[scen], ms = 5, label = scenLabel,
                            capsize = 4, marker = 'o', mfc = colour[scen], mec = colour[scen],
                            ls = '')

                    else:
                        lns1 = ax.errorbar((x[spp] + deltaX[scen]), meanPop, yerr = sdPop,
                            ecolor = colour[scen], color = colour[scen], ms = 5,
                            capsize = 4, marker = 'o', mfc = colour[scen], mec = colour[scen],
                            ls = '')
            if i == 0:
                ax.set_ylabel('90% Percentile of population', fontsize = 16)
                plotFName = os.path.join(self.params.basePath, 'Pop_90_Percentile.png')
#                plotFName = os.path.join(self.params.basePath, 'Pop_90_Percentile.eps')
            else:
                ax.set_ylabel('90% Percentile of scaled population', fontsize = 16)
                plotFName = os.path.join(self.params.basePath, 'scaled_Pop_90_Percentile.png')
#                plotFName = os.path.join(self.params.basePath, 'scaled_Pop_90_Percentile.eps')
#            ax.set_xlabel('Species')
            ax.set_xticks(x, self.params.species)       #, fontsize = 16)
            ax.set_xticklabels(self.params.species, fontsize=16)
            if i == 0:
                ax.legend(loc = 'upper left', fontsize = 12)
            else:
                ax.legend(loc = 'upper left', fontsize = 12)
            P.savefig(plotFName, format='png', dpi = 600)
#            P.savefig(plotFName, format='eps', dpi = 600)
            P.show()

    def probOfDiff(self):
        """
        CALC PROB OF DIFF IN POSTERIOR PRED OF RESID POP
        SEE MAKOWSKI 2019A AND B: "PROB OF DIRECTION" OR "MAXIMUM PROB OF EFFECT"
        """
        popScen2 = self.ResultsGibbs[:, 0] + self.ResultsGibbs[:, 1]     #self.predPopScale[self.scen3 == 1]
        popScen3 = self.ResultsGibbs[:, 0] + self.ResultsGibbs[:, 2]     #self.predPopScale[self.scen3 == 1]
        popScen4 = self.ResultsGibbs[:, 0] + self.ResultsGibbs[:, 3]
        maxScen3 = np.max(popScen3)
        s4GTs3 = popScen4 >= maxScen3
        probDiff3_4 = np.sum(s4GTs3) / self.params.ngibbs
#        probDiff3_4 = len(popScen4[popScen4 >= maxScen3]) / self.params.ngibbs
        print('probDiff of Scen 3 and 4: ', probDiff3_4)
        print('quants scen 2', mquantiles(popScen2, prob=[0.025, 0.5, 0.975]))
        print('quants scen 3', mquantiles(popScen3, prob=[0.025, 0.5, 0.975]))
        print('quants scen 4', mquantiles(popScen4, prob=[0.025, 0.5, 0.975]))
        print('max scen3', maxScen3, 'max scen4', np.max(popScen4))
        print('###############################################')
#        self.scen3 = np.array(self.covPredDat['Scen3']).flatten()
#        self.scen4 = np.array(self.covPredDat['Scen4']).flatten()

#        maxS3 = np.max(self.scen3)
#        diffMask = self.scen4 >= maxS3
#        pD3_4 = np.sum(diffMask) / self.params.ngibbs
#        print('raw sim Results prob', pD3_4)
#        print('raw quants 3', mquantiles(self.scen3, prob=[0.025, 0.5, 0.975]))
#        print('raw quants 4', mquantiles(self.scen4, prob=[0.025, 0.5, 0.975]))
        nRand = 40000
        rand3 = np.random.choice(popScen3, size=nRand, replace=True)
        rand4 = np.random.choice(popScen4, size=nRand, replace=True)
        randDiff = rand4 - rand3
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('prob Rand diff 3,4', pRandDiff)
        print('############# Diff 2, 3  ##########################')
        rand3 = np.random.choice(popScen3, size=nRand, replace=True)
        rand2 = np.random.choice(popScen2, size=nRand, replace=True)
        randDiff = rand3 - rand2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('prob Rand diff 2, 3', pRandDiff)


        print('##########   Raw Sim Test    #####################################')
        self.scen2 = np.array(self.covPredDat['Scen2']).flatten()
        self.scen3 = np.array(self.covPredDat['Scen3']).flatten()
        self.scen4 = np.array(self.covPredDat['Scen4']).flatten()
        self.scen1mask = (self.scen2 + self.scen3 + self.scen4) == 0 

        popS1 = self.predPopScale[self.scen1mask]
        popS2 = self.predPopScale[self.scen2 == 1]
        popS3 = self.predPopScale[self.scen3 == 1]
        popS4 = self.predPopScale[self.scen4 == 1]

        r1 = np.random.choice(popS1, size=nRand, replace=True)
        r2 = np.random.choice(popS2, size=nRand, replace=True)
        r3 = np.random.choice(popS3, size=nRand, replace=True)
        r4 = np.random.choice(popS4, size=nRand, replace=True)
        randDiff = r4 - r3
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('Raw Sim Test prob Rand diff 3,4', pRandDiff)

        randDiff = r3 - r2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('Raw Sim Test prob Rand diff 3,2', pRandDiff)

        randDiff = r4 - r2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('Raw Sim Test prob Rand diff 4 ,2', pRandDiff)

        randDiff = r1 - r4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('Raw Sim Test prob Rand diff 1, 4', pRandDiff)

        randDiff = r1 - r2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('Raw Sim Test prob Rand diff 1, 2', pRandDiff)

        print('############ cats raw    ############')
        cs1 = np.array(self.covPredDat['CatsScen1']).flatten()
        cs2 = np.array(self.covPredDat['CatsScen2']).flatten()
        cs3 = np.array(self.covPredDat['CatsScen3']).flatten()
        cs4 = np.array(self.covPredDat['CatsScen4']).flatten()


        c1 = self.predPopScale[cs1 == 1]
        c2 = self.predPopScale[cs2 == 1]
        c3 = self.predPopScale[cs3 == 1]
        c4 = self.predPopScale[cs4 == 1]

        cr1 = np.random.choice(c1, size=nRand, replace=True)
        cr2 = np.random.choice(c2, size=nRand, replace=True)
        cr3 = np.random.choice(c3, size=nRand, replace=True)
        cr4 = np.random.choice(c4, size=nRand, replace=True)

        randDiff = cr1 - cr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('cat Sim Test prob Rand diff 1, 2', pRandDiff)

        randDiff = cr1 - cr3
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('cat Sim Test prob Rand diff 1, 3', pRandDiff)

        randDiff = cr1 - cr4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('cat Sim Test prob Rand diff 1, 4', pRandDiff)

        randDiff = cr4 - cr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('cat Sim Test prob Rand diff 4, 2', pRandDiff)

        randDiff = cr3 - cr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('cat Sim Test prob Rand diff 3, 2', pRandDiff)

        randDiff = cr3 - cr4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('cat Sim Test prob Rand diff 3, 4', pRandDiff)

        print('############ ferrets raw    ############')
        fs1 = np.array(self.covPredDat['FerretsScen1']).flatten()
        fs2 = np.array(self.covPredDat['FerretsScen2']).flatten()
        fs3 = np.array(self.covPredDat['FerretsScen3']).flatten()
        fs4 = np.array(self.covPredDat['FerretsScen4']).flatten()


        f1 = self.predPopScale[fs1 == 1]
        f2 = self.predPopScale[fs2 == 1]
        f3 = self.predPopScale[fs3 == 1]
        f4 = self.predPopScale[fs4 == 1]

        fr1 = np.random.choice(f1, size=nRand, replace=True)
        fr2 = np.random.choice(f2, size=nRand, replace=True)
        fr3 = np.random.choice(f3, size=nRand, replace=True)
        fr4 = np.random.choice(f4, size=nRand, replace=True)

        randDiff = fr1 - fr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('ferret Sim Test prob Rand diff 1, 2', pRandDiff)

        randDiff = fr1 - fr3
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('ferret Sim Test prob Rand diff 1, 3', pRandDiff)

        randDiff = fr1 - fr4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('ferret Sim Test prob Rand diff 1, 4', pRandDiff)

        randDiff = fr4 - fr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('ferret Sim Test prob Rand diff 4, 2', pRandDiff)

        randDiff = fr3 - fr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('ferret Sim Test prob Rand diff 3, 2', pRandDiff)

        randDiff = fr3 - fr4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('ferret Sim Test prob Rand diff 3, 4', pRandDiff)



        print('############ stowea raw    ############')
        ss1 = np.array(self.covPredDat['StoweaScen1']).flatten()
        ss2 = np.array(self.covPredDat['StoweaScen2']).flatten()
        ss3 = np.array(self.covPredDat['StoweaScen3']).flatten()
        ss4 = np.array(self.covPredDat['StoweaScen4']).flatten()


        s1 = self.predPopScale[ss1 == 1]
        s2 = self.predPopScale[ss2 == 1]
        s3 = self.predPopScale[ss3 == 1]
        s4 = self.predPopScale[ss4 == 1]

        sr1 = np.random.choice(s1, size=nRand, replace=True)
        sr2 = np.random.choice(s2, size=nRand, replace=True)
        sr3 = np.random.choice(s3, size=nRand, replace=True)
        sr4 = np.random.choice(s4, size=nRand, replace=True)

        randDiff = sr1 - sr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('stowea Sim Test prob Rand diff 1, 2', pRandDiff)

        randDiff = sr1 - sr3
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('stowea Sim Test prob Rand diff 1, 3', pRandDiff)

        randDiff = sr1 - sr4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('stowea Sim Test prob Rand diff 1, 4', pRandDiff)

        randDiff = sr4 - sr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('stowea Sim Test prob Rand diff 4, 2', pRandDiff)

        randDiff = sr3 - sr2
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('stowea Sim Test prob Rand diff 3, 2', pRandDiff)

        randDiff = sr3 - sr4
        pRandDiff = np.sum(randDiff >= 0.0) / nRand
        print('stowea Sim Test prob Rand diff 3, 4', pRandDiff)




########            Main function
#######
def main():

    firstRun = True         ## True or False

    params = Params()

    if firstRun:
        simstats = SimStats(params)
    analysedata = AnalyseData(params)

if __name__ == '__main__':
    main()

