#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle
import datetime
import shutil
import simulationFX

########            Main function
#######
def main(params):

    fileobj = open(params.inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    ## COPY PARAMS FILE TO RESULTS DIRECTORY FOR REFERENCE
    shutil.copy(params.paramsStartFName, params.paramsResultsFName)

    basicdata = simulationFX.BasicData(params, gibbsobj)

    simulate = simulationFX.Simulate(params, basicdata)

    ## pickle basic data from present run to be used to initiate new runs
    fileobj = open(params.basicdataFName, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    ## pickle mcmc results for post processing in postProcessing.py
    fileobj = open(params.simulateFName, 'wb')
    pickle.dump(simulate, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()

