#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import datetime

def logit(x):
    """
    logit function
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    inverse logit function
    """
    return np.exp(x) / (1 + np.exp(x))

def thProbFX(tt):
    """
    multinomial probability
    """
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def matrixsub(arr1, arr2):
    """
    looping sub-function to calculate distance matrix among many points
    """
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    """
    distance matrix calculation
    """
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc


def calcRelWrapCauchy(wrp_rpara, nsession, uYearIndx, daypi, yearRecruitIndx):
    """
    Calc the rel cauchy value for all sessions for distributing recruits
    """
    relWrpCauchy = np.zeros(nsession)
    for i in uYearIndx:
        # day pi in year i
        daypiTmp = daypi[yearRecruitIndx == i]
        # pdf of daypi in year i
        dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
        # mask of recruit window in year i
        reldc = dc / np.sum(dc)     #  dc.sum()
        relWrpCauchy[yearRecruitIndx == i] = reldc
    return relWrpCauchy

def g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll):
    logit_g0 = logit(g01)
    g0All_3 = g0All.copy()
    for k in range(nsession):
        sessmask = trapSession == k                            # sess mask for trap data
        # get logit g0 for that bait-trap by season effect
        logit_g0All_1 = logit_g0 + g0MultiplierAll[k]
        g0All_2 = inv_logit(logit_g0All_1)
        g0All_3[sessmask] = g0All_2
    return g0All_3

def g0AllFX(g0_2D, g0All, trapBaitIndx, nsession, trapSession, g0MultiplierAll):
    """
    make a long array of g0 values associated with trap data for each bait trap type
    and repeat for each session, with season multiplier
    """
    # expand by trap-bait type to length of self.nTraps
    g01 = g0_2D[trapBaitIndx]
    # loop thru sessions
    g0All_3 = g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll)
    return g0All_3

def g0AllFX_ChangeTraps(g0_2D, g0All, trapBaitIndx, nsession, trapSession, g0MultiplierAll):
    """
    make a long array of g0 values associated with trap data for each bait trap type
    and repeat for each session, with season multiplier
    """
    g0All_3 = g0All.copy()
    for w in range(nsession):
        sessmask = trapSession == w
        trapBaitIndxSession = trapBaitIndx[sessmask]

#        print('Length trapBaitIndxSession', np.shape(trapBaitIndxSession),
#            'len g0_2D', np.shape(g0_2D),
#            'unique g0_2D', np.unique(g0_2D),
#            'unique trapBaitIndxSession', np.unique(trapBaitIndxSession))
        

        # expand by trap-bait type to length of self.nTraps
        g01 = g0_2D[trapBaitIndxSession]
        logit_g0 = logit(g01)
        # get logit g0 for that bait-trap by season effect
        logit_g0All_1 = logit_g0 + g0MultiplierAll[w]
        g0All_2 = inv_logit(logit_g0All_1)
        g0All_3[sessmask] = g0All_2
#        if w == 0:
#            print('g0', g0_2D, 'tbt', trapBaitIndxSession[0:10], 'g01', g01[0:10])
            

    return g0All_3


##############################################################
class BasicData(object):
    def __init__(self, params, gibbsobj):
        """
        Object to read in data and run simulation
        """
        self.params = params
        ###################
        # Run Functions
        self.readDataGibbs(gibbsobj)
        self.makeCovariateArray()
        self.makeSessionArrays()

        ## FOR SCENARIO 1
        if (self.params.scenario == 1):
            (self.trapSession, self.trapTrapID, self.trapTrapNights, 
                self.trapAvail, self.trapTBT, self.trapMonthSession, self.JulyDecMask, 
                self.fortnightMask, self.nTrapSession, self.seqTrapSession, self.g0All, 
                self.trapLiveMask, self.uTrapTBT, self.nUTrapTBT,
                self.trapYearSession) = (self.trapDatScenario(self.ntraps, 
                     self.trapidContin, self.trapbaittype))
            

            ## COUNT TRAPS NIGHTS AND VISITS IN ITERATION I
            self.countTrapChecks(self.trapLiveMask, self.fortnightMask, 
                self.trapTrapNights, self.trapYearSession)



        if self.params.scenario > 1:
            self.readNorthData()

        self.makeReproMask()
        self.makeYearRecruitIndx()
        self.distributeRecruitMasks()
        self.make_g0_Indx()


    ####################################
    # Function definitions
    def readDataGibbs(self, gibbsobj):
        """
        read in trap data and gibbs results from mcmc
        """
        # get parameters from mcmc gibbs results
        self.getGibbsArrays(gibbsobj)
        ###########################################################
        # Trap data - location
        self.trapdat = np.genfromtxt(self.params.simDatFile, delimiter=',', names=True,
            dtype=['i8', 'i8', 'f8', 'f8', 'i8'])
        self.trapid = self.trapdat['trapid']
        # X and Y location of trap data
        self.trapX = self.trapdat['xmg']
        self.trapY = self.trapdat['ymg']
        self.trapbaittype = self.trapdat['trapbaittype']
        self.trapbaittype[self.trapbaittype == 16] = 14
        self.trapbaittype[self.trapbaittype == 15] = 13
        self.utrapbaittype = np.unique(self.trapbaittype)
        self.nutrapbaittype = len(self.utrapbaittype)
#        self.scenario = self.trapdat['scenario']    # 0 = current, 1 = theoretical
        self.ntraps = len(self.trapid)
        self.trapidContin = np.arange(self.ntraps)
        print('unique sim tbt', np.unique(self.trapbaittype), len(np.unique(self.trapbaittype)))

        ################################################          Grid-cell covariate data
        # covariate data by 1km grid cells.
        self.covDat = np.genfromtxt(self.params.covDatFile, delimiter=',', names=True,
            dtype=['f8', 'f8', 'f8'])
        self.cellX = self.covDat['x']
        self.cellY = self.covDat['y']
        self.eastCov = self.cellX - np.min(self.cellX)
        self.northCov = self.cellY - np.min(self.cellY)
        self.tuss = self.covDat['hab']
        self.ncell = len(self.eastCov)

    def readNorthData(self):
        # Scenario with traps added in North and west
        print('northDatFile', self.params.northDatFile)

        self.northTraps = np.genfromtxt(self.params.northDatFile, delimiter=',', 
            names=True, dtype=['S10', 'f8', 'f8', 'f8']) # tid, xmg, ymg, tbt
        self.northX = self.northTraps['x']
        self.northY = self.northTraps['y']
        self.nNorthTraps = len(self.northX)

    def getGibbsArrays(self, gibbsobj):
        """
        get parameter values for each iteration
        """
        self.ndat = len(gibbsobj.siggibbs)
        self.sampID = np.random.choice(range(self.ndat), self.params.iter, replace = True)
        # get starting N - Nov 2014
        nn = np.shape(gibbsobj.Ngibbs)
        self.NStart = gibbsobj.Ngibbs[self.sampID, -1]
        self.bgibbs = gibbsobj.bgibbs[self.sampID]
        self.rgibbs = gibbsobj.rgibbs[self.sampID]
        self.igibbs = gibbsobj.igibbs[self.sampID]
        self.g0gibbs = gibbsobj.g0gibbs[self.sampID, :-2]          # leave out improved victors
        self.siggibbs = gibbsobj.siggibbs[self.sampID]
        self.rparagibbs = gibbsobj.rparagibbs[self.sampID]               # wrapped cauchy
        self.g0Multigibbs = gibbsobj.g0Multigibbs[self.sampID]           # seasonal g0

    def makeCovariateArray(self):
        """
        use covariate data to make 2-d array
        """
        # Covariates for habitat model
        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)
        self.scaleTuss = (self.tuss - np.mean(self.tuss)) / np.std(self.tuss)
#        self.scaleCatFerrets = np.zeros(self.ncell)
        # stack and make covariate array
        self.xdat = np.hstack([np.expand_dims(self.scaleEast, 1),
                            np.expand_dims(self.scaleNorth, 1),
                            np.expand_dims(self.scaleTuss, 1)])
        if self.params.species == 'Stowea':
            self.catFerretPara = np.array([0.016, 0.075, -0.08, 0.025, 0.069, -0.133])
            predXDat = np.hstack([self.xdat, self.xdat])
            xTmp = np.dot(predXDat, self.catFerretPara)
            xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
            self.xdat = np.hstack([self.xdat, np.expand_dims(xTmp, 1)]) 
        # cell id and dist trap to cell  
        self.cellID = np.arange(0, self.ncell, dtype = int)

    def makeSessionArrays(self):
        """
        make basic dates and julian dates for simulations
        """
        self.dateArray = []
        self.julStart = np.zeros(1, dtype = int)                           # initiate array
        self.dayInterval = np.array([14])  # first interval for December
        firstDate = self.params.startDate + datetime.timedelta(days=14)
        self.dateArray.append(firstDate)        # first date entry
        timetuple = firstDate.timetuple()
        self.julYear= timetuple.tm_yday         # first julian year entry
        self.yearSession = timetuple.tm_year
        self.monthSession = timetuple.tm_mon
        nextdate = firstDate + datetime.timedelta(days=14)
        dayInterval = 14
        cc = 1
        # while loop to populate arrays
        while nextdate < self.params.endDate:
            self.dateArray.append(nextdate)
            timetuple = nextdate.timetuple()
            julYearCC = timetuple.tm_yday
            self.julYear = np.append(self.julYear, julYearCC)
            datediff = nextdate - self.dateArray[0]
            dayDiff = datediff.days         # difference in days
            self.julStart = np.append(self.julStart, dayDiff)
            self.dayInterval = np.append(self.dayInterval, dayInterval)
            # get year and month
            self.yearSession = np.append(self.yearSession, timetuple.tm_year)
            self.monthSession = np.append(self.monthSession, timetuple.tm_mon)
            # get day interval for correct part of year
            if (julYearCC < 183):
                dayInterval = 7
            else:
                dayInterval = 14
            nextdate = self.dateArray[cc] + datetime.timedelta(days = dayInterval)
            cc += 1
        # days in radians
        self.daypi = self.julYear / 365 * 2 * np.pi
        self.nsession = len(self.julStart)
        self.session = np.arange(self.nsession)
        self.uYear = np.unique(self.yearSession)

#        print('mon sess', self.monthSession[:120], 'len year sess', 
#            self.yearSession[:120])


    def trapDatScenario(self, ntraps, trapidContin, trapbaittype):
        """
        trap data unique for each scenario
        """
        trapSession = np.repeat(self.session, ntraps)
        trapTrapID = np.tile(trapidContin, self.nsession)
        trapTrapNights = np.repeat(self.dayInterval, ntraps)
        trapAvail = np.ones(self.nsession * ntraps)
        trapTBT = np.tile(trapbaittype, self.nsession)
        trapMonthSession = np.repeat(self.monthSession, ntraps)


        trapYearSession = np.repeat(self.yearSession, ntraps)

        julyDecMask = trapMonthSession > 6
        fortnightMask = trapMonthSession > 6
        nTrapSession = len(trapSession)
        seqTrapSession = np.arange(nTrapSession)
        g0All =  np.expand_dims(np.zeros(self.nsession * ntraps), 1)
        trapLiveMask = (trapTBT < 2) | (trapTBT > 12)
        ## ADJUST TRAPS FOR SCENARIOS 2 AND 3
        if self.params.scenario == 2:
            (trapTBT, trapTrapNights, trapLiveMask) = self.adjustTrapScen2(
                trapTBT, julyDecMask, trapTrapNights)
        if self.params.scenario == 3:
            (trapTBT, trapLiveMask) = self.adjustTrapScen3(trapTBT, nTrapSession)
        uTrapTBT = np.unique(trapTBT)
        nUTrapTBT = len(uTrapTBT)
        return(trapSession, trapTrapID, trapTrapNights, trapAvail, 
            trapTBT, trapMonthSession, julyDecMask, fortnightMask, nTrapSession, 
            seqTrapSession, g0All, trapLiveMask, uTrapTBT, nUTrapTBT, trapYearSession)

    def adjustTrapScen2(self, trapTBT, julyDecMask, trapTrapNights):
        """
        if run scenario, run this to change live traps to Timms July - Dec
        """
        liveRabbitMask = (trapTBT == 1) | (trapTBT == 14)
        liveFishMask =  (trapTBT == 0) | (trapTBT == 13)
        TimmsFishMask = liveFishMask & julyDecMask
        TimmsRabbitMask = liveRabbitMask & julyDecMask
        # update trapnights for kill traps July - Dec
        TimmsJulyDecMask = TimmsFishMask + TimmsRabbitMask
        trapTrapNights[TimmsJulyDecMask] = 14
        # change bait trap type to kill July - Dec
        trapTBT[TimmsFishMask] = 11
        trapTBT[TimmsRabbitMask] = 12
        # update trapLiveMask for scenario 2
        trapLiveMask = (trapTBT < 2) | (trapTBT > 12)
        return(trapTBT, trapTrapNights, trapLiveMask)

    def adjustTrapScen3(self, trapTBT, nTrapSession):
        """
        run this to change live traps to Timms July - Dec        
        """
        trapTBT[trapTBT == 0] = 11
        trapTBT[trapTBT == 1] = 12
        trapTBT[trapTBT == 13] = 11
        trapTBT[trapTBT == 14] = 12
        # update trapLiveMask for scenario 3
        trapLiveMask = np.zeros(nTrapSession, dtype = bool)
        return(trapTBT, trapLiveMask)

    def calcDistMat(self, trapX, trapY):
        distTrapToCell = distmat(trapX, trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0

    def makeReproMask(self):
        """
        Make mask for identifying reproductive period
        Use this to get mean pop size to calculate number of recruits
        """
        self.reproMask = np.in1d(self.julYear, self.params.reproDays)
        # mask to remove Nov 2024 from repro period
        maskTmp = ((self.yearSession == np.max(self.yearSession)) & 
                    (self.monthSession == 11))
        # Make indexing array for periods on which to calc reproductive pop
        indx = 1
        self.reproPeriodIndx = np.zeros(self.nsession, dtype = int)
        for i in range(1, self.nsession):
            if self.reproMask[i]:
                self.reproPeriodIndx[i] = indx
            if self.reproMask[i-1] and not self.reproMask[i]:
                indx += 1
            if maskTmp[i]:
                self.reproPeriodIndx[i] = 0
                self.reproMask[i] = 1 - self.reproMask[i]
#        print('reproIndx', self.reproPeriodIndx, len(self.reproPeriodIndx))
#        print('reproMask', self.reproMask)

    def makeYearRecruitIndx(self):
        """
        make indx to distribute recruits across session in a 365-day period
        """
        relDay = np.zeros(self.nsession, dtype = int)
        # mask sessions from jan 1 upto end of repro period
        beforeMask = self.params.reproDays[-1] >= self.julYear
        # mask sessions from after repro period to end of December
        afterMask = self.params.reproDays[-1] < self.julYear
        # relative days up
        relDay[beforeMask] = 365 + self.julYear[beforeMask]
        relDay[afterMask] = self.julYear[afterMask] - self.params.reproDays[-1]
        yrRecIndx = np.zeros(self.nsession)
        indx = 0
        for i in range(1, self.nsession):
            if relDay[i] < relDay[i-1]:
                indx += 1
            yrRecIndx[i] = indx
        self.yearRecruitIndx = yrRecIndx.copy()
        self.yearRecruitIndx = self.yearRecruitIndx.astype(int)
        self.uYearIndx = np.unique(self.yearRecruitIndx) 
#        print('recruitIndx', self.yearRecruitIndx, len(self.yearRecruitIndx))
#        print('self.monthSession', self.monthSession, len(self.monthSession))
#        print('sessions', self.session, self.nsession)
#        print('uyearindx', self.uYearIndx)

    def distributeRecruitMasks(self):
        """
        make index and mask arrays for when to distribute new recruits
        """
        calcReproPopMask = np.zeros(self.nsession, dtype = int)
        calcReproPopMask[0] = 1
        for i in range(1, self.nsession):
            if self.reproPeriodIndx[i] < self.reproPeriodIndx[(i - 1)]:
                calcReproPopMask[i] = 1
        self.calcReproPopMask = calcReproPopMask == 1
#        print('calcReproPopMask', self.calcReproPopMask)

    def countTrapChecks(self, trapLiveMask, fortnightMask, trapTrapNights,
            trapYearSession):
        """
        Count number of visits to traps
        """
        midYearsMask = ((trapYearSession > self.params.startDate.year) &
            (trapYearSession < self.params.endDate.year))
        nYearCount = len(np.unique(trapYearSession[midYearsMask]))


        liveFortnightMask = trapLiveMask & fortnightMask
        trapTrapNights[liveFortnightMask] = 9
        self.trapNightAllSess = np.sum(trapTrapNights[midYearsMask]) / nYearCount
#        print('############## mean Ann. trapNightAllSess', self.trapNightAllSess)

        nVisits = trapTrapNights.copy()
        nVisits[liveFortnightMask] = 10          # Live traps July - Dec
        nVisits[trapLiveMask == 0] = 1          # Kill traps 1 visit per session
        self.nVisits = np.sum(nVisits[midYearsMask]) / nYearCount  #[oneYearMask])
#        print('############ mean n visits/yr:', self.nVisits)

    def make_g0_Indx(self):
        """
        make array temporally indexing where high, low and standard g0-multipliers applies
        length is number of session (self.nsession)
        """
        # indx is zero where normal g0 applies
        self.g0_Indx_Multiplier = np.zeros(self.nsession, dtype = int)
        mask1 = self.julYear >= self.params.g0LowDays[0]
        mask2 = self.julYear <= self.params.g0LowDays[1]
        lowMask = mask1 & mask2
        # indx is 1 when low g0 applies
        self.g0_Indx_Multiplier[lowMask] = 1
        mask1 = self.julYear >= self.params.g0HighDays[0]
        mask2 = self.julYear <= self.params.g0HighDays[1]
        highMask = mask1 & mask2
        # indx is 2 when high g0 applies
        self.g0_Indx_Multiplier[highMask] = 2




class Simulate(object):
    def __init__(self, params, basicdata):
        """
        Class and functions to simulate population and trapping dynamics
        """
        ################
        # Call functions
        self.wrapperSimul(params, basicdata)

        ################
    def wrapperSimul(self, params, basicdata):
        """
        wrapper function to call simulate functions
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        if self.params.scenario > 1:
            self.getNorthBlocks()

        if self.params.scenario == 1:
            self.makeIterTrapDat()
            self.basicdata.calcDistMat(self.iter_trapX, self.iter_trapY)

        self.makeStorageArrays()
        self.iterationLooper()
#        print('nstorage', self.nStorage[0])
#        print('nremove', self.removeStorage[:, 0:15])
#        print('recruit', self.recruitStorage[:, 0:15])

    def getNorthBlocks(self):        
        ## NUMBER OF BLOCKS IN NORTH WALL
        (self.bloc, self.rr) = divmod(self.basicdata.nNorthTraps, 
            self.basicdata.nutrapbaittype)
        if (self.params.scenario == 2) or (self.params.scenario == 3):
            self.northTrapID = (np.arange(self.basicdata.nNorthTraps) + 1 + 
                np.max(self.basicdata.trapidContin))


    def makeIterTrapDat(self):
        """
        MAKE ITER ARRAYS FOR TRAP DATA
        """
        self.iter_trapSession = self.basicdata.trapSession.copy()
        self.iter_trapTrapID = self.basicdata.trapTrapID.copy()
        self.iter_trapTrapNights = self.basicdata.trapTrapNights.copy() 
        self.iter_trapAvail = self.basicdata.trapAvail.copy()
        self.iter_trapTBT = self.basicdata.trapTBT.copy()
        self.iter_trapMonthSession = self.basicdata.trapMonthSession.copy()
        self.iter_trapYearSession = self.basicdata.trapYearSession.copy()
        self.iter_JulyDecMask = self.basicdata.JulyDecMask.copy() 
        self.iter_fortnightMask = self.basicdata.fortnightMask.copy()
        self.iter_nTrapSession = self.basicdata.nTrapSession
        self.iter_seqTrapSession =self.basicdata.seqTrapSession.copy()
        self.iter_g0All = self.basicdata.g0All.copy() 
        self.iter_trapLiveMask = self.basicdata.trapLiveMask.copy()
        self.iter_uTrapTBT = self.basicdata.uTrapTBT.copy()
        self.iter_nUTrapTBT = self.basicdata.nUTrapTBT
        self.iter_ntraps = self.basicdata.ntraps
        self.iter_trapidContin = self.basicdata.trapidContin.copy() 
        self.iter_trapbaittype = self.basicdata.trapbaittype.copy()                
        self.iter_trapX = self.basicdata.trapX.copy()
        self.iter_trapY = self.basicdata.trapY.copy()

    def makeStorageArrays(self):
        """
        storage arrays for N, remove, nrecruits
        """
        self.nStorage = np.zeros((self.params.iter, self.basicdata.nsession), dtype = int)
        self.removeStorage = np.zeros((self.params.iter, self.basicdata.nsession), 
            dtype = int)
        self.recruitStorage = np.zeros((self.params.iter, self.basicdata.nsession))
        self.visitStorage = np.zeros((self.params.iter, 2))


    def iterationLooper(self):
        """ 
        loop through iterations
        i = iter; j = session; 
        """
        for i in range(self.params.iter):

            ## ADJUST DATA FOR SCENARIOS 2,3,4: ADD NORTH TRAPS
            if self.params.scenario >1:
                self.permutNorth()
                if self.params.scenario == 4:
                    ## CHANGE X Y DATA TO NORTH WALL FOR SCENARIO 4
#                    self.makeIterTrapDat()
                    self.changeXY_TBT_Scen4()



##            if self.params.scenario > 1:

                if (self.params.scenario == 2) or (self.params.scenario == 3):
                    ## Append north data for scenarios 2 and 3
                    self.appendScen23Data()

        


                ## ORGANISE TRAP DATA FOR SCEN 2, 3 OR 4
                (self.iter_trapSession, self.iter_trapTrapID, self.iter_trapTrapNights, 
                    self.iter_trapAvail, self.iter_trapTBT, self.iter_trapMonthSession, 
                    self.iter_JulyDecMask, 
                    self.iter_fortnightMask, self.iter_nTrapSession, 
                    self.iter_seqTrapSession, self.iter_g0All, 
                    self.iter_trapLiveMask, self.iter_uTrapTBT, self.iter_nUTrapTBT,
                    self.iter_trapYearSession) = (
                        self.basicdata.trapDatScenario(self.iter_ntraps, 
                            self.iter_trapidContin, 
                            self.iter_trapbaittype))


                self.basicdata.calcDistMat(self.iter_trapX, self.iter_trapY)

                self.basicdata.countTrapChecks(self.iter_trapLiveMask, 
                    self.iter_fortnightMask, self.iter_trapTrapNights,
                    self.iter_trapYearSession)

            self.reduceTrapAvail()

            ## calc relative wrapped cauchy for new recruits
            self.relWrpCauchy = calcRelWrapCauchy(self.basicdata.rparagibbs[i], 
                    self.basicdata.nsession, self.basicdata.uYearIndx, 
                    self.basicdata.daypi, self.basicdata.yearRecruitIndx)
            # calc exponential term for pcapt
            self.calcExpTerm(i)
            # get g0 details
            self.getG0AllFX(i)
            # get multinomial probabilities for iteration i
            self.getThMulti(i)
            # loop thru sessions
            for j in range(self.basicdata.nsession):
                self.makeSimSessionArrays(j)
                # if session is first in 'recruitment year' (dec 1)
                if self.basicdata.calcReproPopMask[j]:
                    # get repro pop and distribute recruits in year's sessions       
                    self.getReproPop(i, j)
                # calc n in session i
                self.nPredFx(i, j)
                # simulate the removal process
                self.removalProcess(i, j)

    def permutNorth(self):
        """
        GET PERMUTATIONS OF TBT AND PUT INTO NORTH BLOCKS
        """

        self.northTBT = []
        for b in range(self.bloc):
            tbt_b = np.random.permutation(self.basicdata.utrapbaittype)
            self.northTBT.append(tbt_b)
        if self.rr > 0:
            tbt_b = np.random.choice(self.basicdata.utrapbaittype, 
                size=self.rr, replace=False)
            self.northTBT.append(tbt_b)
        self.northTBT = np.concatenate(self.northTBT)
        ## IF SCENARIO 4 CHANGE NORTH WALL TO ALL KILL TRAPS
        if self.params.scenario == 4:
            self.northTBT[self.northTBT == 0] = 11
            self.northTBT[self.northTBT == 1] = 12
            self.northTBT[self.northTBT == 13] = 11
            self.northTBT[self.northTBT == 14] = 12




    def appendScen23Data(self):
        """
        APPEND MAIN WITH NORTH DATA FOR SCENARIOS 2 AND 3
        """
        self.iter_trapX = np.append(self.basicdata.trapX, self.basicdata.northX)
        self.iter_trapY = np.append(self.basicdata.trapY, self.basicdata.northY)
        self.iter_ntraps = len(self.iter_trapX)
        self.iter_trapidContin = np.arange(self.iter_ntraps)
        self.iter_trapbaittype = np.append(self.basicdata.trapbaittype, self.northTBT) 



    def changeXY_TBT_Scen4(self):
        """
        SELECT MAIN TRAPS AND REPLACE X Y FOR SCEN 4
        """
        self.iter_ntraps = self.basicdata.ntraps
        self.iter_trapidContin = self.basicdata.trapidContin.copy() 
        self.iter_trapbaittype = self.basicdata.trapbaittype.copy()                
        self.iter_trapX = self.basicdata.trapX.copy()
        self.iter_trapY = self.basicdata.trapY.copy()

        randMainIndx = np.random.choice(self.basicdata.trapidContin, 
            self.basicdata.nNorthTraps, replace = False)
        self.iter_trapX[randMainIndx] = self.basicdata.northX
        self.iter_trapY[randMainIndx] = self.basicdata.northY
        self.iter_trapbaittype[randMainIndx] = self.northTBT


    def reduceTrapAvail(self):
        """
        reduce TN and availability 
        """
        nShutdown = np.random.binomial(self.iter_nTrapSession, 
            self.params.probShutdown, size = None)
        nSEBT = np.random.binomial(self.iter_nTrapSession, 
            self.params.probSEBT, size = None)
        idShutdown = np.random.choice(self.iter_seqTrapSession, 
            nShutdown, replace = False)
        idSEBT =  np.random.choice(self.iter_seqTrapSession, nSEBT, replace = False)
        self.iter_trapTrapNights[idSEBT] = self.iter_trapTrapNights[idSEBT] * 0.5
        self.iter_trapAvail[idShutdown] = 0.0
        self.trapNightsAvail = self.iter_trapTrapNights * self.iter_trapAvail


    def makeSimSessionArrays(self, j):
        """
        make arrays for session j
        """
        self.sessionMask = self.iter_trapSession == j
        self.g0Session = self.iter_g0All[self.sessionMask]
        self.availTrapNights = self.trapNightsAvail[self.sessionMask]
        self.tbtSession = self.iter_trapTBT[self.sessionMask]
        self.liveMaskSession = self.iter_trapLiveMask[self.sessionMask]
#        print('g0Sess shp', self.g0Session.shape)
#        print('ntraps', self.iter_ntraps)

    def getReproPop(self, i, j):
        """
        calc repro pop in November
        """
        yearRecruitIndx = self.basicdata.yearRecruitIndx[j]
        if j == 0:
            self.reproPop = self.basicdata.NStart[i]
        else:
            nReproPop = self.nStorage[i, self.basicdata.reproPeriodIndx == yearRecruitIndx] 
            self.reproPop = np.mean(nReproPop)
        self.nTotRecruits = ((self.reproPop * self.basicdata.rgibbs[i]) + 
            self.basicdata.igibbs[i])
        # mask to distribute recruits
        recruitYearMask = self.basicdata.yearRecruitIndx == yearRecruitIndx
        self.recruitYear(i, recruitYearMask)

    def recruitYear(self, i, recruitYearMask):
        """
        Calculate predicted number of recruits in each week of a given year
        """
        relWrpCauchy = self.relWrpCauchy[recruitYearMask]
        recruitYear = self.nTotRecruits * relWrpCauchy
        self.recruitStorage[i, recruitYearMask] = recruitYear
        self.visitStorage[i, 0] = self.basicdata.trapNightAllSess
        self.visitStorage[i, 1] = self.basicdata.nVisits


    def nPredFx(self, i, j):
        """
        single step pred N
        """
        if j == 0:
            nstart = self.basicdata.NStart[i]
        else:
            nstart = self.nStorage[i, (j - 1)] - self.removeStorage[i, (j - 1)]
        nPred = np.round(nstart + self.recruitStorage[i, j])
        self.nStorage[i, j] = nPred.astype(int)


    def getG0AllFX(self, i):
        """
        get g0all for iteration i
        """
        self.g0 = np.expand_dims(self.basicdata.g0gibbs[i], 1)
        self.g0Multiplier = self.basicdata.g0Multigibbs[i]         
        # make array length nSession of multiplier by season
        self.g0MultiplierAll = self.g0Multiplier[self.basicdata.g0_Indx_Multiplier]   # length of nsession
        # g0 for all traps with seasonal variation
        
        self.iter_g0All = g0AllFX_ChangeTraps(self.g0, self.iter_g0All, 
                self.iter_trapTBT, self.basicdata.nsession, 
                self.iter_trapSession, self.g0MultiplierAll)
#        print('g0all', self.iter_g0All[0:20])
#        print('self.g0Multiplier', self.g0MultiplierAll)
#        print('self.basicdata.g0_Indx_Multiplier', self.basicdata.g0_Indx_Multiplier)
#        print('goall shp', self.iter_g0All.shape)


    def calcExpTerm(self, i):
        """
        calculate the distance exponential term with sigma
        """
        self.var2 = 2.0 * (self.basicdata.siggibbs[i]**2.0)
        self.expTermMat =  np.exp(-(self.basicdata.distTrapToCell2) / self.var2)
#        print('shp expterm', self.expTermMat.shape)
#        print('shp distmat', self.basicdata.distTrapToCell2.shape)

    def PPredatorTrapCaptFX(self):
        """
        prob that stoat at specifid locs was capt in specified traps
        """
        expTermArray = self.expTermMat[:, self.locCat]
#        print('expTermArray', expTermArray.shape)
        pNoCapt = 1.0 - (self.g0Session * expTermArray)
#        print('pNoCapt', pNoCapt.shape)
        pNoCapt = pNoCapt.flatten()
        pNoCaptNights = pNoCapt**(self.availTrapNights)
#        print('self.availTrapNights', self.availTrapNights.shape)
#        print('pNoCaptNights', pNoCaptNights.shape)
        self.pTrapCapt = 1.0 - pNoCaptNights
        self.totPCapt = 1.0 - np.prod(1.0 - self.pTrapCapt)
        

    def removalProcess(self, i, j):
        """
        simulate location, removal, and adjust trap avail for session j
        """
        ncats = self.nStorage[i, j]
        for k in range(ncats):
            locCat = np.random.multinomial(1, self.thMultiNom, size = None)
            self.locCat = self.basicdata.cellID[locCat == 1]
            self.PPredatorTrapCaptFX()
            self.captEvent = np.random.binomial(1, self.totPCapt, size = None)
            self.removeStorage[i, j] += self.captEvent
            # id trap and adjust availability for other cats
            if self.captEvent == 1:
                multinomProb = thProbFX(self.pTrapCapt)
                multinomProb = multinomProb.flatten()
                trapCaptID = np.random.multinomial(1, multinomProb, size = None)
                trapID = self.iter_trapidContin[trapCaptID == 1]
                if self.liveMaskSession[trapID]:
                    self.availTrapNights[trapID] = self.availTrapNights[trapID] - 1.0
                    if self.availTrapNights[trapID] < 0.0:
                        self.availTrapNights[trapID] = 0.0
                else:
                    self.availTrapNights[trapID] = self.availTrapNights[trapID] * 0.5

    def getThMulti(self, i):
        """
        get location multinomial probs
        """
        self.b = np.expand_dims(self.basicdata.bgibbs[i], 1)
        self.mu = np.dot(self.basicdata.xdat, self.b)
        thMultiNomTemp = thProbFX(self.mu)
        self.thMultiNom = thMultiNomTemp.flatten()

