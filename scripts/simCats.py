#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
#from numba import jit
#import params
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import cats
import datetime

def logit(x):
    """
    logit function
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    inverse logit function
    """
    return np.exp(x) / (1 + np.exp(x))

def thProbFX(tt):
    """
    multinomial probability
    """
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def matrixsub(arr1, arr2):
    """
    looping sub-function to calculate distance matrix among many points
    """
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    """
    distance matrix calculation
    """
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc


def calcRelWrapCauchy(wrp_rpara, nsession, uYearIndx, daypi, yearRecruitIndx):
    """
    Calc the rel cauchy value for all sessions for distributing recruits
    """
    relWrpCauchy = np.zeros(nsession)
    for i in uYearIndx:
        # day pi in year i
        daypiTmp = daypi[yearRecruitIndx == i]
        # pdf of daypi in year i
        dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
        # mask of recruit window in year i
        reldc = dc / np.sum(dc)     #  dc.sum()
        relWrpCauchy[yearRecruitIndx == i] = reldc
    return relWrpCauchy

def g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll):
    logit_g0 = logit(g01)
    g0All_3 = g0All.copy()
    for k in range(nsession):
        sessmask = trapSession == k                            # sess mask for trap data
        # get logit g0 for that bait-trap by season effect
        logit_g0All_1 = logit_g0 + g0MultiplierAll[k]
        g0All_2 = inv_logit(logit_g0All_1)
        g0All_3[sessmask] = g0All_2
    return g0All_3

def g0AllFX(g0_2D, g0All, trapBaitIndx, nsession, trapSession, g0MultiplierAll):
    """
    make a long array of g0 values associated with trap data for each bait trap type
    and repeat for each session, with season multiplier
    """
    # expand by trap-bait type to length of self.nTraps
    g01 = g0_2D[trapBaitIndx]
    # loop thru sessions
    g0All_3 = g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll)
    return g0All_3

def g0AllFX_ChangeTraps(g0_2D, g0All, trapBaitIndx, nsession, trapSession, g0MultiplierAll):
    """
    make a long array of g0 values associated with trap data for each bait trap type
    and repeat for each session, with season multiplier
    """
    g0All_3 = g0All.copy()
    for w in range(nsession):
        sessmask = trapSession == w
        trapBaitIndxSession = trapBaitIndx[sessmask]
        # expand by trap-bait type to length of self.nTraps
        g01 = g0_2D[trapBaitIndxSession]
        logit_g0 = logit(g01)
        # get logit g0 for that bait-trap by season effect
        logit_g0All_1 = logit_g0 + g0MultiplierAll[w]
        g0All_2 = inv_logit(logit_g0All_1)
        g0All_3[sessmask] = g0All_2
#        if w == 0:
#            print('g0', g0_2D, 'tbt', trapBaitIndxSession[0:10], 'g01', g01[0:10])
            

    return g0All_3


##############################################################
##############################################################
#####
##
class Params(object):
    def __init__(self):
        """
        parameter class for simulations
        """
        # number of iterations to simulate
        self.iter = 300
        # number of habitat covariates
        self.ncov = 3
        # suppresion threshold to stay below
        self.popThreshold = 55
        # set days on which to base population growth rate
        self.reproDays = range(365-61, 365-30)         # 1 Nov - 30 Nov
        # set days for low and high g0 values
        self.g0LowDays = np.array([365-116, 365])         # days >= 5 Sept to 31 Dec
        self.g0HighDays = np.array([58, 160])                # March - 10 June
        ## Reduce trap availability parameters
        self.probShutdown = .01
        self.probSEBT = .01 
        #########   SET THE SCENARIO    ######################
        self.scenario = 9
        print('########## Scenario: ', self.scenario)
##############################################################
##############################################################
class BasicData(object):
    def __init__(self, params, gibbsobj, simDatFile, covFname, northDatFile):
        """
        Object to read in data and run simulation
        """
        self.params = params
        ###################
        # Run Functions
        self.readDataGibbs(gibbsobj, simDatFile, covFname, northDatFile)
        self.makeCovariateArray()
        self.basicdates()

    ####################################
    # Function definitions
    def readDataGibbs(self, gibbsobj, simDatFile, covFname, northDatFile):
        """
        read in trap data and gibbs results from mcmc
        """
        # get parameters from mcmc gibbs results
        self.getGibbsArrays(gibbsobj)
        ###########################################################
        # Trap data - location
        self.trapdat = np.genfromtxt(simDatFile, delimiter=',', names=True,
            dtype=['i8', 'i8', 'f8', 'f8', 'i8'])
        self.trapid = self.trapdat['trapid']
        # X and Y location of trap data
        self.trapX = self.trapdat['xmg']
        self.trapY = self.trapdat['ymg']
        self.trapbaittype = self.trapdat['trapbaittype']
        self.trapbaittype[self.trapbaittype == 16] = 14
        self.trapbaittype[self.trapbaittype == 17] = 15
#        self.liveMask = (self.trapbaittype < 2) & (self.trapbaittype > 13)
        self.scenario = self.trapdat['scenario']    # 0 = current, 1 = theoretical
        self.ntraps = len(self.trapid)
        self.trapid2 = np.arange(self.ntraps)

        # Scenario with traps added in North - From Andy's traps
        if self.params.scenario == 9:
            self.northTraps = np.genfromtxt(northDatFile, delimiter=',', names=True,
                dtype=['i8', 'f8', 'f8', 'i8']) # tid, xmg, ymg, tbt
            
            northtid = np.max(self.trapid) + self.northTraps['tid'] + 1
            self.trapid = np.append(self.trapid, northtid)
            self.trapX = np.append(self.trapX, self.northTraps['xmg'])
            self.trapY = np.append(self.trapY, self.northTraps['ymg'])
            self.trapbaittype = np.append(self.trapbaittype, self.northTraps['trapbaittype'])
            self.trapbaittype[self.trapbaittype == 16] = 14
            self.trapbaittype[self.trapbaittype == 17] = 15
            northScenario = np.ones(len(northtid), dtype = int)
            self.scenario = np.append(self.scenario, northScenario)    # 0 = current, 1 = theoretical
            self.ntraps = len(self.trapid)
            self.trapid2 = np.arange(self.ntraps)
        
        ################################################          Grid-cell covariate data
        # covariate data by 1km grid cells.
        self.covDat = np.genfromtxt(covFname, delimiter=',', names=True,
            dtype=['f8', 'f8', 'f8'])
        self.cellX = self.covDat['x']
        self.cellY = self.covDat['y']
        self.eastCov = self.cellX - np.min(self.cellX)
        self.northCov = self.cellY - np.min(self.cellY)
        self.tuss = self.covDat['hab']
        self.ncell = len(self.eastCov)

    def getGibbsArrays(self, gibbsobj):
        """
        get parameter values for each iteration
        """
        self.ndat = len(gibbsobj.siggibbs)
        self.sampID = np.random.choice(range(self.ndat), self.params.iter, replace = True)
        # get starting N - Nov 2014
        nn = np.shape(gibbsobj.Ngibbs)
        self.NStart = gibbsobj.Ngibbs[self.sampID, -1]
        self.bgibbs = gibbsobj.bgibbs[self.sampID]
        self.rgibbs = gibbsobj.rgibbs[self.sampID]
        self.igibbs = gibbsobj.igibbs[self.sampID]
        self.g0gibbs = gibbsobj.g0gibbs[self.sampID, :-2]          # leave out improved victors
        self.siggibbs = gibbsobj.siggibbs[self.sampID]
        self.rparagibbs = gibbsobj.rparagibbs[self.sampID]               # wrapped cauchy
        self.g0Multigibbs = gibbsobj.g0Multigibbs[self.sampID]           # seasonal g0

    def makeCovariateArray(self):
        """
        use covariate data to make 2-d array
        """
        # Covariates for habitat model
        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)
        self.scaleTuss = (self.tuss - np.mean(self.tuss)) / np.std(self.tuss)
        # stack and make covariate array
        self.xdat = np.hstack([np.expand_dims(self.scaleEast, 1),
                            np.expand_dims(self.scaleNorth, 1),
                            np.expand_dims(self.scaleTuss, 1)])
        # cell id and dist trap to cell  
        self.cellID = np.arange(0, self.ncell, dtype = int)
        self.nCatInCellTemplate = np.zeros(self.ncell)

    def basicdates(self):
        """
        make basic dates and julian dates for simulations
        """
        # get date details for starting day
        self.dateArray = []
        self.startDate = datetime.date(2014, 11, 30)
        self.julStart = np.zeros(1, dtype = int)                           # initiate array
        # make tuple of date info to extract julian day in year
        timetuple = self.startDate.timetuple()
        self.startingJulYear = timetuple.tm_yday
        # get date details for last day of simulation
        self.endDate = datetime.date(2024, 11, 30)
        ####################################
        ####################################
        #   Scenario functions run here
        firstScenarios = np.array([1,3,4,7])
        if np.in1d(self.params.scenario, firstScenarios):
            self.scenario1Wrapper()
        else:
            self.scenario2Wrapper()
        ####################################
        ####################################

    def makeSessionArrays(self):
        """
        make arrays of sessions
        """
        self.nsession = len(self.julStart)
        self.session = np.arange(self.nsession)
        self.uYear = np.unique(self.yearSession)
        # days in radians
        self.daypi = self.julYear / 365 * 2 * np.pi

    def makeReproMask(self):
        """
        Make mask for identifying reproductive period
        Use this to get mean pop size to calculate number of recruits
        """
        self.reproMask = np.in1d(self.julYear, self.params.reproDays)
        # mask to remove Nov 2024 from repro period
        maskTmp = ((self.yearSession == np.max(self.yearSession)) & 
                    (self.monthSession == 11))
        # Make indexing array for periods on which to calc reproductive pop
        indx = 1
        self.reproPeriodIndx = np.zeros(self.nsession, dtype = int)
        for i in range(1, self.nsession):
            if self.reproMask[i]:
                self.reproPeriodIndx[i] = indx
            if self.reproMask[i-1] and not self.reproMask[i]:
                indx += 1
            if maskTmp[i]:
                self.reproPeriodIndx[i] = 0
                self.reproMask[i] = 1 - self.reproMask[i]
#        print('reproIndx', self.reproPeriodIndx, len(self.reproPeriodIndx))
#        print('reproMask', self.reproMask)


    def makeYearRecruitIndx(self):
        """
        make indx to distribute recruits across session in a 365-day period
        """
        relDay = np.zeros(self.nsession, dtype = int)
        # mask sessions from jan 1 upto end of repro period
        beforeMask = self.params.reproDays[-1] >= self.julYear
        # mask sessions from after repro period to end of December
        afterMask = self.params.reproDays[-1] < self.julYear
        # relative days up
        relDay[beforeMask] = 365 + self.julYear[beforeMask]
        relDay[afterMask] = self.julYear[afterMask] - self.params.reproDays[-1]
        yrRecIndx = np.zeros(self.nsession)
        indx = 0
        for i in range(1, self.nsession):
            if relDay[i] < relDay[i-1]:
                indx += 1
            yrRecIndx[i] = indx
        self.yearRecruitIndx = yrRecIndx.copy()
        self.yearRecruitIndx = self.yearRecruitIndx.astype(int)
        self.uYearIndx = np.unique(self.yearRecruitIndx) 
#        print('recruitIndx', self.yearRecruitIndx, len(self.yearRecruitIndx))
#        print('self.monthSession', self.monthSession, len(self.monthSession))
#        print('sessions', self.session, self.nsession)
#        print('uyearindx', self.uYearIndx)

    def distributeRecruitMasks(self):
        """
        make index and mask arrays for when to distribute new recruits
        """
        calcReproPopMask = np.zeros(self.nsession, dtype = int)
        calcReproPopMask[0] = 1
        for i in range(1, self.nsession):
            if self.reproPeriodIndx[i] < self.reproPeriodIndx[(i - 1)]:
                calcReproPopMask[i] = 1
        self.calcReproPopMask = calcReproPopMask == 1
#        print('calcReproPopMask', self.calcReproPopMask)
        
    def makeTrapAvailData(self):
        """
        make arrays of traps avail, trap nights for each session
        """
        self.trapSession = np.repeat(self.session, self.ntraps)
        self.trapTrapID = np.tile(self.trapid2, self.nsession)
        self.trapTrapNights = np.repeat(self.dayInterval, self.ntraps)
        self.trapAvail = np.ones(self.nsession * self.ntraps)
        self.trapTBT = np.tile(self.trapbaittype, self.nsession)
        self.trapMonthSession = np.repeat(self.monthSession, self.ntraps)
        self.JulyDecMask = self.trapMonthSession > 6
        if self.params.scenario < 7:
            self.fortnightMask = self.trapMonthSession > 6
        else:
            self.fortnightMask = self.trapMonthSession < 13
        self.nTrapSession = len(self.trapSession)
        self.seqTrapSession = np.arange(self.nTrapSession)
        self.g0All =  np.expand_dims(np.zeros(self.nsession * self.ntraps), 1)
        # reduce victor TN July - Dec
        self.trapLiveMask = (self.trapTBT < 2) | (self.trapTBT > 13)
#        self.reduceMask = self.trapLiveMask & self.JulyDecMask
#        self.trapTrapNights[self.reduceMask] = 9.0         # 10 on 4 off 

    def makeScenario3Data(self):
        """
        if run scenario, run this to change live traps to Timms July - Dec
        """
        liveRabbitMask = (self.trapTBT == 1) | (self.trapTBT == 15)
        liveFishMask =  (self.trapTBT == 0) | (self.trapTBT == 14)
        TimmsFishMask = liveFishMask & self.JulyDecMask
        TimmsRabbitMask = liveRabbitMask & self.JulyDecMask
        # update trapnights for kill traps July - Dec
        TimmsJulyDecMask = TimmsFishMask + TimmsRabbitMask
        self.trapTrapNights[TimmsJulyDecMask] = 14
        # change bait trap type to kill July - Dec
        self.trapTBT[TimmsFishMask] = 12
        self.trapTBT[TimmsRabbitMask] = 13
        # update trapLiveMask for scenario 3
        self.trapLiveMask = (self.trapTBT < 2) | (self.trapTBT > 13)

    def countTrapChecks(self):
        """
        Count number of visits to traps
        """
        liveFortnightMask = self.trapLiveMask & self.fortnightMask
        self.trapTrapNights[liveFortnightMask] = 9
        self.trapNightAllSess = np.sum(self.trapTrapNights)
        print('##############  self.trapNightAllSess', self.trapNightAllSess)

        self.nVisits = self.trapTrapNights.copy()
        self.nVisits[liveFortnightMask] = 10          # Live traps July - Dec
        self.nVisits[self.trapLiveMask == 0] = 1          # Kill traps 1 visit per session
        print('############   Sum of n visits:', np.sum(self.nVisits))

    def make_g0_Indx(self):
        """
        make array temporally indexing where high, low and standard g0-multipliers applies
        length is number of session (self.nsession)
        """
        # indx is zero where normal g0 applies
        self.g0_Indx_Multiplier = np.zeros(self.nsession, dtype = int)
        mask1 = self.julYear >= self.params.g0LowDays[0]
        mask2 = self.julYear <= self.params.g0LowDays[1]
        lowMask = mask1 & mask2
        # indx is 1 when low g0 applies
        self.g0_Indx_Multiplier[lowMask] = 1
        mask1 = self.julYear >= self.params.g0HighDays[0]
        mask2 = self.julYear <= self.params.g0HighDays[1]
        highMask = mask1 & mask2
        # indx is 2 when high g0 applies
        self.g0_Indx_Multiplier[highMask] = 2


    ####################################################################
    ####################################################################
    #############        Scenario 1
    #############
    def scenario1Wrapper(self):
        """
        Wrapping function to call functions related to scenario 1
        """
        self.trapDatScenario1()
        self.makeScenario1Data()
        self.makeSessionArrays()
#        self.makeReproRecruitArrays()
        self.makeReproMask()
        self.makeYearRecruitIndx()
        self.distributeRecruitMasks()
        self.makeTrapAvailData()
        if self.params.scenario == 3:
            self.makeScenario3Data()
        self.countTrapChecks()
        self.make_g0_Indx()

    def trapDatScenario1(self):
        """
        limit data by scenario
        """
        self.trapid = self.trapid[self.scenario == 0]
        self.trapX = self.trapX[self.scenario == 0]
        self.trapY = self.trapY[self.scenario == 0]
        self.trapbaittype = self.trapbaittype[self.scenario == 0]
        # Change if scenario 4
        if self.params.scenario == 4:
            self.makeScenario4Data()
#        self.liveMask = self.liveMask[self.scenario == 0]
        self.ntraps = len(self.trapid)
        self.trapid2 = np.arange(self.ntraps)
        distTrapToCell = distmat(self.trapX, self.trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0

    def makeScenario4Data(self):
        """
        if run scenario, run this to change live traps to Timms all year
        """
        self.trapbaittype[self.trapbaittype == 14] = 12
        self.trapbaittype[self.trapbaittype == 15] = 13      
        self.trapbaittype[self.trapbaittype == 0] = 12
        self.trapbaittype[self.trapbaittype == 1] = 13      
#        self.liveMask[self.liveMask] = 0

    def makeScenario1Data(self):
        """
        # make session and date data 
        # current trapping regime
        """
        # initial day interval for first week of December
        self.dayInterval = np.array([14])  # first interval for December
        firstDate = self.startDate + datetime.timedelta(days=14)
        self.dateArray.append(firstDate)        # first date entry
        timetuple = firstDate.timetuple()
        self.julYear= timetuple.tm_yday         # first julian year entry
        self.yearSession = timetuple.tm_year
        self.monthSession = timetuple.tm_mon
        nextdate = firstDate + datetime.timedelta(days=14)
        dayInterval = 14
        cc = 1
        # while loop to populate arrays
        while nextdate < self.endDate:
            self.dateArray.append(nextdate)
            timetuple = nextdate.timetuple()
            julYearCC = timetuple.tm_yday
            self.julYear = np.append(self.julYear, julYearCC)
            datediff = nextdate - self.dateArray[0]
            dayDiff = datediff.days         # difference in days
            self.julStart = np.append(self.julStart, dayDiff)
            self.dayInterval = np.append(self.dayInterval, dayInterval)
            # get year and month
            self.yearSession = np.append(self.yearSession, timetuple.tm_year)
            self.monthSession = np.append(self.monthSession, timetuple.tm_mon)
            # get day interval for correct part of year
            if (self.params.scenario < 7) & (julYearCC < 183):
                dayInterval = 7
            else:
                dayInterval = 14
            nextdate = self.dateArray[cc] + datetime.timedelta(days = dayInterval)
            cc += 1
    ###############
    ###############         End scenario 1 functions
    ############################################################
    ############################################################


    ####################################################################
    ####################################################################
    #############        Scenario 2
    #############
    def scenario2Wrapper(self):
        """
        Wrapping function to call functions related to scenario 2
        """
        self.trapDatScenario2()
        self.makeScenario2Data()
        self.makeSessionArrays()
#        self.makeReproRecruitArrays()
        self.makeReproMask()
        self.makeYearRecruitIndx()
        self.distributeRecruitMasks()
        self.makeTrapAvailData()
        # mask in scenario 5 or 9
        mask5_9 = (self.params.scenario == 5) | (self.params.scenario == 9)   
        print('mask 5 9', mask5_9)
        if mask5_9:
            self.makeScenario3Data()
        self.countTrapChecks()
        self.make_g0_Indx()


    def trapDatScenario2(self):
        """
        limit data by scenario
        """
        self.ntraps = len(self.trapid)
        self.trapid2 = np.arange(self.ntraps)
        distTrapToCell = distmat(self.trapX, self.trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0
        # Change if scenario 6: western steel and all Timms (no live traps 12 months)
        if self.params.scenario == 6:
            self.makeScenario4Data()


    def makeScenario2Data(self):
        """
        # make session and date data 
        # current trapping regime
        """
        # initial day interval for first week of December
        self.dayInterval = np.array([14])  # first interval for December
        firstDate = self.startDate + datetime.timedelta(days=14)
        self.dateArray.append(firstDate)        # first date entry
        timetuple = firstDate.timetuple()
        self.julYear= timetuple.tm_yday         # first julian year entry
        self.yearSession = timetuple.tm_year
        self.monthSession = timetuple.tm_mon
        nextdate = firstDate + datetime.timedelta(days=14)
        dayInterval = 14
        cc = 1
        # while loop to populate arrays
        while nextdate < self.endDate:
            self.dateArray.append(nextdate)
            timetuple = nextdate.timetuple()
            julYearCC = timetuple.tm_yday
            self.julYear = np.append(self.julYear, julYearCC)
            datediff = nextdate - self.dateArray[0]
            dayDiff = datediff.days         # difference in days
            self.julStart = np.append(self.julStart, dayDiff)
            self.dayInterval = np.append(self.dayInterval, dayInterval)
            # get year and month
            self.yearSession = np.append(self.yearSession, timetuple.tm_year)
            self.monthSession = np.append(self.monthSession, timetuple.tm_mon)
            # get day interval for correct part of year
            maskScenario = (self.params.scenario < 7) | (self.params.scenario == 9)
            if maskScenario & (julYearCC < 183):
                dayInterval = 7
            else:
                dayInterval = 14
            nextdate = self.dateArray[cc] + datetime.timedelta(days = dayInterval)
            cc += 1



    ###############
    ###############         End scenario 2 functions
    ############################################################
    ############################################################


class Simulate(object):
    def __init__(self, params, basicdata):
        """
        Class and functions to simulate population and trapping dynamics
        """
        ################
        # Call functions
        self.wrapperSimul(params, basicdata)

        ################
    def wrapperSimul(self, params, basicdata):
        """
        wrapper function to call simulate functions
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        self.reduceTrapAvail()
        self.makeStorageArrays()

        self.iterationLooper()
#        print('nstorage', self.nStorage[0])
#        print('nremove', self.removeStorage[:, 0:15])
#        print('recruit', self.recruitStorage[:, 0:15])
        
    def reduceTrapAvail(self):
        """
        reduce TN and availability 
        """
        nShutdown = np.random.binomial(self.basicdata.nTrapSession, self.params.probShutdown, size = None)
        nSEBT = np.random.binomial(self.basicdata.nTrapSession, self.params.probSEBT, size = None)
        idShutdown = np.random.choice(self.basicdata.seqTrapSession, nShutdown, replace = False)
        idSEBT =  np.random.choice(self.basicdata.seqTrapSession, nSEBT, replace = False)
        self.basicdata.trapTrapNights[idSEBT] = self.basicdata.trapTrapNights[idSEBT] * 0.5
        self.basicdata.trapAvail[idShutdown] = 0.0
        self.trapNightsAvail = self.basicdata.trapTrapNights * self.basicdata.trapAvail

    def makeStorageArrays(self):
        """
        storage arrays for N, remove, nrecruits
        """
        self.nStorage = np.zeros((self.params.iter, self.basicdata.nsession), dtype = int)
        self.removeStorage = np.zeros((self.params.iter, self.basicdata.nsession), dtype = int)
        self.recruitStorage = np.zeros((self.params.iter, self.basicdata.nsession))

    def iterationLooper(self):
        """
        loop through iterations
        i = iter; j = session; 
        """
        for i in range(self.params.iter):
            ## calc relative wrapped cauchy for new recruits
            self.relWrpCauchy = calcRelWrapCauchy(self.basicdata.rparagibbs[i], self.basicdata.nsession, 
                    self.basicdata.uYearIndx, self.basicdata.daypi, self.basicdata.yearRecruitIndx)
            # calc exponential term for pcapt
            self.calcExpTerm(i)
            # get g0 details
            self.getG0AllFX(i)
            # get multinomial probabilities for iteration i
            self.getThMulti(i)
            # loop thru sessions
            for j in range(self.basicdata.nsession):
                self.makeSimSessionArrays(j)
                # if session is first in 'recruitment year' (dec 1)
                if self.basicdata.calcReproPopMask[j]:
                    # get repro pop and distribute recruits in year's sessions       
                    self.getReproPop(i, j)
                # calc n in session i
                self.nPredFx(i, j)
                # simulate the removal process
                self.removalProcess(i, j)

    def makeSimSessionArrays(self, j):
        """
        make arrays for session j
        """
        self.sessionMask = self.basicdata.trapSession == j
        self.g0Session = self.basicdata.g0All[self.sessionMask]
        self.availTrapNights = self.trapNightsAvail[self.sessionMask]
        self.tbtSession = self.basicdata.trapTBT[self.sessionMask]
        self.liveMaskSession = self.basicdata.trapLiveMask[self.sessionMask]
#        print('g0Sess shp', self.g0Session.shape)
#        print('ntraps', self.basicdata.ntraps)

    def getReproPop(self, i, j):
        """
        calc repro pop in November
        """
        yearRecruitIndx = self.basicdata.yearRecruitIndx[j]
        if j == 0:
            self.reproPop = self.basicdata.NStart[i]
        else:
            nReproPop = self.nStorage[i, self.basicdata.reproPeriodIndx == yearRecruitIndx] 
            self.reproPop = np.mean(nReproPop)
        self.nTotRecruits = (self.reproPop * self.basicdata.rgibbs[i]) + self.basicdata.igibbs[i]
        # mask to distribute recruits
        recruitYearMask = self.basicdata.yearRecruitIndx == yearRecruitIndx
        self.recruitYear(i, recruitYearMask)

    def recruitYear(self, i, recruitYearMask):
        """
        Calculate predicted number of recruits in each week of a given year
        """
        relWrpCauchy = self.relWrpCauchy[recruitYearMask]
        recruitYear = self.nTotRecruits * relWrpCauchy
        self.recruitStorage[i, recruitYearMask] = recruitYear

    def nPredFx(self, i, j):
        """
        single step pred N
        """
        if j == 0:
            nstart = self.basicdata.NStart[i]
        else:
            nstart = self.nStorage[i, (j - 1)] - self.removeStorage[i, (j - 1)]
        nPred = np.round(nstart + self.recruitStorage[i, j])
        self.nStorage[i, j] = nPred.astype(int)


    def getG0AllFX(self, i):
        """
        get g0all for iteration i
        """
        self.g0 = np.expand_dims(self.basicdata.g0gibbs[i], 1)
        self.g0Multiplier = self.basicdata.g0Multigibbs[i]         
        # make array length nSession of multiplier by season
        self.g0MultiplierAll = self.g0Multiplier[self.basicdata.g0_Indx_Multiplier]   # length of nsession
        # g0 for all traps with seasonal variation

        self.basicdata.g0All = g0AllFX_ChangeTraps(self.g0, self.basicdata.g0All, self.basicdata.trapTBT,
                self.basicdata.nsession, self.basicdata.trapSession, self.g0MultiplierAll)
#        print('g0all', self.basicdata.g0All[0:20])
#        print('g0', self.g0[0:20])
#        print('self.g0Multiplier', self.g0MultiplierAll)
#        print('self.basicdata.g0_Indx_Multiplier', self.basicdata.g0_Indx_Multiplier)
#        print('goall shp', self.basicdata.g0All.shape)


    def calcExpTerm(self, i):
        """
        calculate the distance exponential term with sigma
        """
        self.var2 = 2.0 * (self.basicdata.siggibbs[i]**2.0)
        self.expTermMat =  np.exp(-(self.basicdata.distTrapToCell2) / self.var2)
#        print('shp expterm', self.expTermMat.shape)
#        print('shp distmat', self.basicdata.distTrapToCell2.shape)

    def PPredatorTrapCaptFX(self):
        """
        prob that stoat at specifid locs was capt in specified traps
        """
        expTermArray = self.expTermMat[:, self.locCat]
#        print('expTermArray', expTermArray.shape)
        pNoCapt = 1.0 - (self.g0Session * expTermArray)
#        print('pNoCapt', pNoCapt.shape)
        pNoCapt = pNoCapt.flatten()
        pNoCaptNights = pNoCapt**(self.availTrapNights)
#        print('self.availTrapNights', self.availTrapNights.shape)
#        print('pNoCaptNights', pNoCaptNights.shape)
        self.pTrapCapt = 1.0 - pNoCaptNights
        self.totPCapt = 1.0 - np.prod(1.0 - self.pTrapCapt)
        

    def removalProcess(self, i, j):
        """
        simulate location, removal, and adjust trap avail for session j
        """
        ncats = self.nStorage[i, j]
        for k in range(ncats):
            locCat = np.random.multinomial(1, self.thMultiNom, size = None)
            self.locCat = self.basicdata.cellID[locCat == 1]
            self.PPredatorTrapCaptFX()
            self.captEvent = np.random.binomial(1, self.totPCapt, size = None)
            self.removeStorage[i, j] += self.captEvent
            # id trap and adjust availability for other cats
            if self.captEvent == 1:
                multinomProb = thProbFX(self.pTrapCapt)
                multinomProb = multinomProb.flatten()
                trapCaptID = np.random.multinomial(1, multinomProb, size = None)
                trapID = self.basicdata.trapid2[trapCaptID == 1]
                if self.liveMaskSession[trapID]:
                    self.availTrapNights[trapID] = self.availTrapNights[trapID] - 1.0
                    if self.availTrapNights[trapID] < 0.0:
                        self.availTrapNights[trapID] = 0.0
                else:
                    self.availTrapNights[trapID] = self.availTrapNights[trapID] * 0.5

    def getThMulti(self, i):
        """
        get location multinomial probs
        """
        self.b = np.expand_dims(self.basicdata.bgibbs[i], 1)
        self.mu = np.dot(self.basicdata.xdat, self.b)
        thMultiNomTemp = thProbFX(self.mu)
        self.thMultiNom = thMultiNomTemp.flatten()


class Results(object):
    def __init__(self, params, basicdata, simulate, predatorpath):
        """
        Class and functions to process results
        """
         ################
        # Call functions
        self.wrapperResults(params, basicdata, simulate, predatorpath)

        ################
    def wrapperResults(self, params, basicdata, simulate, predatorpath):
        """
        wrapper function to call simulate functions
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        self.simulate = simulate
        self.predatorpath = predatorpath
        self.calcProbSuccess()
        self.calcSummaryStat()
        self.plotPop_Remove()
        self.plot_1_iter()

    def calcProbSuccess(self):
        """
        calc prob of success keeping pop below threshold
        """
        self.failMask = self.simulate.nStorage > self.params.popThreshold
        self.failArray = np.sum(self.failMask, axis = 1)
        self.successMask = self.failArray == 0
        self.probSuccess = np.sum(self.successMask) / self.params.iter
        print('prob success', self.probSuccess)
        print('array', self.failArray) 
#        print('nstorage', self.simulate.nStorage[:2])

    def calcSummaryStat(self):
        """
        get mean n, removed, recruit
        """
        self.meanN = np.mean(self.simulate.nStorage, axis = 0)
        self.meanRemove = np.mean(self.simulate.removeStorage, axis = 0)               
        self.meanRecruit = np.mean(self.simulate.recruitStorage, axis = 0)

#        print('meanN', self.meanN)


    def plotPop_Remove(self):
        """
        plot mean pred population size, recruits, removed and TN 
        """
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(self.basicdata.dateArray, self.meanN, label = 'Pop size', color = 'k', linewidth = 3)
        lns2 = ax.plot(self.basicdata.dateArray, self.meanRemove, label = 'Cats removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(self.basicdata.dateArray, self.meanRecruit, label = 'New recruits', color = 'b', linewidth = 3)
        P.axhline(y=self.params.popThreshold, color = 'k', linestyle = '--')
#        ax2 = ax.twinx()
#        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', color = 'y', linewidth = 3)
#        lns = lns1 + lns2 + lns3 + lns4
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 80])
        minDate = datetime.date(2014, 11, 25)
        maxDate = datetime.date(2024, 12, 5)
        ax.set_xlim(minDate, maxDate)
#        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
#        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        fileName = 'sim_Scenario' + str(self.params.scenario) + '.png'
        plotFname = os.path.join(self.predatorpath, fileName)
        P.savefig(plotFname, format='png')
        P.show()


    def plot_1_iter(self):
        """
        plot mean pred population size, recruits, removed and TN
        """
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(self.basicdata.dateArray, self.simulate.nStorage[0], label = 'Pop size', color = 'k', linewidth = 3)
        lns2 = ax.plot(self.basicdata.dateArray, self.simulate.removeStorage[0], label = 'Cats removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(self.basicdata.dateArray, self.simulate.recruitStorage[0], label = 'New recruits', color = 'b', linewidth = 3)
        P.axhline(y=self.params.popThreshold, color = 'k', linestyle = '--')
#        ax2 = ax.twinx()
#        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', color = 'y', linewidth = 3)
#        lns = lns1 + lns2 + lns3 + lns4
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 80])
        minDate = datetime.date(2014, 11, 25)
        maxDate = datetime.date(2024, 12, 5)
        ax.set_xlim(minDate, maxDate)
#        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
#        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        fileName = 'iter1_Scenario' + str(self.params.scenario) + '.png'
        plotFname = os.path.join(self.predatorpath, fileName)
        P.savefig(plotFname, format='png')
        P.show()

            
########            Main function
#######
def main():

    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    simDatFile = os.path.join(predatorpath,'simTrapDat.csv')
    covDatFile = os.path.join(predatorpath,'covDat.csv')
    northDatFile = os.path.join(predatorpath,'northWall.csv')
  
    inputGibbs = os.path.join(predatorpath, 'out_gibbs.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    params = Params()

    basicdata = BasicData(params, gibbsobj, simDatFile, covDatFile, northDatFile)

    simulate = Simulate(params, basicdata)

    results = Results(params, basicdata, simulate, predatorpath)

if __name__ == '__main__':
    main()



