#!/usr/bin/env python

import os
import numpy as np
import pickle
import datetime

class Params(object):
    def __init__(self, species=None, scenario=None):
        """
        parameter class for simulations
        """
        #########   SET THE SCENARIO    ######################
        self.species = species      #'Cats'
        self.scenario = scenario    #4       ## 1,2,3,4
        print('Params ########## Scenario: ', self.scenario, 'Species', self.species)
        ######################################################

        self.predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
        scenDir = 'Scen' + str(self.scenario) + self.species
        self.resultsPath = os.path.join(self.predatorpath, 'simulationResults', scenDir)

        ## PICKLE FILE PATHS
        bdFName = 'basicdata' + self.species + str(self.scenario) +'.pkl'
        self.basicdataFName = os.path.join(self.resultsPath, bdFName)
        simFName = 'simulate' + self.species + str(self.scenario) +'.pkl'
        self.simulateFName = os.path.join(self.resultsPath, simFName)
        ## N DATA FNAME
        nDatFName = 'nDat' + self.species + str(self.scenario) +'.csv'
        self.nDatFName = os.path.join(self.resultsPath, nDatFName)
        ## PARAMS FILE NAME FOR REFERENCE
        paramsFName = 'params' + self.species + 'Scen' + str(self.scenario) + '.py'
        self.paramsResultsFName = os.path.join(self.resultsPath, paramsFName)        
        self.paramsStartFName = 'simulationParams.py'

        # paths and data to read in
        self.simDatFile = os.path.join(self.predatorpath,'simMod2Data.csv')
        if self.species == 'Hedgehogs':
            self.covDatFile = os.path.join(self.predatorpath,'tussockXY50m.csv')
        else:
            self.covDatFile = os.path.join(self.predatorpath,'covDat250.csv')

#        self.covDatFile = os.path.join(self.predatorpath,'covDat250.csv')
        self.northDatFile = os.path.join(self.predatorpath,'steelWall100m.csv')
        gibbsFName = 'out_gibbs' + self.species + 'Mod2.pkl'
        self.inputGibbs = os.path.join(self.predatorpath, gibbsFName)

        # number of iterations to simulate
        self.iter = 400
        # number of habitat covariates
        if self.species == 'Stowea':
            self.ncov = 2
        else:
            self.ncov = 3
        # suppresion threshold to stay below
        if self.species == 'Hedgehogs':
            self.popThreshold = 200
        else:
            self.popThreshold = 55


        # set days on which to base population growth rate
        if self.species == 'Hedgehogs':
            self.reproDays = range(365-107, 365-75)         # 1 Nov - 30 Nov
            # set days for low and high g0 values
            date_0 = datetime.date(2007, 6, 1)
            timetuple = date_0.timetuple()
            julYr_0 = timetuple.tm_yday
            date_1 = datetime.date(2007, 8, 31)
            timetuple = date_1.timetuple()
            julYr_1 = timetuple.tm_yday
            self.g0LowDays = np.array([julYr_0, julYr_1])
            self.g0HighDays = np.array([0, 107])                # March - 10 June
        else:
            self.reproDays = range(365-62, 365-31)         # 1 Nov - 30 Nov
            # set days for low and high g0 values
            self.g0LowDays = np.array([365-116, 365])         # days >= 5 Sept to 31 Dec
            self.g0HighDays = np.array([58, 160])                # March - 10 June


        ## Reduce trap availability parameters
        self.probShutdown = .01
        self.probSEBT = .03 
        # get date details for last day of simulation
        self.startDate = datetime.date(2024, 11, 30)
        self.endDate = datetime.date(2034, 11, 30)
        ## RESULT QUANTILES
        self.resultsQuantile = 0.90

        print('params iter', self.iter)
        print('sim data', self.simDatFile)
        print('cov data', self.covDatFile)
        print('north data', self.northDatFile)
        print('gibbs data', self.inputGibbs)
        print('params file', self.paramsResultsFName)
