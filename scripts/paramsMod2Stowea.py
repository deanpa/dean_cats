#!/usr/bin/env python

#import sys
import numpy as np
import os
import pickle
import empiricalPredator

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ###################################################
        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000    # 3000       # number of estimates to save for each parameter
        self.thinrate = 60   #60   #50         # thin rate
        self.burnin = 0 # 15000             # burn in number of iterations
        totalIterations = ((self .ngibbs * self.thinrate) + self.burnin)
        self.interval = 30000 # do 25000 next   #55000 + 15000 +   #totalIterations
        self.checkpointfreq = self.interval
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, totalIterations,
            self.thinrate)

        ## Run Conditions
        self.initialRun = False
        self.useCheckedData = True   # gibbs arrays not full from previous runs

        ###################################################
        ###################################################
        ###################################################
        ###################################################
        # name of species trap-data name: ( 'Cats', 'Ferrets', 'Hedgehogs', 'Stowea')

        self.species = 'Stowea'
        self.modelID = 'Mod2'
        # set path and output file names 
        self.predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
        # filename to pickle basic data from present run to be used to initiate new runs
        self.outBasicdataFname = os.path.join(self.predatorpath,
            'out_basicdata' + self.species + self.modelID + '.pkl')
        # pickle predator data from present run to be used to initiate new runs
        self.outPredatordataFname = os.path.join(self.predatorpath,
            'out_predatordata' + self.species + self.modelID + '.pkl')
        # pickle mcmc results for post processing in gibbsProcessing.py
        self.outGibbsFname = os.path.join(self.predatorpath,
            'out_gibbs' + self.species + self.modelID + '.pkl')
        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.predatorpath,
            'out_checking' + self.species + self.modelID + '.pkl')
        # text file for hi and low g0 values
        self.g0_HiLowFname = os.path.join(self.predatorpath,
            'g0_HiLow_' + self.species + self.modelID + '.txt')
        # plot legend label
        self.plotLegendName = self.species + ' removed'
        # plot name 
        self.removeRecruitPlotFname = os.path.join(self.predatorpath, 
            'N_remove_recruit_' + self.species + self.modelID + '.png')
        # Results summary table names
        self.summaryTableFname = os.path.join(self.predatorpath, 
            'summaryTable_' + self.species + self.modelID + '.txt')
        # pickle name
        self.ParameterFname = os.path.join(self.predatorpath, 
            'pickle' + self.species + self.modelID + '.pkl')

        ##############
        ###############      Initial values parameters for updating
        ############################################################################
        ############################################################################
        #############################   STOAT-WEASEL PARAMETERS
        self.maxN =  100
        self.sigma = 310.0
        self.sigma_mean = 300
        self.sigma_sd = 10.0     #np.sqrt(100)
        self.sigma_search_sd = 8
        self.g0_alpha = 3.0    #1.8  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
        self.g0_beta = 49.0     #26.87   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
        self.g0Sd = 0.02          # search parameter for g0
        self.g0Multiplier = np.array([0, -.3, .11]) 
        self.g0MultiPrior = np.array([0, 1.5])
        self.g0MultiSearch = 0.1
        self.nTopTraps = 26      # limit prob of capt by this number of traps

        self.ig = 75      #/365.0*7.0
        # uniform prior between 20 and 300
        self.immDistr = 'nor' # or 'uni'
        self.immSearch = 3.0
        self.immUniform = np.array([5, 300])
        self.imm_mean = 80.         # 5  shape
        self.imm_sd = 30.         # 15 scale
        ###################
        ##################      # Reproduction parameters
        self.rg = 2.2
        self.r_shape = 16.0  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
        self.r_scale = 0.15  # 2.0  #4.0        #0.1  # 4.0
        self.reproSearch = 0.5
        # latent initial number of recruits ~ uniform(10, 100)
        self.initialReproPop = 34.0
        self.IRR_priors = np.array([4, 120])
        self.initialReproSearch = 7.0

        # wrp cauchy parameters for distributing recruits
        self.rpara = np.array([np.pi*2/3, .1])                # real numbers: normally distributed
        # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_wrpc_Priors = np.array([1.0, 20.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
        self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    
        self.rparaSearch = np.array([.015, .015])

        # set days on which to base population growth rate
        self.reproDaysBack = np.array([122, 92])
        self.reproDays = range(365-self.reproDaysBack[0], 365-self.reproDaysBack[1])                 # 1 Sept - 30 Sept
        # set days for low and high g0 values
        self.g0LowDays = np.array([365-122, 365 - 31])          # 1 Aug to 30 Nov
        self.g0HighDays = np.array([32, 151])                   # Feb - May 

        ######################################
        ######################################
        # modify  variables used for habitat model
        self.xdatDictionary = {'scaleEast' : 0, 'scaleNorth' : 1, 'scaleTuss' : 2,
                            'scaleCatFerret' : 3}
        self.scaleEast = self.xdatDictionary['scaleEast']
        self.scaleNorth = self.xdatDictionary['scaleNorth']
        self.scaleTuss = self.xdatDictionary['scaleTuss']
        self.scaleCatFerret = self.xdatDictionary['scaleCatFerret']
        # array of index values to get variables from 2-d array
        #############   REMOVED TUSSUCK BECAUSE OF CORRELATION
        self.xdatIndx = np.array([self.scaleEast, self.scaleCatFerret], dtype = int)    
        ######################################
        ######################################

        self.b = np.array([.004, -.2])
        self.catFerretPara = np.array([0.016, 0.075, -0.08, 0.025, 0.069, -0.133])
        # beta priors on habitat coefficients
        self.bPrior = 0.0
        self.bPriorSD = np.sqrt(1.0)
        self.nbcov = len(self.b)
 
