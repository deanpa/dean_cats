#!/usr/bin/env python

import os, sys
import numpy as np
import pickle
import datetime
import empiricalPredator
sys.path.append("..")
import predators
import pylab as P


def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

#@jit
def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

class Params(object):
    def __init__(self, predatorpath):
        """
        ## set parameters for analysis
        """
        ## RTC parameters
        self.monthDayLiz = [2, 7]       ## February 7
        self.yearRTC = np.arange(2006, 2014)
        self.daysBackRTC = 180
        self.sigma = np.array([260.0, 403.0, 273.0, 201.0])   
        self.searchRadius = 4.0

        ## Density parameters
        self.daysBackDensity = 364
#        self.densitySearchDist = 375
        ## Name parameters
        self.sppNames = np.array(['Cats', 'Ferrets', 'Hedgehogs', 'Stowea'])

        self.rtcTableFname = os.path.join(predatorpath, 'rtc_days' + 
            str(self.daysBackRTC) + '_rad' + str(np.int(self.searchRadius)) + '.txt')
        print('RTC Fname', self.rtcTableFname)

        self.densityTableFname = os.path.join(predatorpath, 'densityRisk' + 
            str(self.daysBackDensity) + '_rad' + str(np.int(self.searchRadius)) + '.txt')
        print('Density Fname', self.densityTableFname)


class AnalyseData(object):
    def __init__(self, covDatFile, lizPtFile, conAreaFile, predatorpath, params):
        self.params = params
        self.predatorpath = predatorpath

        ##################
        ## Run functions
        self.getBasicdata()
        self.readData(covDatFile, lizPtFile, conAreaFile)
        self.makeDates()
        self.setupStorageArrays()
        self.getConAreaDistMat()
        self.conAreaRTC()
        self.rtcConAreaTable()

                ## functions for density 
#        self.speciesParameters()
#        self.readMCMCResults()
#        self.relativePredation()
#        self.annualPopPeaks()
#        self.conAreaDensity()
#        self.densityConAreaTable()

        ## End running functions
        #########################

    def readData(self, covDatFile, lizPtFile, conAreaFile):
        """
        ## read in covariate data for analysis
        """
        # covariate data
        self.covDat = np.genfromtxt(covDatFile, delimiter=',', names=True,
            dtype=['f8', 'f8', 'f8'])
        self.cellX = self.covDat['x']
        self.cellY = self.covDat['y']
        self.tuss = self.covDat['hab']
        self.nCell = len(self.cellX)
        print('nCells', self.nCell, 'area ha =', self.nCell * 6.25, 
            'area km2 =', self.nCell / 16.0)
        self.cellID = np.arange(self.nCell, dtype = int)
        ## get lizard location and ID data
        self.lizPtDat = np.genfromtxt(lizPtFile, delimiter=',', names=True,
            dtype=['S10', 'S10', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.lizX = self.lizPtDat['easting']
        self.lizY = self.lizPtDat['northing']
        lizID = self.lizPtDat['sID']
        # decode byte data
        self.nLiz = len(self.lizX)
        self.lizID = decodeBytes(lizID, self.nLiz)
        print('sid', self.lizID[:5], type(self.lizID))
        ## get conArea data points
        self.conAreaDat = np.genfromtxt(conAreaFile, delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8'])
        self.conID = self.conAreaDat['pid']
        self.conX = self.conAreaDat['x_mg']
        self.conY = self.conAreaDat['y_mg']
#        print('conarea', self.conID, type(self.conID), self.conX)
        
    def getBasicdata(self):
        """
        ## read in basicdata for each species
        """
        # loop thru species
        for i in range(4):
            if i < 3:
                fname = os.path.join(self.predatorpath,'out_basicdata250' + 
                    self.params.sppNames[i] + '.pkl')
            else:
                fname = os.path.join(self.predatorpath,'out_basicdataEmpStowea.pkl')
            ## read pickle
            fileobj = open(fname, 'rb')
            basicdata = pickle.load(fileobj)
            fileobj.close()
            ## get trapping data from pickle
            trapped = basicdata.trapTrapped
            if i == 0:
                lenTrapSess = len(trapped)
                self.trapTrapped2D = np.zeros((lenTrapSess, 4), dtype = int)
                self.trapSession = basicdata.trapSession
                self.trapNightsAvail_2D = basicdata.trapNightsAvail_2D
                self.trapX = basicdata.trapX
                self.trapY = basicdata.trapY
                self.nTraps = basicdata.nTraps
                self.sessionYear = basicdata.sessionYear
                self.sessionMonth = basicdata.sessionMonth
                self.sessionDay = basicdata.sessionDay
                self.nsession = basicdata.nsession
            ## populated the 2d trapping data - ((nsess * ntraps, 4 species))
            self.trapTrapped2D[:,i] = trapped

    def makeDates(self):
        """
        ## make dates, julian days and sessions line up
        """            
        self.firstDate = datetime.date(self.sessionYear[0], self.sessionMonth[0], self.sessionDay[0])
        print('first date', self.firstDate)
        self.dates = self.firstDate
        self.session = np.arange(self.nsession)
        print('nsess', self.nsession)
        self.julSess = np.zeros(self.nsession, dtype = int)
        for i in range(1, self.nsession):
            date_i = datetime.date(self.sessionYear[i], self.sessionMonth[i], self.sessionDay[i])
            self.dates = np.append(self.dates, date_i)
            dayDiff = date_i - self.firstDate
            self.julSess[i] = dayDiff.days
        print('Last date', self.dates[-1])


    def setupStorageArrays(self):
        """
        ## set up storage arrays for results
        """
        self.yearResults = np.arange(2006, 2014, dtype = int)
        self.nYearResults = len(self.yearResults)
        self.conAreaID = self.conID[2:]
        self.nConID = len(self.conAreaID)
        self.yearArray = np.repeat(self.yearResults, self.nConID)
        self.conIDArray = np.tile(self.conAreaID, self.nYearResults)
        self.nRTC = len(self.yearArray)
        self.sumTrapped = np.zeros((self.nRTC, 4))
        self.sumTrapNight = np.zeros((self.nRTC, 4))
    
    def getConAreaDistMat(self):
        """
        ## make distance matrix between conareas and all traps
        ## dimensions are traps X conservation areas
        """
        self.conDistMat = distmat(self.trapX, self.trapY, self.conX, self.conY)
        self.densityDistMat = distmat(self.cellX, self. cellY, self.conX, self.conY)

    def conAreaRTC(self):
        """
        ## get RTC for each conArea 3,4,5,6
        """
        ## loop thru species first
        for g in range(4):
            maxDist = self.params.sigma[g] * self.params.searchRadius
            distMask = self.conDistMat <= maxDist
            trapDat = self.trapTrapped2D[:,g]
            cc = 0
            ## loop thru years
            for i in range(self.nYearResults):
                date_i = datetime.date(self.yearResults[i], self.params.monthDayLiz[0],
                    self.params.monthDayLiz[1])
                jul_i = (date_i - self.firstDate).days
#                print('i', i, 'date_i', date_i, 'jul_i', jul_i)
                minJul = jul_i - self.params.daysBackRTC
                julMask = (self.julSess >= minJul) & (self.julSess <= jul_i)            
                sess_i = self.session[julMask]
                ## loop thru Conservation Areas
                for j in range(self.nConID):
                    distMask_i = distMask[:, j]
                    ## loop thru session
                    for k in sess_i:
                        sessMask = self.trapSession == k
                        trap_i = trapDat[sessMask]
                        trap_ij = trap_i[distMask_i]
                        tNight_i = self.trapNightsAvail_2D[sessMask]
                        tNight_ij = tNight_i[distMask_i]
                        sumTrapped_gijk = np.sum(trap_ij)
                        sumTNight_gijk= np.sum(tNight_ij)                
                        self.sumTrapped[cc, g] += sumTrapped_gijk
                        self.sumTrapNight[cc, g] += sumTNight_gijk
#                    print('spp col', g, 'year row', i, 'area row', j, 'row', cc) 
                    cc += 1
        self.RTC_ConArea = self.sumTrapped / self.sumTrapNight
#        print('rtc', self.RTC_ConArea)
        self.yearArray = np.repeat(self.yearResults, self.nConID)
        self.conIDArray = np.tile(self.conAreaID, self.nYearResults)

    def rtcConAreaTable(self):
        """
        ## make structured array of lizard data with predation covariates
        """
        structured = np.empty((self.nRTC,), dtype=[('ConsAreaID', np.int), 
                    ('Year', np.int),
                    ('CatRTC', np.float), ('FerretRTC', np.float),
                    ('HedgehogRTC', np.float), ('StoWeaRTC', np.float)])
        # copy data over
        structured['ConsAreaID'] = self.conIDArray
        structured['Year'] = self.yearArray
        structured['CatRTC'] = self.RTC_ConArea[:,0]
        structured['FerretRTC'] = self.RTC_ConArea[:,1]
        structured['HedgehogRTC'] = self.RTC_ConArea[:,2]
        structured['StoWeaRTC'] = self.RTC_ConArea[:,3]
        fmtDat=['%u', '%u', '%.8f', '%.8f', '%.8f', '%.8f']
        headNames = ['ConsAreaID Year CatRTC FerretRTC HedgehogRTC StoWeaRTC']
        np.savetxt(self.params.rtcTableFname, structured, comments = '', fmt = fmtDat,
            header = 'ConsAreaID Year CatRTC FerretRTC HedgehogRTC StoWeaRTC')  

    #####################################################################
    ###############
    ###############     Functions for conservation area density estimation
    ###############

    def speciesParameters(self):
        """
        ## get species specific parameters
        """

        self.predNames = np.array(['Cats', 'Ferrets', 'Hedgehogs', 'Stowea'])
        self.sppCoeff = np.array([[-0.005, 0.037, -0.052],
                                [0.007, 0.098, -0.121],
                                [-0.005, 0.037, -0.052], 
                                [0.043, 0.0, -0.031]])
        self.sppPeak = np.array([69.0, 80.0, 120.0, 30.0])
        self.scaleEast = (self.cellX - np.mean(self.cellX)) / np.std(self.cellX)
        self.scaleNorth = (self.cellY - np.mean(self.cellY)) / np.std(self.cellY)
        self.scaleTuss = (self.tuss - np.mean(self.tuss)) / np.std(self.tuss)
        # stack and make covariate array
        self.xdatFull = np.zeros((self.nCell, 4))
        self.xdatFull[:, 0] = self.scaleEast
        self.xdatFull[:, 1] = self.scaleNorth
        self.xdatFull[:, 2] = self.scaleTuss
        ## get covariate for Stowea
        predXDat = np.hstack([self.xdatFull[:, :3], self.xdatFull[:, :3]])
        catFerretPara = np.append(self.sppCoeff[0], self.sppCoeff[1])
        xTmp = np.dot(predXDat, catFerretPara)
        xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
        self.xdatFull[:, -1] = xTmp

    def relativePredation(self):
        """
        ## get covariates for 4 species
        """
        self.cellRiskSurface = np.zeros((self.nCell, 4))
        self.cellRelRisk = np.zeros((self.nCell, 4))
        stoweaArr = np.array([0,1,3])
        for i in range(4):
            if i < 3:
                cellPred_i = np.dot(self.xdatFull[:, :3], self.sppCoeff[i])
                self.cellRiskSurface[:, i] = (cellPred_i - np.mean(cellPred_i)) / np.std(cellPred_i)
            else:
                cellPred_i = np.dot(self.xdatFull[:, stoweaArr], self.sppCoeff[i])
                self.cellRiskSurface[:, i] = (cellPred_i - np.mean(cellPred_i)) / np.std(cellPred_i)
            # spatial and numerical effects
            relPredEffect = (np.exp(cellPred_i) / np.sum(np.exp(cellPred_i)))
            self.cellRelRisk[:, i] = relPredEffect

#        print('rel Pred eff', self.cellRiskSurface[:5])
#        print('mean', np.mean(self.cellRiskSurface, axis = 0), np.std(self.cellRiskSurface, axis = 0),
#            'max', np.max(self.cellRiskSurface, axis = 0))
#        print('rel Pred eff', self.cellRelRisk[:5])
#        print('mean', np.mean(self.cellRelRisk, axis = 0), np.std(self.cellRelRisk, axis = 0),
#            'max', np.max(self.cellRelRisk, axis = 0))


    def readMCMCResults(self):
        """
        ## read in mcmc results for each species - get mean pop size
        """
        self.mcmcPop = np.zeros((self.nsession, 4))
        for i in range(4):
            fname = 'summaryTable_' + self.params.sppNames[i] + '.txt'
            fnamepath = os.path.join(self.predatorpath, fname)
            table_i = np.genfromtxt(fnamepath, delimiter=' ', names=True,
            dtype=['S10', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 
                'f8', 'f8', 'f8'])
            meanPop_i = table_i['Mean'][:self.nsession]
            self.mcmcPop[:, i] = meanPop_i


    def annualPopPeaks(self):
        """
        ### get annual peak in pop size
        """
        self.peakPop = np.zeros((self.nYearResults, 4))
        for i in range(self.nYearResults):
            date_i = datetime.date(self.yearResults[i], self.params.monthDayLiz[0],
                self.params.monthDayLiz[1])
            jul_i = (date_i - self.firstDate).days
#            print('i', i, 'date_i', date_i, 'jul_i', jul_i)
            minJul = jul_i - self.params.daysBackDensity
            julMask = (self.julSess >= minJul) & (self.julSess <= jul_i)            
            sess_i = self.session[julMask]
            for j in range(4):
                self.peakPop[i, j] = np.max(self.mcmcPop[sess_i, j])
        print('peakpop', self.peakPop)

        ## Density parameters
#        self.daysBackDensity = 364
#        self.densitySearchDist = 375
#        self.yearResults = np.arange(2006, 2014, dtype = int)
#        self.nYearResults
#        self.cellRelRisk = np.zeros((self.nCell, 4))

    def conAreaDensity(self):
        """
        ## get density for each conservation Area 3,4,5,6
        """
        self.densityConArea= np.zeros((self.nRTC, 4))
        ## loop thru species first
        for g in range(4):
            maxDist = self.params.sigma[g] * self.params.searchRadius
            distMask = self.densityDistMat <= maxDist
            trapDat = self.trapTrapped2D[:,g]
            cc = 0
            ## loop thru years
            for i in range(self.nYearResults):
                peakPop_gi = self.peakPop[i, g]
                ## loop thru Conservation Areas
                for j in range(self.nConID):
                    distMask_i = distMask[:, j]
                    # density in individual / km squared (resol = 250 m)
                    cellRR_gij = (self.cellRelRisk[distMask_i, g] * peakPop_gi)
                    invDist = 1.0 / np.log(self.densityDistMat[distMask_i, j] + 1.0)
                    sumInvDist = np.sum(invDist)
                    prodRRInvDist = (cellRR_gij * invDist)
                    wtAveDensity = np.sum(prodRRInvDist) / sumInvDist
                    self.densityConArea[cc, g] = wtAveDensity
#                    print('spp col', g, 'year row', i, 'area row', j, 'row', cc) 
                    cc += 1
        print('densityConArea', self.densityConArea)

    def densityConAreaTable(self):
        """
        ## make structured array of density risk by species
        """
        structured = np.empty((self.nRTC,), dtype=[('ConsAreaID', np.int), 
                    ('Year', np.int),
                    ('CatDen', np.float), ('FerretDen', np.float),
                    ('HedgehogDen', np.float), ('StoWeaDen', np.float)])
        # copy data over
        structured['ConsAreaID'] = self.conIDArray
        structured['Year'] = self.yearArray
        structured['CatDen'] = self.densityConArea[:,0]
        structured['FerretDen'] = self.densityConArea[:,1]
        structured['HedgehogDen'] = self.densityConArea[:,2]
        structured['StoWeaDen'] = self.densityConArea[:,3]
        fmtDat=['%u', '%u', '%.8f', '%.8f', '%.8f', '%.8f']
        np.savetxt(self.params.densityTableFname, structured, comments = '', fmt = fmtDat,
            header = 'ConsAreaID Year CatDensity FerretDensity HedgehogDensity StoWeaDensity')  


def main():

    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')

    params = Params(predatorpath)

    # paths and data to read in
    covDatFile = os.path.join(predatorpath,'covDat250.csv')
    lizPtFile = os.path.join(predatorpath,'PilotGrids.csv')
    conAreaFile = os.path.join(predatorpath,'conAreaPT_MG.csv')

    analysedata = AnalyseData(covDatFile, lizPtFile, conAreaFile, predatorpath, params)

if __name__ == '__main__':
    main()

