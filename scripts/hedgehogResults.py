#!/usr/bin/env python

from paramsHedgehogs import Params
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
from numba import jit
import empiricalResults

def main():
    params = Params()
    print('########################')
    print('########################')
    print('###')
    print('#    Species = Hedgehogs')
    print('###')
    print('########################')
    print('########################')

    # paths and data to read in
    predatorpath = os.getenv('PREDATORPROJDIR', default='.')

#    inputGibbs = os.path.join(predatorpath, 'out_gibbs.pkl')
    fileobj = open(params.outGibbsFname, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()
    print('gibbsResults Name:', params.outGibbsFname)    

    resultsobj = empiricalResults.ResultsProcessing(gibbsobj, params, predatorpath)

    resultsobj.getPopNames()

    resultsobj.makeTableFX()

    resultsobj.makeG0Table()
                    
    resultsobj.plotFX()

    resultsobj.plotPop_Remove() 
   
    resultsobj.writeToFileFX()

if __name__ == '__main__':
    main()


