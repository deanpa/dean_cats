#!/usr/bin/env python

import os
import numpy as np
import pickle
from manipCat import PickleTrapData

class RawTrapData(object):
    def __init__(self, trapFname, newTrapFname):
        """
        Object to read in trap location data
        """
        ###########################################################
        # Trap data - location
        self.oldtrap = np.genfromtxt(trapFname, delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        # number of traps
        self.trapdatTID = self.oldtrap['TRAP']
        # self.nTrapsPre = len(self.trapTRAPPre)
        # X and Y location of trap data
        self.trapDatX = self.oldtrap['EASTING']
        self.trapDatY = self.oldtrap['NORTHING']

        # New Trap data - location from Patrick Libby on 10 May 2015
        self.newtrap = np.genfromtxt(newTrapFname, delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.newTrapID = self.newtrap['tid']
        # X and Y location of trap data
        self.newtrapX = self.newtrap['xmg']
        self.newtrapY = self.newtrap['ymg']

        ###########################################################
        # Run rawtrapdata functions
        self.appendTrapDat()

    ###############################################################
    ###############################################################
    ################# Functions

    def appendTrapDat(self):
        """
        cycle thru new data and append to old trap data
        """
        uTrapid = np.unique(self.newTrapID)       # new unique traps 
        nuTrapid = len(uTrapid)               # number of traps
        # build new trapdata
        for i in range(nuTrapid):
            id_i = self.newTrapID[i]
            mask = np.in1d(id_i, self.trapdatTID)
            if mask == 0:
                x_i = self.newtrapX[self.newTrapID == id_i]
                y_i = self.newtrapY[self.newTrapID == id_i]
                self.trapdatTID = np.append(self.trapdatTID, id_i)
                self.trapDatX = np.append(self.trapDatX, x_i)
                self.trapDatY= np.append(self.trapDatY, y_i)

#class PickleTrapData(object):
#    def __init__(self, rawtrapdata):
#        """
#        Object to pickel trap data
#        """
#        self.trapdatTID = rawtrapdata.trapdatTID.copy()
#        self.trapDatX = rawtrapdata.trapDatX.copy()
#        self.trapDatY = rawtrapdata.trapDatY.copy()


########            Main function
#######
def main():
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    trapDatFile = os.path.join(predatorpath,'trapDat2.csv')
    newTrapDatFile = os.path.join(predatorpath,'trapDatPat_DeanManip.csv')
    # initiate rawtrapdata class and object 
    rawtrapdata = RawTrapData(trapDatFile, newTrapDatFile)

    pickletrapdata = PickleTrapData(rawtrapdata)

    # pickle trap data to be used in manipCats.py
    outManipdata = os.path.join(predatorpath,'out_newTrapData.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(pickletrapdata, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()




