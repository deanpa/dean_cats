#!/bin/bash

#SBATCH --job-name Cats_Mod2
#SBATCH --account landcare00015 
#SBATCH --mail-type=end
#SBATCH --mail-user=deanpa@protonmail.com
#SBATCH --time=24:00:00
#SBATCH --mem-per-cpu=3000  
#SBATCH --hint=nomultithread


module load TuiView/1.2.4-gimkl-2018b-Python-3.7.3

srun startMod2Cats.py