#!/usr/bin/env python

import os
import numpy as np
import pickle
import datetime
from manipCat import PickleCaptData

# special codes for bait type
CODE_E = 0
CODE_F = 1
CODE_RM = 2
CODE_R = 2

# special codes for trap type
CODE_CAGE = 0
CODE_CON = 1
CODE_DOC150 = 2
CODE_DOC250 = 3
CODE_FENN6 = 4
CODE_SX = 1
CODE_TIMMS = 5
CODE_VICTOR = 6

# special codes for months
CODE_jan = 1
CODE_feb = 2
CODE_mar = 3
CODE_apr = 4
CODE_may = 5
CODE_jun = 6
CODE_jul = 7
CODE_aug = 8
CODE_sep = 9
CODE_oct = 10
CODE_nov = 11
CODE_dec = 12


class RawCaptData(object):
    def __init__(self, predatorpath ):
        """
        Object to read in cat, trap data
        """
        ###########################################################
        # Run rawcaptdata functions
        self.wrapperCaptFX(predatorpath)
        self.makeSppCaptArrays()
        self.makeAvailArrays()
        self.getMonthNumeric()
        self.getTypeBaitNumeric()
        self.datesAsIntegers()
        self.getJulianArrays()


    # Functions    ###################################

    def wrapperCaptFX(self, predatorpath):
        """
        (1) wrapper function for merging capture data
        """
        self.makeEmptyArrays()
        self.readCaptData(predatorpath)

    def makeEmptyArrays(self):
        """
        (2) make empty arrays to populate
        """
        self.cDay = np.array([])
        self.cMonthTmp = np.array([])
        self.cYear = np.array([])
        self.cTrap = np.array([])
        self.cTtypeTmp = np.array([])
        self.cBaitTmp = np.array([])
        self.cSpeciesTmp = np.array([])

    def readCaptData(self, predatorpath):
        """
        (3) read in all capt data, and append into large arrays
        """
        monSeq = np.array(['01', '02', '03', '04', '05', '06', '07', '08',
                           '09', '10', '11', '12'])
        yrSeq = np.array(['2012', '2013', '2014'])
        datatype = np.array(['i8', 'S10', 'i8', 'i8', 'S10', 'S10', 
                        'S10', 'S10', 'f8', 'f8'])
        # cat capture data from monthly data files
        # loop thru years
        for i in range(3):
            if i == 0:
                # loop thru months
                for j in np.arange(6, 12):
                    monFName = 'm' + monSeq[j] + yrSeq[i] + '.csv'
                    captDatFile = os.path.join(predatorpath, monFName)
                    self.captDat = np.genfromtxt(captDatFile,  delimiter=',', 
                           dtype = datatype, names=True)
                    self.appendCaptData()
            if i == 1:
                # loop thru cMonthTmps
                for j in range(12):
                    monFName = 'm' + monSeq[j] + yrSeq[i] + '.csv'
                    captDatFile = os.path.join(predatorpath, monFName)
                    self.captDat = np.genfromtxt(captDatFile,  delimiter=',', 
                           dtype = datatype, names=True)
                    self.appendCaptData()
            if i == 2:
                # loop thru cMonthTmps
                for j in range(11):
                    monFName = 'm' + monSeq[j] + yrSeq[i] + '.csv'
                    captDatFile = os.path.join(predatorpath, monFName)
                    self.captDat = np.genfromtxt(captDatFile,  delimiter=',', 
                           dtype = datatype, names=True)
                    self.appendCaptData()


    def appendCaptData(self):
        """
        (4) append into large arrays
        """
        day = self.captDat['DAY']
        mon = self.captDat['MONTH']         
        year = self.captDat['YEAR']
        trap = self.captDat['TRAP']
        ttype = self.captDat['TTYPE']
        bait = self.captDat['BAIT']
        spp = self.captDat['SPECIES']
        # Append data from dataset to large array
        self.cDay = np.append(self.cDay, day)
        self.cMonthTmp = np.append(self.cMonthTmp, mon)
        self.cYear = np.append(self.cYear, year)
        self.cTrap = np.append(self.cTrap, trap)
        self.cTtypeTmp = np.append(self.cTtypeTmp, ttype)
        self.cBaitTmp = np.append(self.cBaitTmp, bait)
        self.cSpeciesTmp = np.append(self.cSpeciesTmp, spp)


    def makeSppCaptArrays(self):
        """
        (5) # make boolean arrays of species captures
        """
        self.cCat_Caught = self.cSpeciesTmp == b'Cat'
        self.cFerret_Caught = self.cSpeciesTmp == b'Ferret'
        self.cStoat_Caught = self.cSpeciesTmp == b'Stoat'
        self.cWeasel_Caught = self.cSpeciesTmp == b'Weasel'
        self.cHedgehog_Caught = self.cSpeciesTmp == b'Hedgehog'
        self.cShutdown = self.cSpeciesTmp == b'Shutdown'
        self.cSE_BT = (self.cSpeciesTmp == b'SE') | (self.cSpeciesTmp == b'BT')

    def makeAvailArrays(self):
        """
        (6) make availability arrays to pass to manipCats.py
        """
        ndat = len(self.cDay)
        # numeric array of trap available
        self.cTrapAvail = np.repeat(0.5, ndat)
        self.cTrapAvail[self.cSpeciesTmp == b'0'] = 1.0
#        self.cTrapAvail[self.cCat_Caught] = 1.0
        self.cTrapAvail[self.cShutdown] = 0.0

    def getMonthNumeric(self):
        """
        (7) convert strings to integer months 1 - 12
        """
        self.cMonth = np.zeros(len(self.cDay), dtype = int)
        self.cMonth[(self.cMonthTmp == b'JANUARY') | (self.cMonthTmp == b'January')] = CODE_jan
        self.cMonth[(self.cMonthTmp == b'FEBRUARY') | (self.cMonthTmp == b'February')] = CODE_feb
        self.cMonth[(self.cMonthTmp == b'MARCH') | (self.cMonthTmp == b'March')] = CODE_mar
        self.cMonth[(self.cMonthTmp == b'APRIL') | (self.cMonthTmp == b'April')] = CODE_apr
        self.cMonth[(self.cMonthTmp == b'MAY') | (self.cMonthTmp == b'May')] = CODE_may
        self.cMonth[(self.cMonthTmp == b'JUNE') | (self.cMonthTmp == b'June')] = CODE_jun
        self.cMonth[(self.cMonthTmp == b'JULY') | (self.cMonthTmp == b'July')] = CODE_jul
        self.cMonth[(self.cMonthTmp == b'AUGUST') | (self.cMonthTmp == b'August')] = CODE_aug
        self.cMonth[(self.cMonthTmp == b'SEPTEMBER') | (self.cMonthTmp == b'September')] = CODE_sep
        self.cMonth[(self.cMonthTmp == b'OCTOBER') | (self.cMonthTmp == b'October')] = CODE_oct
        self.cMonth[(self.cMonthTmp == b'NOVEMBER') | (self.cMonthTmp == b'November')] = CODE_nov
        self.cMonth[(self.cMonthTmp == b'DECEMBER') | (self.cMonthTmp == b'December')] = CODE_dec

    def getTypeBaitNumeric(self):
        """
        (8) convert strings to integer
        """
        ndat = len(self.cDay)
        self.cTTYPE = np.zeros(ndat, dtype = int)
        self.cBAIT = np.zeros(ndat, dtype = int)
        self.cTTYPE[self.cTtypeTmp == b'CON'] = CODE_CON
        self.cTTYPE[self.cTtypeTmp == b'DOC150'] = CODE_DOC150
        self.cTTYPE[self.cTtypeTmp == b'DOC250'] = CODE_DOC250
        self.cTTYPE[self.cTtypeTmp == b'FENN6'] = CODE_FENN6
        self.cTTYPE[self.cTtypeTmp == b'SX'] = CODE_SX
        self.cTTYPE[self.cTtypeTmp == b'TIMMS'] = CODE_TIMMS
        self.cTTYPE[self.cTtypeTmp == b'VICTOR'] = CODE_VICTOR
        ### convert baits
        self.cBAIT[self.cBaitTmp == b'E'] = CODE_E
        self.cBAIT[self.cBaitTmp == b'F'] = CODE_F
        self.cBAIT[self.cBaitTmp == b'R'] = CODE_R
        self.cBAIT[self.cBaitTmp == b'RM'] = CODE_RM

    def datesAsIntegers(self):
        """
        (9) convert float dates to integers
        """
        self.cMonth = np.array(self.cMonth)
        self.cYear = self.cYear.astype(int)
        self.cDay = self.cDay.astype(int)


    def getJulianArrays(self):
        """
        (10) create julian from start and julian in year arrays
        """
        # first date in dataset 1
        day1_date = datetime.datetime(2005, 12, 1)
        # first julian day in dataset 1
        day1_jul = 1
        # length of new data
        ndat = len(self.cYear)
        # empty arrays to populate
        self.cJulYear = np.zeros(ndat, dtype = int)
        self.cJulStart = np.zeros(ndat, dtype = int)
        # loop thru data to get julian array values for new data
        for i in range(ndat):
            # date of new data - entry i
            date = datetime.datetime(self.cYear[i], self.cMonth[i], self.cDay[i])
            # make tuple of date info to extract julian day in year
            timetuple = date.timetuple()
            self.cJulYear[i] = timetuple.tm_yday
            # difference in dates between first day in dataset 1 and new data
            dateDiff = date - day1_date
            dayDiff = dateDiff.days         # difference in days
            self.cJulStart[i] = dayDiff + day1_jul     # populate julian days in new data


########            Main function
#######
def main():
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    # initiate rawcaptdata class and object 
    rawcaptdata = RawCaptData(predatorpath)

    picklecaptdata = PickleCaptData(rawcaptdata)

    # pickle trap data to be used in manipCats.py
    outManipdata = os.path.join(predatorpath,'out_newCaptData.pkl')
    fileobj = open(outManipdata, 'wb')
    pickle.dump(picklecaptdata, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main()

