#!/usr/bin/env python

#import sys
import numpy as np
import os
import pickle


def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)
                    

class ProcessData(object):
    def __init__(self, predatorpath, covDatFile, lizPtFile):
        """
        Object to set initial parameters
        """

        ##################
        ## Run functions
        self.readData(covDatFile, lizPtFile)
        self.speciesParameters() 
        self.relativePredation()
        self.getLizardCellID()
        self.makeLizardCovariateArray(predatorpath)

        ## End Run functions
        ####################

    
    def readData(self, covDatFile, lizPtFile):
        """
        ## read in data for analysis
        """
        # covariate data by 1km grid cells.
        self.covDat = np.genfromtxt(covDatFile, delimiter=',', names=True,
            dtype=['f8', 'f8', 'f8'])
        self.cellX = self.covDat['x']
        self.cellY = self.covDat['y']
        self.tuss = self.covDat['hab']
        self.nCell = len(self.cellX)
        self.cellID = np.arange(self.nCell, dtype = int)
        ## get lizard location and ID data
        self.lizPtDat = np.genfromtxt(lizPtFile, delimiter=',', names=True,
            dtype=['S10', 'S10', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
        self.lizX = self.lizPtDat['easting']
        self.lizY = self.lizPtDat['northing']
        lizID = self.lizPtDat['sID']
        # decode byte data
        self.nLiz = len(self.lizX)
        self.lizID = decodeBytes(lizID, self.nLiz)
#        print('sid', self.lizID[:5], type(self.lizID))

    def speciesParameters(self):
        """
        ## get species specific parameters
        """
        self.predNames = np.array(['Cats', 'Ferrets', 'Hedgehogs', 'Stowea'])
        self.sppCoeff = np.array([[-0.005, 0.012, -0.054],
                                [0.007, 0.098, -0.121],
                                [-0.009, 0.015, -0.023], 
                                [0.043, 0.0, -0.031]])
        self.sppPeak = np.array([69.0, 80.0, 120.0, 30.0])
        self.scaleEast = (self.cellX - np.mean(self.cellX)) / np.std(self.cellX)
        self.scaleNorth = (self.cellY - np.mean(self.cellY)) / np.std(self.cellY)
        self.scaleTuss = (self.tuss - np.mean(self.tuss)) / np.std(self.tuss)
        # stack and make covariate array
        self.xdatFull = np.zeros((self.nCell, 4))
        self.xdatFull[:, 0] = self.scaleEast
        self.xdatFull[:, 1] = self.scaleNorth
        self.xdatFull[:, 2] = self.scaleTuss
        ## get covariate for Stowea
        predXDat = np.hstack([self.xdatFull[:, :3], self.xdatFull[:, :3]])
        catFerretPara = np.append(self.sppCoeff[0], self.sppCoeff[1])
        xTmp = np.dot(predXDat, catFerretPara)
        xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
        self.xdatFull[:, -1] = xTmp

        covXdat = np.zeros((self.nCell, 4))
        catR = np.dot(self.xdatFull[:, :3], self.sppCoeff[0])
        stoweaArr = np.array([0,1,3])
        stoatR = np.dot(self.xdatFull[:, stoweaArr], self.sppCoeff[-1])
        covXdat[:,0] = self.cellX
        covXdat[:,1] = self.cellY
        covXdat[:,2] = catR
        covXdat[:,3] = stoatR
        
        print('covXdat', covXdat)
        np.savetxt('covXdat.csv', covXdat, comments = '', delimiter=',',
            header = 'Eastings, Northings, catRisk, stoatRisk')




    def relativePredation(self):
        """
        ## get covariates for 4 species
        """
        stoweaArr = np.array([0,1,3])
        self.cellPredation = np.zeros((self.nCell, 8))
        for i in range(4):
            if i < 3:
                cellPred_i = np.dot(self.xdatFull[:, :3], self.sppCoeff[i])
                self.cellPredation[:, i] = (cellPred_i - np.mean(cellPred_i)) / np.std(cellPred_i)
            else:
                cellPred_i = np.dot(self.xdatFull[:, stoweaArr], self.sppCoeff[i])
                self.cellPredation[:, i] = (cellPred_i - np.mean(cellPred_i)) / np.std(cellPred_i)
            # spatial and numerical effects
            relPredEffect = (np.exp(cellPred_i) / np.sum(np.exp(cellPred_i)) * self.sppPeak[i])
#            numPredEffect = cellPred_i * self.sppPeak[i])
#            relPredEffect = (np.exp(numPredEffect) / np.sum(np.exp(numPredEffect)) * self.sppPeak[i])
            self.cellPredation[:, (i + 4)] = relPredEffect

        print('rel Pred eff', self.cellPredation[:5])
        print('mean', np.mean(self.cellPredation, axis = 0), np.std(self.cellPredation, axis = 0),
            'max', np.max(self.cellPredation, axis = 0))
        
        ## check for colinearity among covariates
        print('Covariates correlation matrix')
        print(np.round(np.corrcoef(self.cellPredation, rowvar = 0), 4))
            
    def getLizardCellID(self):
        """
        ## get cell id for lizard sampling locations
        """
        self.lizardCellID = np.zeros(self.nLiz, dtype = int)
        for i in range(self.nLiz):
            dist_i = distxy(self.lizX[i], self.lizY[i], self.cellX, self.cellY)
            mindist = np.min(dist_i)
            cellID_i = self.cellID[dist_i == mindist]
            self.lizardCellID[i] = cellID_i
        print('lizcellid', self.lizardCellID)

    def makeLizardCovariateArray(self, predatorpath):
        """
        ## make structured array of lizard data with predation covariates
        """
        structured = np.empty((self.nLiz,), dtype=[('SiteID', 'U12'), ('Eastings', np.float),
                    ('Northings', np.float),('catRisk', np.float), ('ferretRisk', np.float),
                    ('hedgehogRisk', np.float), ('stoatRisk', np.float), 
                    ('catRiskNum', np.float), ('ferretRiskNum', np.float),
                    ('hedgehogRiskNum', np.float), ('stoatRiskNum', np.float)])
        # copy data over
        structured['SiteID'] = self.lizID
        structured['Eastings'] = self.lizX
        structured['Northings'] = self.lizY
        structured['catRisk'] = self.cellPredation[self.lizardCellID,0]
        structured['ferretRisk'] = self.cellPredation[self.lizardCellID,1]
        structured['hedgehogRisk'] = self.cellPredation[self.lizardCellID,2]
        structured['stoatRisk'] = self.cellPredation[self.lizardCellID,3]
        structured['catRiskNum'] = self.cellPredation[self.lizardCellID,4]
        structured['ferretRiskNum'] = self.cellPredation[self.lizardCellID,5]
        structured['hedgehogRiskNum'] = self.cellPredation[self.lizardCellID,6]
        structured['stoatRiskNum'] = self.cellPredation[self.lizardCellID,7]
        self.lizardTableFname = os.path.join(predatorpath, 'pilotGridsPredators.txt')
        print('liz Table', self.lizardTableFname)
        fmtDat=['%s', '%.4f', '%.4f', '%.4f', '%.4f',
                    '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f']
#        headerCol = 'SiteID Eastings Northings catRisk ferretRisk hedgehogRisk stoatRisk catRiskNum ferretRiskNum hedgehogRiskNum stoatRiskNum'
#        np.savetxt(self.lizardTableFname, structured, comments = '', fmt = fmtDat)     #, header = headerCol )
#        np.savetxt(self.lizardTableFname, structured, comments = '')
        np.savetxt(self.lizardTableFname, structured, comments = '', fmt = fmtDat, 
            header = 'SiteID Eastings Northings catRisk ferretRisk hedgehogRisk stoatRisk catRiskNum ferretRiskNum hedgehogRiskNum stoatRiskNum')



    def writeTable(self, predatorpath):
        """
        ## write table with predation risk covariates to directory
        """
        # Results summary table names
        self.lizardTableFname = os.path.join(predatorpath, 'lizardPredators.txt')
        print('liz Table', self.lizardTableFname)
 

########            Main function
#######
def main():


    # set path and output file names
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')

    # paths and data to read in
    covDatFile = os.path.join(predatorpath,'covDat250.csv')
    lizPtFile = os.path.join(predatorpath,'PilotGrids.csv')

    processdata = ProcessData(predatorpath, covDatFile, lizPtFile)  




if __name__ == '__main__':
    main()
