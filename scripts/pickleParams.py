#!/usr/bin/env python

#import sys
#import numpy as np
#import os
import pickle
#import empiricalPredator
import basicsModule

import paramsCats
import paramsFerrets
import paramsHedgehogs
import paramsStowea


########            Main function
#######
def main():
    # instance of cat params
    cattmp = paramsCats.Params()
    # define class structure in basicsModule.py
    catclass = basicsModule.CatParameters(cattmp)
    fileobj = open(cattmp.ParameterFname, 'wb')
    pickle.dump(catclass, fileobj)
    fileobj.close()

    # instance of ferret params
    ferrettmp = paramsFerrets.Params()
    # define class structure in basicsModule.py
    ferretclass = basicsModule.FerretParameters(ferrettmp)
    fileobj = open(ferrettmp.ParameterFname, 'wb')
    pickle.dump(ferretclass, fileobj)
    fileobj.close()


    # instance of hedgehog params
    hedgehogtmp = paramsHedgehogs.Params()
    # define class structure in basicsModule.py
    hedgehogclass = basicsModule.HedgehogParameters(hedgehogtmp)
    fileobj = open(hedgehogtmp.ParameterFname, 'wb')
    pickle.dump(hedgehogclass, fileobj)
    fileobj.close()



    # instance of stowea params
    stoweatmp = paramsStowea.Params()
    # define class structure in basicsModule.py
    stoweaclass = basicsModule.StoweaParameters(stoweatmp)
    fileobj = open(stoweatmp.ParameterFname, 'wb')
    pickle.dump(stoweaclass, fileobj)
    fileobj.close()






if __name__ == '__main__':
    main()

