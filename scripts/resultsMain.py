#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import resultsFX
import datetime

def main(params):
    ## RESULT QUANTILES
    resultsQuantile = 0.90


    print('########################')
    print('########################')
    print('###')
    print('#    Species =', params.species)
    print('#    Scenario =', params.scenario)
    print('#    Iterations =', params.iter)
    print('#    Quantile =', resultsQuantile)
    print('########################')
    print('########################')

    ## READ IN BASICDATA PICKLE
    fileobj = open(params.basicdataFName, 'rb')
    basicdata = pickle.load(fileobj)
    fileobj.close()

    print('gibbsResults Name:', params.inputGibbs)    

    ## READ IN SIMULATE PICKLE
    fileobj = open(params.simulateFName, 'rb')
    simulate = pickle.load(fileobj)
    fileobj.close()

    ## RUN RESULTS PROCESSING MODULE
    results = resultsFX.Results(params, basicdata, simulate, resultsQuantile)


if __name__ == '__main__':
    main()


