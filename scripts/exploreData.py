#!/usr/bin/env python

import os
import numpy as np
from numba import jit
import pickle
import datetime
import paramsMod2HH
import basicsModule


 
def getTrapSummary(tna, trapped, tbt, nsess):
    """
    function to explore trapbaittype captures summaries and trapnights
    """
    tbtAll = np.tile(tbt, nsess)
    utbt = np.unique(tbt)
    print('trapNightsAvail', len(tna), tna[0:100])
    print('trapped', len(trapped), trapped[0:100])
    print('trapBaitIndx', len(tbtAll), tbtAll[0:100])
    print('utbt', utbt)
    ntbt = len(utbt)
    tnaRes = np.empty(ntbt)
    trapRes = np.empty(ntbt)
    for i in utbt:
        tnaRes[i] = np.sum(tna[tbtAll == i])
        trapRes[i] = np.sum(trapped[tbtAll == i])
    trap_tna = trapRes / tnaRes 
    ResTab = np.hstack([np.expand_dims(utbt, 1), np.expand_dims(trapRes, 1), np.expand_dims(tnaRes,1), np.expand_dims(trap_tna, 1)])

    print(ResTab)
                                




class DataWorks(object):
    def __init__(self, params, predatortrapdata, basicdata):

        self.params = params
        self.predatortrapdata = predatortrapdata

        print('self.params.species', self.params.species)


        ## RUN FUNCTIONS
        self.getTrapData()
        print('trapped', len(self.trapped), self.trapped[:200])



    def getTrapData(self):
        """
        # use indicator in params file to get species trap data
        """
        if self.params.species == 'Cats':
            self.trapped = self.predatortrapdata.cattrap
        elif self.params.species == 'Ferrets':
            self.trapped = self.predatortrapdata.ferretTrap
        elif self.params.species == 'Hedgehogs':
            self.trapped = self.predatortrapdata.hedgehogTrap
        elif self.params.species == 'Stowea':
            self.trapped = self.predatortrapdata.stoweaTrap
        nd = len(self.trapped)
        print('sum trapped', np.sum(self.trapped))
        

    def getTrapNightsFX(self):
        """
        Get number of trap nights * avail
        Get 0 and 1 array of traps that caught
        """
        self.trapNightsAvail = np.zeros(self.nsession * self.nTraps)
        self.trapTrapped = np.zeros(self.nsession * self.nTraps)            # capt captures
        for i in range(self.nsession):
            sessmask = self.trapSession == i                                # sess mask for
            captSeqTrapIDSession = self.captTrapID[self.session == i]    # sequence trap ID
            tmpTN = np.zeros(self.nTraps)                                   # empty tn arra
            tmptrapcapt = np.zeros(self.nTraps)                             # length trap d
            trappedSession = self.trapped[self.session == i]                # length capt d
            ndaySess = self.nDays[self.session == i]                        # capt session 
            # loop through capt data in session i
            cc = 0
            for j in range(self.nTraps):
                if self.trapID[j] in captSeqTrapIDSession:
#                    if not np.isscalar(ndaySess[captSeqTrapIDSession == self.trapID[j]]):
#                        print('ndaySess', ndaySess[captSeqTrapIDSession == self.trapID[j]]
#                        print('i and j', i, j)

                    tmpTN[j] = ndaySess[captSeqTrapIDSession == self.trapID[j]]     # trap 
                    tt_j = trappedSession[captSeqTrapIDSession == self.trapID[j]]   # numbe
#                    if i == 424:
#                        cc += tt_j
#                        print('tid', self.trapID[j], 'tt_j', tt_j, 'cc', cc)
                    # whether trap captured predator  
                    tmptrapcapt[j] = tt_j                                                 
            # trap nights and captures by trap and session (repeating traps for all session
            self.trapNightsAvail[sessmask] = tmpTN
            self.trapTrapped[sessmask] = tmptrapcapt
        self.trapNightsAvail_2D = np.expand_dims(self.trapNightsAvail, 1)
#        print('n - rm', self.N - self.removeDat)








########            Main function
#######
def main():

    params = paramsMod2HH.Params()

    # paths and data to read in
    if params.species == 'Hedgehogs':
        covDatFile = os.path.join(params.predatorpath,'tussockXY50m.csv')
    else:
        covDatFile = os.path.join(params.predatorpath,'covDat250.csv')



    # set path and output file names 
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # read in the pickled capt and trap data from 'manipCats.py'
    predatorTrapFile = os.path.join(params.predatorpath,'out_manipdata2.pkl')
    fileobj = open(predatorTrapFile, 'rb')
    predatortrapdata = pickle.load(fileobj)
    fileobj.close()

    # initiate basicdata from script
    basicdata = basicsModule.BasicData(predatortrapdata, covDatFile, params)



#    dataworks = DataWorks(params, predatortrapdata, basicdata)

if __name__ == '__main__':
    main()


