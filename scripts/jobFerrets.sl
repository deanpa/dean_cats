#!/bin/bash
#SBATCH --job-name FerrMod2
#SBATCH --account landcare00015 
#SBATCH --mail-type=end
#SBATCH --mail-user=deanpa@protonmail.com
#SBATCH --time=27:00:00
#SBATCH --mem=3000  
#SBATCH --hint=nomultithread

module load TuiView/1.2.4-gimkl-2018b-Python-3.7.3

srun startMod2Ferrets.py

