#!/usr/bin/env python

import os
import numpy as np
import pylab as P


class PlotData(object):
    def __init__(self, simResFile, predatorpath):
        """
        # class for plotting
        """
        self.readData(simResFile)
        self.plotData(predatorpath)

    def readData(self, simResFile):
        """
        read data from file
        """
        self.simdat = np.genfromtxt(simResFile, delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8', 'f8'])
        self.pSuppress = self.simdat['pSuppress']
        self.totalVisits = self.simdat['totalVisits']
        self.scenario = self.simdat['Scenario']  

    def plotData(self, predatorpath):
        """
        make scatter plot
        """
        # make figure
        xdat = self.totalVisits/1000
        ydat = self.pSuppress
        id = self.scenario
        fig = P.figure(figsize=(12, 8))
        ax = fig.gca()
        ax.scatter(self.totalVisits/1000, self.pSuppress, s = 150, color = 'k')
        for label, x, y in zip(id, xdat, ydat):
            ax.annotate(label, xy = (x, y), xytext = (-10, 10), textcoords = 'offset points')
        ax.set_xlabel('Total number of trap visits (x 1000)', fontsize = 15)
        ax.set_ylabel('Probability of \n population < 55', fontsize = 15, rotation = 0, ha = 'right',
                position = (200., .55))
        P.tight_layout(w_pad= 2)
        figFname = os.path.join(predatorpath, 'SimulationResultPlot.png')
        P.savefig(figFname, format='png')
        P.show()



########            Main function
#######
def main():

    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    simResFile = os.path.join(predatorpath,'SimulationResults.csv')

    plotdata = PlotData(simResFile, predatorpath)


if __name__ == '__main__':
    main()


