#!/bin/bash
#SBATCH -J Ferrets
#SBATCH -A landcare00015 
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=112:00:00
#SBATCH --mem-per-cpu=3000  
#SBATCH -C wm
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

source /landcare/sw/osgeo/osgeo.sh


######  CATS       ################
#srun startPredators.py --basicdata=$PREDATORPROJDIR/out_basicdata250Cats.pkl --predatordata=$PREDATORPROJDIR/out_predatordata250Cats.pkl
srun startPredators.py --basicdata=$PREDATORPROJDIR/out_basicdata250Cats.pkl --predatordata=$PREDATORPROJDIR/out_predatordata250Cats.pkl --checkingdata=$PREDATORPROJDIR/out_checking250Cats.pkl


######  FERRETS    ################
#srun startPredators.py --basicdata=$PREDATORPROJDIR/out_basicdata250Ferrets.pkl --predatordata=$PREDATORPROJDIR/out_predatordata250Ferrets.pkl
srun startPredators.py --basicdata=$PREDATORPROJDIR/out_basicdata250Ferrets.pkl --predatordata=$PREDATORPROJDIR/out_predatordata250Ferrets.pkl --checkingdata=$PREDATORPROJDIR/out_checking250Ferrets.pkl


######  HEDGEHOGS    ################
#srun startPredators.py --basicdata=$PREDATORPROJDIR/out_basicdata250Hedgehogs.pkl --predatordata=$PREDATORPROJDIR/out_predatordata250Hedgehogs.pkl
#srun startPredators.py --basicdata=$PREDATORPROJDIR/out_basicdata250Hedgehogs.pkl --predatordata=$PREDATORPROJDIR/out_predatordata250Hedgehogs.pkl --checkingdata=$PREDATORPROJDIR/out_checking250Hedgehogs.pkl


#####   SIMPLE STARTS   ############
#srun startPredators.py

#srun manipCat.py

