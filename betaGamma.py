#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
#import starlings
#import params
from scipy.special import gamma
from scipy.special import gammaln
#from starlings import dwrpcauchy
from scipy.stats.mstats import mquantiles


def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))



def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    Wikipedia pdf equation
    """
#    e_num = np.exp(-2*rho)
#    e_denom = 2 * np.exp(-rho)
#    sinh_rho = (1 - e_num) / e_denom
#    cosh_rho = (1 + e_num) / e_denom
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc



class G0Test(object):
    def __init__(self):

        self.gg = np.arange(.001,.99, .001)
#        self.dpdf = stats.beta.pdf(self.gg, self.params.g0_alpha, self.params.g0_beta)
#        P.plot(self.gg, self.dpdf)
#        P.show()
#        P.cla()
        mn = 0.06
        sd = 0.06
        (a, b) = getBetaFX(mn, sd)

#        mode = .03
#        a = 0.92     # 1.4 # 1.1
#        b = 22.1     # 84.38889
#        b = ((a-1.0)/mode) - a + 2.0
        print("a and b", a, b)
        print('mean beta', (a/(a+b)), 'sd =', np.sqrt(a*b / (a+b)**2 / (a+b+1)))
#        print('mode', mode) 
#        randbeta = np.random.beta(a,b,100000)
#        print('nabove', len(randbeta[randbeta>.04]), 'nbelow', len(randbeta[randbeta < .04]))
        self.dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, self.dpdf)
#        P.ylim(0., 15.0)
        P.xlim(0.001, .99)        
#        P.hist(randbeta)
        P.show()
#        P.cla()

class gammaTest(object):
    def __init__(self):

#        self.params = params
#        print(self.params.g0_alpha)
#        self.rr = np.arange(.005, 1.5, .001)
        self.rr = np.arange(.01, 5.0, .01)
        mode = 2.25
#        sh = 9.929    # 4. # 3.0    # 2.8        # 2.8750    # .001
        sc = 0.15   #.14 #.5   #.8 # .2 #1.0   # 0.8    # 1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667  #3.    
        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
#        print('mode', (sh - 1.0) * sc)
#        sc = 3.0     # 1.5     # .66667    
#        rate = 1.0/sc    #0.5  #40.0     #1.1      #.5 #4  #1.      #2
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/rate !!!
        randg = np.random.gamma(sh, sc, size = 2000)          # use shape and scale
        print('mean randg', np.mean(randg), 'sd randg', np.std(randg))
        print("mean", sh*sc, "sd", np.sqrt(sh*sc**2), "mode", (sh-1.)*sc)
#        print("parameter calc mean", sh * sc)
#        print("max LL r", self.rr[self.dpdf==np.max(self.dpdf)], ((sh-1)*sc))
#        print("max pdf", self.dpdf[self.dpdf==np.max(self.dpdf)])
        
        P.figure()
#        P.subplot(1,2,1)
#        P.hist(randg)
#        P.subplot(1,2,2)
        P.plot(self.rr, self.dpdf)
#        P.ylim(0, 0.5)
        P.show()
#        P.cla()

class wrpCTest(object):
    def __init__(self):
        days = np.arange(365)
#        aa = days/364 * 2. 
        aa = np.arange(0, (np.pi*2), .05)
        mu = 3.0    #-1.5 # 1.4    # np.pi*2
        rho = 4.0   # .44   # 3.   #.7
        dwrpc = dwrpcauchy(aa, mu, rho)
        st_dwrpc = (dwrpc/np.max(dwrpc)) * 0.05
#        print(dwrpc)
#        print('st_dwrpc', st_dwrpc)
        P.figure(0)
        P.plot(aa, dwrpc)
#        P.plot(aa, st_dwrpc, color = 'r')
#        P.ylim(0, 1.0)
        P.show()
        P.cla()

class normalBeta(object):
    def __init__(self):
        M = .4
        V = .01
        dd = logit(np.arange(.001, 1, .001))
        ddpdf = stats.norm.pdf(dd, logit(M), .5)
        l_mu = np.random.normal(logit(M), .05, 2000)
        mu = inv_logit(l_mu)
#        print(np.std(mu))
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(mu)
        P.xlim(0,1.0)
        P.subplot(1,2,2)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
#        P.show()
#        P.cla()
        

class normalFX(object):
    def __init__(self):
        M = 250.
        S = 60.
        dd = np.arange(30, 470)
        ddpdf = stats.norm.pdf(dd, M, S)
        P.figure(0)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
        P.show()

        


class Poisson(object):
    def __init__(self):
        dat = 91
        datGammaLn = gammaln(dat + 1)
        ll = 178.002847241 

        lpmf = self.ln_poissonPMF(dat, datGammaLn, ll)

        pois = stats.poisson.logpmf(dat, ll)
#        print('lpmf', lpmf, 'pois', pois)

    def ln_poissonPMF(self, dat, datGammaLn, ll):
        """
        calc log pmf for poission
        """
        ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
        return(ln_ppmf)



class halfNorm(object):
    def __init__(self):
        dat = np.sqrt(50)
#        dat = np.arange(400.0)
        sig = 90.0
        HNpdf = stats.halfnorm.pdf(dat,loc=0.0, scale = sig)
        
        print('hnpdf', np.sum(HNpdf))
        P.figure()
        P.plot(dat, HNpdf)
        P.show()


class multiVNorm(object):
    def __init__(self):
        dat = np.array([5., 5.])
#        dat = np.arange(400.0)
        dat = np.zeros((10, 2))
        sig = 90.0
        sigMat = np.expand_dims(sig, 1)
        ndat = len(dat)
        ndat = 6
        covMat = np.identity(ndat) * sigMat
        meanMat = np.zeros(ndat)
#        MVpdf = stats.multivariate_normal.logpdf(dat, mean = meanMat, cov = covMat)
        rMV1 = np.random.multivariate_normal(meanMat, covMat, size = 1)
        rMV2 = np.random.multivariate_normal(meanMat, covMat, size = 1)
        distmv = np.sqrt(rMV1**2 + rMV2**2)
        print('distmv', np.mean(distmv), np.std(distmv), np.max(distmv))  
        print('covMat', covMat, 'rMV1', rMV1)
#        print('mvpdf', np.sum(MVpdf))
#        P.figure()
#        P.plot(dat, MVpdf)
#        P.hist(rMV1)
#        P.show()



class circHR(object):
    def __init__(self):
        normMean = 0.0
        normSig = 90.0
        n = 20000
        xnorm = np.random.normal(normMean, normSig, n)
        ynorm = np.random.normal(normMean, normSig, n)

        dat 
        LLxy

        normDist = np.sqrt(xnorm**2 + ynorm**2)
        normSD = np.std(normDist)
        print('normSD', normSD)
        normQuant = mquantiles(normDist, prob=[0.95, 0.99])

        print('norm 95%', normQuant, normQuant/normSig)


        distHN = stats.halfnorm.rvs(loc = 0., scale = normSig, size =n)
        HNSD = np.std(distHN)
        print('HNSD', HNSD)
        HNQuant = mquantiles(distHN, prob=[0.95, 0.99])

        print('HN 95%', HNQuant, HNQuant/normSig)


class betadist(object):
    def __init__(self):
        m = .5
        s = .4
        (a,b) = getBetaFX(m,s)
        rbeta = np.random.beta(a,b,1000)
        print('mean beta', np.mean(rbeta))
        P.figure()
        P.hist(rbeta)
        P.show()


########            Main function
#######
def main():

#    betadist()
    G0Test()
#    gamtest = gammaTest()
#    wrpCTest()
#    normalBeta()
#    normalFX()
#    Poisson()
#    halfNorm()
#    multiVNorm()
#    circHR()

if __name__ == '__main__':
#    PP = params.Params()
    main()

