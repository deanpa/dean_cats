#!/usr/bin/env python

import os
import numpy as np
import pylab as P
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.stats.mstats import mquantiles
import numpy.ma as ma
from scipy.stats.mstats import mquantiles


class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        #######################################################
        #######################################################
        self.sigma = 90.0
        self.g0 = 0.13
        self.trapTestSe = 0.95
        self.trapNights = 4.0
        # max probability of interaction and deposition of DNA
        self.d0 = 0.15
        # DNA - test sensitivity
        self.seDNA = np.linspace(0.98, 0.5,  num = 10)
        # time that TB DNA remains intact     
        self.timeTB = np.arange(5.0, 51.0, step = 5.0)
        # cost of trap and DNA device deploy and diagnostics
        self.costDNA = 25.00
        self.costTrap = 25.00
        
        #######################################################
        #######################################################

class ProcessObj(object):
    def __init__(self, params):
        """
        Object to calculate sensitivities
        """
        self.params = params

        #########################
        #########   Run functions
        self.makeLocations()
        self.distxy()
        self.predictFX()
        self.calcOpEff()
        self.getImPlotLabels()
        self.plotOperEff()


        ########    End running functions
        #################################



    def makeLocations(self):
        """
        # make trap location and cell x and y data
        """
        x1 = np.arange(0.0, 1100.0, step = 100.)
        self.x = np.tile(x1, len(x1))               # columns
        y1 = np.arange(0.0, 1100.0, step = 100.)
        self.y = np.repeat(y1, len(y1))             # rows
#        tmpArr = np.zeros((len(self.x), 2))
#        tmpArr[:,0] = self.y
#        tmpArr[:,1] = self.x
#        for i in range(len(self.x)):
#            print(tmpArr[i])
        # Trap location
        self.trapLoc = np.array([np.mean(self.y), np.mean(self.x)])

    def distxy(self):
        """
        calc distance between trap and grid points
        """
        self.dist = np.sqrt(np.power(self.trapLoc[0] - self.x, 2) + 
                    np.power(self.trapLoc[1] - self.y, 2))
        self.distMask = self.dist < (self.params.sigma * 4.0)
        self.distDetect = self.dist[self.distMask]
#        print(self.distDetect)

    def pDetectFX(self, g0, nights, testSe):
        """
        calc prob detect given seDNA and time life of DNA
        """
        pNoTBCaptureCell = 1.0 - (g0 * np.exp(-(self.dist**2) / 2 / (self.params.sigma**2)))
        pTBCaptureCellNights = 1.0 - pNoTBCaptureCell**nights
        meanCellSe = np.mean(pTBCaptureCellNights)
        SSe = meanCellSe * testSe
        return(SSe)


    def predictFX(self):
        """
        calc prob detect across seDNA and time life of DNA, and method
        """
        self.nTimeTB = len(self.params.timeTB)
        self.nSeDNA = len(self.params.seDNA)
        # result storage array
        self.SSe_2D = np.zeros((self.nTimeTB, self.nSeDNA))
        # calc trap pdetect
        self.tbDetectTrap = self.pDetectFX(self.params.g0, self.params.trapNights, 
            self.params.trapTestSe)
        ## loop thru DNA time and se values
        # loop thru time columns of 2-D array i
        for i in range(self.nTimeTB):
            for j in range(self.nSeDNA):
                self.SSe_2D[j, i] = self.pDetectFX(self.params.d0, 
                    self.params.timeTB[i], self.params.seDNA[j])

    def calcOpEff(self):
        """
        calc operational efficiency of traps and DNA
        """
        self.opEffDNA = self.SSe_2D / self.params.costDNA 
        self.opEffTrap = self.tbDetectTrap / self.params.costTrap
        self.relativeOpEff = self.opEffDNA / self.opEffTrap

        nAbove1 = len(self.relativeOpEff[self.relativeOpEff < 1.0])
        nTot = (self.nTimeTB * self.nSeDNA)
        self.percentile = nAbove1 / nTot
        print(nAbove1, nTot)
        print('percent', self.percentile)
        print(self.relativeOpEff)

    def getImPlotLabels(self):
        """
        get NPV and TTE axes and labels
        """
        self.xticks = self.params.timeTB.copy()
        self.yticks = self.params.seDNA * 100.
        self.brx = np.max(self.xticks)
        self.tlx = np.min(self.xticks)
        self.tly = np.max(self.yticks)
        self.bry = np.min(self.yticks)
        self.extent = ([(self.tlx - 0.05), (self.brx + 0.05),
            (self.bry - 0.025), (self.tly + 0.025)])
        print('xticks', self.xticks, self.yticks, self.extent)


    def plotOperEff(self):
        """
        plot 2-D image of source population
        """
        P.figure(figsize=(10,10))
        minRast = np.min(self.relativeOpEff)
        maxRast = np.max(self.relativeOpEff)
        raster = self.relativeOpEff.copy()
#        raster_masked_array = np.ma.masked_where(np.isnan(raster), raster)
        cmap = P.cm.jet
        cmap.set_bad('w', 1.)
        ax = P.gca()
        P.xticks(self.xticks, fontsize = 12)
        P.yticks(self.yticks, fontsize = 12)
        # image plot - all elements arguments are important
        imgplot = ax.imshow(raster, cmap=cmap, extent = self.extent,
            interpolation = 'none')
        quantLevels = mquantiles(raster, prob=[self.percentile])
        ContLines = P.contour(raster, levels = quantLevels, linewidths=2, extent = self.extent,
           origin = 'upper', colors='k')
        P.title('Relative operation efficiency of DNA detection', fontsize=12)
        P.xlabel('Field deployment time (days)', fontsize = 14)
        P.ylabel('DNA-device diagnostic sensitivity (%)', fontsize = 14)
        imgplot.set_clim(minRast, maxRast)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="3%", pad=0.075)
        cbar = P.colorbar(imgplot, cax=cax)
        imgplot.set_clim(0.0, maxRast)
        cbar.ax.tick_params(labelsize=10)
        P.savefig("RelOpEffDNA.png", format='png')
        P.show()


########            Main function
#######
def main():
    params = Params()
    processobj = ProcessObj(params)


if __name__ == '__main__':
    main()

