#!/usr/bin/env python

#import sys
#import defol
import numpy as np

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        ##############
        ###############      Initial values parameters for updating
        self.sigma = 310.0
        self.sigma_s = 309.9
        self.sigma_mean = 300
        self.sigma_sd = np.sqrt(500)
        self.sigma_search_sd = 6
#        a and b 1.7222222222222223 84.38888888888889

        self.g0_alpha = 1.1  # 1.722   # 1.124   #1.145    #1.43     # 0.91    #3.9 # .095    # 1.0    # beta prior for g0
        self.g0_beta = 3.4   # 84.38889 #36.355   #27.48    #27.25    # 14.29    #191.1    #37.05    #15.667    # beta prior for g0
        self.g0Sd = 0.01          # search parameter for g0
        self.g0Multiplier = np.array([0, -.3, .11])
        self.g0MultiPrior = np.array([0, 5.0])
        self.nTopTraps = 26.0      # limit prob of capt by this number of traps

        self.ig = 75      #/365.0*7.0
        self.i_s = 74     #/365.0*7.0
        # uniform prior between 20 and 300
        self.imm_mean = 100         # 5  shape
        self.imm_sd = 50         # 15 scale

        ###################
        ##################      # Reproduction parameters
        self.rg = .650
        self.rs = .70
        self.r_shape = 2.0  # 1.5  #3.0        #0.1  # 4.0     # gamma growth rate priors
        self.r_scale = 0.8  # 2.0  #4.0        #0.1  # 4.0
#        self.recruitHalfWindow = 6.0/12*2*np.pi    # 12 months total.
        # latent initial number of recruits ~ uniform(10, 100)
        self.initialReproPop = 34.0
        self.IRR_priors = np.array([4, 120])

        # wrp cauchy parameters for distributing recruits
        self.rpara = np.array([np.pi*2/3, .1])                # real numbers: normally distributed
        self.rpara_s =  np.array([np.pi*2/3.1, .09])          # greater than 0: gamma distributed
        # wrapped cauchy parameters: mu priors from normal, rho from a gamma 
        self.mu_wrpc_Priors = np.array([3.0, 10.0])    # np.array([np.pi*2/3, 3]) #normal priors mu    
        self.rho_wrpc_Priors = np.array([0.001, 1000.0])     #np.array([3.0, 0.33333]) #gamma priors for rho    
        
        ######################################
        ######################################
        # modify  variables used for habitat model
        self.xdatDictionary = {'scaleEast' : 0, 'scaleNorth' : 1, 'scaleTuss' : 2}
        self.scaleEast = self.xdatDictionary['scaleEast']
        self.scaleNorth = self.xdatDictionary['scaleNorth']
        self.scaleTuss = self.xdatDictionary['scaleTuss']
        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.scaleEast, self.scaleNorth, self.scaleTuss], dtype = int)    
        ######################################
        ######################################

        self.b = np.array([.004, .06, -.10])
        # beta priors on habitat coefficients
        self.bPrior = 0.0
        self.bPriorSD = np.sqrt(1000)
        self.nbcov = len(self.b)
        self.maxN =  360
#        # sd for [N | Npred]
#        self.sdN = .2
#        self.prior_sdN = np.array([.1, .1])         # gamma priors
        # set days on which to base population growth rate
        self.reproDays = range(365-61, 365-30)         # 1 Nov - 30 Nov
        # set days for low and high g0 values
        self.g0LowDays = np.array([365-116, 365])         # days >= 5 Sept to 31 Dec
        self.g0HighDays = np.array([58, 160])                # March - 10 June

        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 2500       # number of estimates to save for each parameter
        self.thinrate = 2        # thin rate
        self.burnin = 0             # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################


