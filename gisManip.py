#!/usr/bin/env python


# IMPORT MODULES
import os
import numpy as np
import pickle
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdalconst
#from gdalconst import *
#from numba import jit



#### GDAL COMMAND LINE FUNCTIONS:

# Get infor on layer in shapefile
# ogrinfo -so $PREDATORPROJDIR/analysisExtent.shp 

# MAKE GLOBAL MASK  [-te xmin ymin xmax ymax]
# gdal_rasterize -tr 250 250 -te 2300195.37 5518297.52 2313239.355 5532373.38 -ot Byte -init 0 -burn 1 analysisExtent.shp extentMaskR250.tif

def getSpatialRef(srcRaster):
    """
    get x-y min and max, ncols and rows, and resol of raster
    """
    (ulx, xres, xskew, uly, yskew, yres)  = srcRaster.GetGeoTransform()
    lrx = ulx + (srcRaster.RasterXSize * xres)
    lry = uly + (srcRaster.RasterYSize * yres)
    # ulx, uly is the upper left corner, lrx, lry is the lower right corner
    return(ulx, uly, lrx, lry, xres, yres)

def getColsAndRows(easting, northing, MIN_X, MAX_Y, RESOL):
    """
    given an array of eastings and an array of northings
    return the cols and rows
    """
    col = np.round((easting - MIN_X) / RESOL).astype(np.int)
    row = np.round((MAX_Y - northing) / RESOL).astype(np.int)
    return(col, row)


class RawData():
    def __init__(self, predatorpath, extentFName, tussFName):
        """
        Read in data and do one-time manipulations
        """
        # set constants
        # Extent: (2300195.370784, 5518297.520864) - (2313239.355206, 5532373.382349)

        self.RES = 250.0 # in metres
        self.MIN_X = 2300195.37 
        self.MAX_X = 2313239.355 
        self.MIN_Y = 5518297.52 
        self.MAX_Y = 5532373.38 
        #
        self.MIN_X_PT = 2300195.37 + (self.RES / 2.0)
#        self.MAX_X_PT = 2313239.355 + (RES / 2.0)
#        self.MIN_Y_PT = 5518297.52 - (RES / 2.0)
        self.MAX_Y_PT = 5532373.38 - (self.RES / 2.0)

        ##########################################
        # RUN FUNCTIONS
        self.readData(predatorpath, extentFName, tussFName)
        self.getXY()
        self.makeWriteTxt(predatorpath)
        
    ############################
    # Class functions
    def readData(self, predatorpath, extentFName, tussFName):
        """
        read in data in tif format
        """
        extFN =  os.path.join(predatorpath, extentFName)
        self.tussFN =  os.path.join(predatorpath, tussFName)
        self.extMask = gdal.Open(extFN).ReadAsArray()
        tmpMask = gdal.Open(extFN)
        (ulx, uly, lrx, lry, xres, yres) = getSpatialRef(tmpMask)
        nr = (lrx - ulx) / xres
        nc = (uly - lry) / xres
        print('spatRefExtent', 'ulx', ulx, 'uly', uly, lrx, lry, 'xres', xres, yres,
            'nr', nr, 'nc', nc)
        self.tuss500 = gdal.Open(self.tussFN).ReadAsArray()
        (self.rows, self.cols) = self.extMask.shape
        print('shp', self.extMask.shape, 'rowsCols', self.rows, self.cols,
            'shp tuss', self.tuss500.shape)

    def getXY(self):
        """
        get x y data for covariate points.
        """
        self.nPoints = np.sum(self.extMask)
        print('ext', np.sum(self.extMask), self.extMask[0], 'minx', 
            self.MIN_X_PT, 'npoints', self.nPoints)
        self.x = np.empty(self.nPoints)
        self.y = np.empty(self.nPoints)
        self.tuss = np.empty(self.nPoints)
        tuss1K = gdal.Open(self.tussFN)
#        print('meta', tuss1K.GetGeoTransform())
        (ulx, uly, lrx, lry, xres, yres) = getSpatialRef(tuss1K)
        nr = (lrx - ulx) / xres
        nc = (uly - lry) / xres
#        print('spatRef', 'ulx', ulx, 'uly', uly, lrx, lry, 'xres', xres, yres, 
#            'nr', nr, 'nc', nc)
        del tuss1K
        cc = 0
        for i in range(self.rows):
            for j in range(self.cols):
                if self.extMask[i, j] == 1:
                    self.x[cc] = self.MIN_X_PT + (j * self.RES)
                    self.y[cc] = self.MAX_Y_PT - (i * self.RES)
                    (col, row) = getColsAndRows(self.x[cc], self.y[cc], ulx, uly, xres)
                    self.tuss[cc] = self.tuss500[row, col]
#                    if (i < 30) and (i>25):
#                        print('x', self.x[cc], 'y', self.y[cc], 'col', col, 'row', row, 'tuss', self.tuss[cc])
                    cc += 1
#        print('x', self.y[:30])
#        del tuss1K


    def makeWriteTxt(self, predatorpath):
        """
        make structured array and write to directory
        """
#        tuss250 = self.tuss.astype(np.int)
        covDat250 = np.empty(self.nPoints, dtype=[('x', 'f8'), ('y', 'f8'), ('tuss250', 'i8')])        
        covDat250['x'] = self.x
        covDat250['y'] = self.y
        covDat250['tuss250'] = self.tuss
        txtName = os.path.join(predatorpath, 'covDat250.csv')
        np.savetxt(txtName, covDat250, delimiter=',',
             header='x, y, hab', comments='')


########            Main function
#######
def main():
    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    extentFName = 'extentMaskR250.tif'
    tussFName = 'tuss500.tif'

    rawdata = RawData(predatorpath, extentFName, tussFName)

if __name__ == '__main__':
    main()

