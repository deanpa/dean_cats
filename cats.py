#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
#from numba import jit
import params
import pickle


def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

#@jit
def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def initialPPredatorTrapCaptFX(basicdata, availTrapNights, location, g0Param, debug = False):      
    """
    # prob that predator was capt in trap
    """
    distToTraps = basicdata.distTrapToCell2[:, location]      # dist from cell j to all traps
    eterm = np.exp(-(distToTraps) / basicdata.var2)           # prob predator-trap pair
    pNoCapt = 1. - g0Param * eterm
    pNoCaptNights = pNoCapt**(availTrapNights)
    pNoCaptNights[pNoCaptNights >= .9999] = 0.9999
    pcapt = 1 - pNoCaptNights
    pcapt[pcapt > .97] = 0.97
    if debug == True:
        print("basicdata.distTrapToCell2.shp", basicdata.distTrapToCell2.shape)
        print("distToTraps.shp", distToTraps.shape)
    return pcapt


def removeDatFX(nsession, predator, session):
    removeDat = np.arange(nsession)
    for i in range(nsession):
        removeDat[i] = np.sum(predator[session==i])
    return(removeDat)


def multinomial_pmf(probs, counts):
    probssum = probs.sum()
    if probssum < 0.999 or probssum > 1.0001:
        raise ValueError("probs must sum to 1")
    if probs.size != counts.size:
        raise ValueError("probs and counts must be the same size")
    return gammaln(counts.sum() + 1.0) - gammaln(counts + 1.0).sum() + np.sum(np.log(probs)*counts)


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    """
    e_num = np.exp(-2*rho)
    e_denom = 2 * np.exp(-rho)
    sinh_rho = (1 - e_num) / e_denom
    cosh_rho = (1 + e_num) / e_denom
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc



def calcRelWrapCauchy(wrp_rpara, nsession, uYearIndx, daypi, yearRecruitIndx):
    """
    Calc the rel cauchy value for all sessions for distributing recruits
    """
    relWrpCauchy = np.zeros(nsession)
    for i in uYearIndx:
        # day pi in year i
        daypiTmp = daypi[yearRecruitIndx == i]
        # pdf of daypi in year i
        dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
        # mask of recruit window in year i
        reldc = dc / np.sum(dc)     #  dc.sum()
        relWrpCauchy[yearRecruitIndx == i] = reldc
    return relWrpCauchy
        
def g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll):
    logit_g0 = logit(g01)
    g0All_3 = g0All.copy()
    for k in range(nsession):
        sessmask = trapSession == k                            # sess mask for trap data
        # get logit g0 for that bait-trap by season effect
        logit_g0All_1 = logit_g0 + g0MultiplierAll[k]
        g0All_2 = inv_logit(logit_g0All_1)
        g0All_3[sessmask] = g0All_2
    return g0All_3

def g0AllFX(g0_2D, g0All, trapBaitIndx, nsession, trapSession, g0MultiplierAll):
    """
    make a long array of g0 values associated with trap data for each bait trap type
    and repeat for each session, with season multiplier
    """
    # expand by trap-bait type to length of self.nTraps
    g01 = g0_2D[trapBaitIndx]
    # loop thru sessions
    g0All_3 = g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll)
    return g0All_3



def getTrapSummary(tna, trapped, tbt, nsess):
    """
    function to explore trapbaittype captures summaries and trapnights
    """
    tbtAll = np.tile(tbt, nsess)
    utbt = np.unique(tbt)
    print('trapNightsAvail', len(tna), tna[0:100])
    print('trapped', len(trapped), trapped[0:100])
    print('trapBaitIndx', len(tbtAll), tbtAll[0:100])
    print('utbt', utbt)
    ntbt = len(utbt)
    tnaRes = np.empty(ntbt)
    trapRes = np.empty(ntbt)
    for i in utbt:
        tnaRes[i] = np.sum(tna[tbtAll == i])
        trapRes[i] = np.sum(trapped[tbtAll == i])
    trap_tna = trapRes / tnaRes 
    ResTab = np.hstack([np.expand_dims(utbt, 1), np.expand_dims(trapRes, 1), np.expand_dims(tnaRes,1), np.expand_dims(trap_tna, 1)])
    print(ResTab)
                                


class BasicData(object):
    def __init__(self, predatortrapdata, covFname, params):
        """
        Object to read in cat, trap and covariate-cell data
        Import updatable params from params
        """
        self.params = params

        self.predatortrapdata = predatortrapdata
        # move predatortrapdata variables into basicdata
        self.session = self.predatortrapdata.week - 1
        self.session = self.session.astype(int)
        self.captTrapID = self.predatortrapdata.captTrapID
        self.julianStart = self.predatortrapdata.julianStart
        self.julianYear = self.predatortrapdata.julianYear
        self.captTrapBait = self.predatortrapdata.ttypebait
        self.ttype = self.predatortrapdata.ttype
        self.trapped = self.predatortrapdata.cattrap
        self.nDays = self.predatortrapdata.nDays
        self.year = self.predatortrapdata.year
        # passed in here to pass on to gibbsobj pickle for postProcessing
        self.fortnightMask = self.predatortrapdata.fortnightMask
        self.sessionIntervalMask = self.predatortrapdata.sessionIntervalMask
        ########################################## session data for results
        self.sessionYear = self.predatortrapdata.sessionYear
        self.sessionMonth = self.predatortrapdata.sessionMonth
        self.sessionDay = self.predatortrapdata.sessionDay
        ###########################################  Trap data
        # data associated with trap location data
        self.trapX = self.predatortrapdata.trapX
        self.trapY = self.predatortrapdata.trapY
        self.trapID = self.predatortrapdata.trapTrapid
        # number of traps
        self.nTraps = self.predatortrapdata.nTraps
        self.trapBaitIndx = self.predatortrapdata.trapBaitIndx   # index for applying g0 across trap-bait types

#        self.testCapt_TrapData()

        #############################################
        # new associated variables
        self.uSession = np.unique(self.session)
        self.nsession = len(self.uSession)   # np.int(max(self.session + 1))
        self.ndat = len(self.session)
        self.uniqueCaptTrapBait = np.unique(self.captTrapBait)  # unique trap-bait combos (16)
        self.nTrapBait = len(self.uniqueCaptTrapBait)       # number of combos (16)
        self.uJulian = np.unique(self.julianYear)
        self.zeroPCapt = np.zeros(self.nTraps)              # template of zeros to get pcapt in 3 traps

#        print('trapped', np.sum(self.trapped), self.trapped[self.trapped>0])

        ###################################################
        ################################################          Grid-cell covariate data
        # covariate data by 1km grid cells.
        self.covDat = np.genfromtxt(covFname, delimiter=',', names=True,
            dtype=['f8', 'f8', 'f8'])
        self.cellX = self.covDat['x']
        self.cellY = self.covDat['y']

        self.eastCov = self.cellX - np.min(self.cellX)
        self.northCov = self.cellY - np.min(self.cellY)
        self.tuss = self.covDat['hab']
        self.ncell = len(self.eastCov)

        # Covariates for habitat model       
        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)
        self.scaleTuss = (self.tuss - np.mean(self.tuss)) / np.std(self.tuss)

        # stack and make covariate array
        xdatFull = np.hstack([np.expand_dims(self.scaleEast,1),
                            np.expand_dims(self.scaleNorth,1),
                            np.expand_dims(self.scaleTuss,1)])
        # select covariates using index from params
        selectCov = xdatFull[:, self.params.xdatIndx]
        # 2-d array of covariates
        self.xdat = selectCov.copy()
        # number of beta parameter, including intercept
        self.nbcov = self.params.nbcov

        ##############
        ###############      Initial values parameters from params.py for updating
        ##############       Have in basicdata so can update, and import pickle
        # maximum number of predators from params
        self.maxN = self.params.maxN
        # sigma home range parameter
        self.sigma = self.params.sigma
        self.sigma_s = self.params.sigma_s

        ###################  g0 preparation

        # Make "self.startJulSess": starting julian day for each week-session
        # Make "self.yearSess" : year associated with each session
        self.getWeekJulian()

        # make "self.g0_Indx_Multiplier" array indexing where high, low and standard g0-multipliers applies
        self.make_g0_Indx()

        # array of 3 for each season
        self.g0Multiplier = self.params.g0Multiplier                            # update
        self.g0Multiplier_s = self.g0Multiplier.copy()
        self.g0Multiplier_s[1:] = np.random.normal(self.g0Multiplier[1:], .001)
        # gamma priors on multiplier by season (1,1)

        # make array length nSession of multiplier by season
        self.g0MultiplierAll = self.g0Multiplier[self.g0_Indx_Multiplier]       # update when update self.g0Multiplier
        self.g0MultiplierAll_s = self.g0Multiplier_s[self.g0_Indx_Multiplier]

        # g0 capture parameter
        self.g0 = np.exp(np.random.normal(np.log(.02),.01, size = self.nTrapBait))
        self.g0 = np.expand_dims(self.g0, 1)

        # make indexing array for sessions that corresponds to trap data
        self.gettrapSession()

        # array length of nsession * ntraps  with g0 for the associated trap-bait type
        # incorporates seasonal effect
#        self.g0All =  np.zeros(self.nsession * self.nTraps)
        self.g0All =  np.expand_dims(np.zeros(self.nsession * self.nTraps), 1)
        self.g0All = g0AllFX(self.g0, self.g0All, self.trapBaitIndx, self.nsession, 
                        self.trapSession, self.g0MultiplierAll)

        # proposed g0_s
        self.g0_s = np.exp(np.random.normal(np.log(self.g0),.005))
        self.g0All_s =  np.expand_dims(np.zeros(self.nsession * self.nTraps), 1)
        self.g0All_s = g0AllFX(self.g0_s, self.g0All_s, self.trapBaitIndx, self.nsession, 
                        self.trapSession, self.g0MultiplierAll)

        # immigration and mortality parameter
        self.ig =  self.params.ig
        self.i_s =  self.params.i_s

        ##########################################
        #########################################   # wrp cauchy parameters for reproduction

        # growth rate parameter from params.
        self.rg = self.params.rg        
        self.rs = self.params.rs
        self.initialReproPop = self.params.initialReproPop
        self.initialReproPop_s = self.params.initialReproPop - 1

        # wrapped cauchy parameters and function for distributing new recruits
        self.rpara = self.params.rpara
        self.rpara_s =  self.params.rpara_s
        # mask of daypi temporal window to distribute new recruits

        ###### Predict new recruits
        # days in radians
        self.daypi = self.startJulSess / 365 * 2 * np.pi

        # Make "self.reproMask" mask  for identifying reproductive period
        # and "self.reproPeriodIndx" for indexing reproductive periods
        self.makeReproMask()
#        print('reproMask', self.reproMask, len(self.reproMask))
        
        # make index for year to distribute new recruits: "self.yearRecruitIndx "
        self.makeYearRecruitIndx()
        self.uYearIndx = np.unique(self.yearRecruitIndx)    # unique year recruitment indices

#        self.maxUYearIndx = np.max(self.uYearIndx)          # max indx for recruits in 12/2014 
        self.nYear = len(self.uYearIndx)                    # number of years for reproduction
        # number of sessions in year 0. use to update latent initial repro pop.
        self.nSessYr0 = len(self.yearRecruitIndx[self.yearRecruitIndx == 0]) 

        # make array self.nSessReproPeriod used for calc mean repro pop
        self.getNSessInReproPeriod()

        # relative wrapped cauchy function for recruitment periods for all years    
        self.relWrpCauchy = np.zeros(self.nsession, dtype = int)        
        self.relWrpCauchy = calcRelWrapCauchy(self.rpara, self.nsession, self.uYearIndx, 
                            self.daypi, self.yearRecruitIndx)       #, self.recruitWindowMask)
        self.relWrpCauchy_s = calcRelWrapCauchy(self.rpara_s, self.nsession, self.uYearIndx, 
                            self.daypi, self.yearRecruitIndx)       #, self.recruitWindowMask_s)

        # get removeDat per week session
        self.removeDat = removeDatFX(self.nsession, self.trapped, self.session)

        # get initial N and associated recruitment values
        self.getInitialN()

#        print('N', self.N)
#        print('Ns', self.Ns)

        self.cellID = np.arange(self.ncell, dtype = int)
        self.nCatInCellTemplate = np.zeros(self.ncell)

        # make arrays associated with trap data
#        self.getTrapIDFX()
        self.getTrapNightsFX()

        # make matrix of distance from traps (rows) and cells (col)
        distTrapToCell = distmat(self.trapX, self.trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0

        # calc initial Npred and Npred_s for all sessions
        self.Npred = self.NpredAllSessFX(self.N)
        self.Npred_s = self.NpredAllSessFX(self.Ns) 

#        print('npred', self.Npred)
#        print('npred_s', self.Npred_s)
#        print(self.removeDat.dtype, self.N.dtype)

        #######################
        # habitat beta parameters and initial mu and Th values
        self.b = self.params.b
        self.lth = np.dot(self.xdat, self.b)
#        self.lth = np.random.normal(self.mu.flatten(), .5, self.ncell)
        self.thMultiNom = thProbFX(self.lth, debug = False)

        self.bs = np.random.normal(self.b, .02)
        self.lth_s = np.dot(self.xdat, self.bs)
        self.thMultiNom_s = thProbFX(self.lth_s)

#        print('self.N', self.N)

        # habitat variance parameter
        self.nsample = np.array([-3, -2, -1, 1, 2, 3])
        self.datseq = np.arange(self.params.maxN, dtype = int)
        self.var2 = 2.0 * (self.params.sigma**2.0)
        self.var2_s = 2.0 * (self.params.sigma_s**2.0)
        self.llikTh = np.empty(self.nsession)
        self.llikTh_s = np.empty(self.nsession)
        self.llikR = np.empty(self.nsession - 1)
        self.llikR_s = np.empty(self.nsession - 1)
        self.llikImm = np.empty(self.nsession - 1)
        self.llikImm_s = np.empty(self.nsession - 1)
        self.llikg0Sig = np.empty(self.nsession)
        self.llikg0Sig_s = np.empty(self.nsession)
        self.expTermMat =  np.exp(-(self.distTrapToCell2) / self.var2)
        self.expTermMat_s =  np.exp(-(self.distTrapToCell2) / self.var2_s)
        
        ##############
        ##############      # end updatable parameters



    ####################     # Basicdata functions.
    ####################


    def testCapt_TrapData(self):
        """
        # test to see if have perfect coverage between
          trap data and capture data
        """
        ucapt = np.unique(self.captTrapID)
        utrap = np.unique(self.trapID)
        ct = np.in1d(ucapt, utrap)
        print('len capt', len(ucapt), 'n Trues', np.sum(ct))
        tc = np.in1d(utrap, ucapt)
        print('len trap', len(utrap), 'n Trues', np.sum(tc))

    def getWeekJulian(self):
        """
        Get starting julian day and year for each week-session
        Use to make mask for reproductive period
        """
        self.startJulSess = np.zeros(self.nsession, dtype = int)
        self.yearSess = np.zeros(self.nsession, dtype = int)
        for i in range(self.nsession):
            sess = self.uSession[i]
            j1 = self.julianYear[self.session == sess]
            year = self.year[self.session == sess]
            self.startJulSess[i] = np.min(j1)        
            self.yearSess[i] = np.min(year)

    def makeYearRecruitIndx(self):
        """
        make indx to distribute recruits across session in a 365-day period 
        """
        relDay = np.zeros(self.nsession, dtype = int)
        # mask sessions from jan 1 upto end of repro period
        beforeMask = self.params.reproDays[-1] >= self.startJulSess
        # mask sessions from after repro period to end of December
        afterMask = self.params.reproDays[-1] < self.startJulSess
        # relative days up
        relDay[beforeMask] = 365 + self.startJulSess[beforeMask]
        relDay[afterMask] = self.startJulSess[afterMask] - self.params.reproDays[-1] 
        yrRecIndx = np.zeros(self.nsession)
        indx = 0
        for i in range(1, self.nsession):
            if relDay[i] < relDay[i-1]:
                indx += 1
            yrRecIndx[i] = indx
        self.yearRecruitIndx = yrRecIndx.copy()
        self.yearRecruitIndx = self.yearRecruitIndx.astype(int)
#        print('recruitIndx', self.yearRecruitIndx, len(self.yearRecruitIndx))
#        print('self.sessionMonth', self.sessionMonth, len(self.sessionMonth))
#        print('sessions', self.uSession, self.nsession)   

    def makeReproMask(self):
        """
        Make mask for identifying reproductive period
        Use this to get mean pop size to calculate number of recruits
        """
#        mask1 = self.startJulSess >= self.params.reproDays[0]
#        mask2 = self.startJulSess <= self.params.reproDays[1]
#        self.reproMask = mask1 & mask2
        self.reproMask = np.in1d(self.startJulSess, self.params.reproDays)
        # Make indexing array for periods on which to calc reproductive pop
        indx = 1
        self.reproPeriodIndx = np.zeros(self.nsession, dtype = int)
        for i in range(1, self.nsession):
            if self.reproMask[i]:
                self.reproPeriodIndx[i] = indx
            if self.reproMask[i-1] and not self.reproMask[i]:
                indx += 1
        # remove Nov 2014 from repro preiod
        self.reproPeriodIndx[self.reproPeriodIndx == 9] = 0
        self.reproMask[(self.nsession - 2):] = 1 - self.reproMask[(self.nsession - 2):]
#        print('reproIndx', self.reproPeriodIndx, len(self.reproPeriodIndx))


    def getInitialN(self):
        """
        looping to get reasonable inital N values
        """
        self.N = np.zeros(self.nsession, dtype = np.int)
        self.Ns = np.zeros(self.nsession, dtype = np.int)
        self.reproPop = np.zeros(self.nYear)
        self.reproPop[0] = self.initialReproPop
        self.reproPop_s = np.zeros(self.nYear)
        self.reproPop_s[0] = self.initialReproPop + 1
        self.totalRecruits = np.zeros(self.nYear)
        self.totalRecruits_s = np.zeros(self.nYear)
        self.sessionRecruits = np.zeros(self.nsession)
        self.sessionRecruits_s = np.zeros(self.nsession)
        nnow = self.initialReproPop
        self.N[0]= nnow
        self.Ns[0] = nnow - 1

        self.totalRecruits[0] = (self.reproPop[0] * self.rg) + self.ig
        self.totalRecruits_s[0] = (self.reproPop_s[0] * self.rg) + self.ig

        for i in range(1, self.nsession):
            if self.yearRecruitIndx[i] == 0:
                sr = self.totalRecruits[0] * self.relWrpCauchy[i]
                sr_s = self.totalRecruits_s[0] * self.relWrpCauchy[i]
                self.sessionRecruits[i] = sr
                self.sessionRecruits_s[i] = sr_s
                nnew = nnow - self.removeDat[i-1] + sr
                if nnew < 1:
                    nnew = 5
                nnew2 = stats.poisson.rvs(nnew, size=None)
            if self.yearRecruitIndx[i] > 0:
                yrIndx = self.yearRecruitIndx[i]
                nPopi= self.N[self.reproPeriodIndx == yrIndx]
                rPop = np.mean(nPopi)
                nPopi_s= self.Ns[self.reproPeriodIndx == yrIndx]
                rPop_s = np.mean(nPopi_s)
                self.reproPop[self.uYearIndx == yrIndx] = rPop
                self.reproPop_s[self.uYearIndx == yrIndx] = rPop_s
                totrec = (self.rg * rPop) + self.ig
                totrec_s = (self.rg * rPop_s) + self.ig
                self.totalRecruits[self.uYearIndx == yrIndx] = totrec
                self.totalRecruits_s[self.uYearIndx == yrIndx] = totrec
                relwc = self.relWrpCauchy[i]
                sr = totrec * relwc
                sr_s = totrec_s * relwc
                self.sessionRecruits[i] = sr
                self.sessionRecruits_s[i] = sr_s
#                # use this to accomodate 12/2014
#                if i > (self.nsession - 3):
#                    self.sessionRecruits[i] = 0.15
#                    self.sessionRecruits_s[i] = 0.15
                nnew = nnow - self.removeDat[i-1] + sr
                if nnew > self.params.maxN:
                    nnew = self.params.maxN -50    #360
                nnew2 = stats.poisson.rvs(nnew, size=None)
            if nnew2 <= self.removeDat[i]:
                nnew2 = self.removeDat[i] + 5
            if nnew2 < 1:
                nnew2 = 5
            self.N[i] = nnew2
            self.Ns[i] = nnew2 + 1
            nnow = nnew2
#        print('self.N', self.N)
#        print('self.reproPop', self.reproPop)
#        print('self.totRecruits', self.totalRecruits)
#        print('self.sessionRec', self.sessionRecruits)

    def getNSessInReproPeriod(self):
        """
        make array of length uYearIndx (7) that has number of sessions
        in each corresponding repro period
        """
        self.nSessInRepro = np.zeros(len(self.uYearIndx))
        for i in range(1, len(self.uYearIndx)):
            nsess = len(self.reproPeriodIndx[self.reproPeriodIndx == i])
            self.nSessInRepro[i] = nsess

    def NpredAllSessFX(self, nn):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.nsession)
        Npred[0] = self.reproPop[0] + self.sessionRecruits[0]
        for i in self.uSession[:-1]:
            Nday = nn[i] - self.removeDat[i]
            Nday = np.where(Nday < 0, 0, Nday)
            Nday = Nday + self.sessionRecruits[i+1]        # it[i+1]   # imm upto previous
            Npred[i+1] = Nday
        return(Npred)


    def make_g0_Indx(self):
        """
        make array temporally indexing where high, low and standard g0-multipliers applies
        length is number of session (self.nsession)
        """
        # indx is zero where normal g0 applies
        self.g0_Indx_Multiplier = np.zeros(self.nsession, dtype = int)
        mask1 = self.startJulSess >= self.params.g0LowDays[0]      
        mask2 = self.startJulSess <= self.params.g0LowDays[1]      
        lowMask = mask1 & mask2
        # indx is 1 when low g0 applies
        self.g0_Indx_Multiplier[lowMask] = 1                    

        mask1 = self.startJulSess >= self.params.g0HighDays[0]
        mask2 = self.startJulSess <= self.params.g0HighDays[1]
        highMask = mask1 & mask2
        # indx is 2 when high g0 applies
        self.g0_Indx_Multiplier[highMask] = 2
#        print('self.g0_Indx_Multiplier', self.g0_Indx_Multiplier)


    def gettrapSession(self):
        """
        create session ID for Trap data (nsession * nTrap)
        """
        sess = np.arange(self.nsession)
        self.trapSession = np.repeat(sess, self.nTraps)
#        print("self.trapSession.shape", self.trapSession.shape)

    def getTrapNightsFX(self):
        """
        Get number of trap nights * avail
        Get 0 and 1 array of traps that caught
        """
        self.trapNightsAvail = np.zeros(self.nsession * self.nTraps)
        self.trapTrapped = np.zeros(self.nsession * self.nTraps)            # capt captures by trap and session
        for i in range(self.nsession):
            sessmask = self.trapSession == i                                # sess mask for trap data
            captSeqTrapIDSession = self.captTrapID[self.session == i]    # sequence trap ID in capt6 data
            tmpTN = np.zeros(self.nTraps)                                   # empty tn array to populate
            tmptrapcapt = np.zeros(self.nTraps)                             # length trap dat
            trappedSession = self.trapped[self.session == i]                # length capt dat in sess i
            ndaySess = self.nDays[self.session == i]                        # capt session trap night/day 
            # loop through capt data in session i
            cc = 0
            for j in range(self.nTraps):
                if self.trapID[j] in captSeqTrapIDSession:
#                    if not np.isscalar(ndaySess[captSeqTrapIDSession == self.trapID[j]]):
#                        print('ndaySess', ndaySess[captSeqTrapIDSession == self.trapID[j]])
#                        print('i and j', i, j)

                    tmpTN[j] = ndaySess[captSeqTrapIDSession == self.trapID[j]]     # trap nights in trap j sess i
                    tt_j = trappedSession[captSeqTrapIDSession == self.trapID[j]]   # number of predators trapped
#                    if i == 424:
#                        cc += tt_j
#                        print('tid', self.trapID[j], 'tt_j', tt_j, 'cc', cc)
                    # whether trap captured predator  
                    tmptrapcapt[j] = tt_j                                                 
            # trap nights and captures by trap and session (repeating traps for all session)
            self.trapNightsAvail[sessmask] = tmpTN
            self.trapTrapped[sessmask] = tmptrapcapt
        self.trapNightsAvail_2D = np.expand_dims(self.trapNightsAvail, 1)
#        print('n - rm', self.N - self.removeDat)


class PredatorData(object):
    def __init__(self, basicdata, params, debug = False):
        """
        object to keep latent predator loc, pcaptured, whether trapped, which trap
        """
        # a seris of 1-d arrays maxN * nsession
        predatorSession = np.repeat(np.array(range(basicdata.nsession), dtype= int), basicdata.maxN)
        predatorID = np.tile(np.arange(0, basicdata.maxN), basicdata.nsession)
        predatorPres = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorLoc = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorRemove = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorTrapID = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorPCapt = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession))
        predatorTrapPCapt =  np.zeros(np.multiply(basicdata.maxN, basicdata.nsession))
        # loop through sessions
        for i in range(basicdata.nsession):
            tmpPres = predatorPres[predatorSession ==i]
            tmpPres[0:basicdata.N[i]] = 1
            # array of 0 and 1, 1 where present
            predatorPres[predatorSession == i] = tmpPres
            tmpLoc = predatorLoc[predatorSession == i]
            tmpPCapt = predatorPCapt[predatorSession == i]
            tmpRemove = predatorRemove[predatorSession == i]
            # randomly select predators to remove
            selPredatorRem = np.random.permutation(range(basicdata.N[i]))[0:basicdata.removeDat[i]]            
            tmpRemove[selPredatorRem] = 1
            predatorRemove[predatorSession == i] = tmpRemove
            trapSessMask = basicdata.trapSession == i
            # start to id traps that capture each present predator in session i
            # number trapped by trap in session i
            trappedSession = basicdata.trapTrapped[trapSessMask]

            # number of traps that captured predators
            nTraps = len(trappedSession[trappedSession > 0])
            # array number of captures length of number of traps that captured predators
            selTrapSprungStillOpen = trappedSession[trappedSession > 0]   #np.ones(nTraps)
            # number of predators trapped in all traps in sess i that capt or not 
#            tpredatorcaptsess = basicdata.trapTrapped[basicdata.trapSession == i]
            # array of trap ID that captured predators in sess i
            selTrapIDCaptPredator = basicdata.trapID[trappedSession > 0]             
            # arrays of zeros to populate below            
            tmppredatorTrapID = predatorTrapID[predatorSession == i]                    # arrays of zeros to populate
            tmppredatorTrapPCapt = predatorTrapPCapt[predatorSession == i]
            # trap nights for each trap in session i
            tnightsavailSession = basicdata.trapNightsAvail[trapSessMask]
            # g0 for each trap in session i
            g0Session = basicdata.g0All[basicdata.trapSession == i].flatten()
#            g0Session = basicdata.g0All[trapSessMask]
#            print("g0Sess", g0Session.shape)
            leftToRem = basicdata.removeDat[i]

           # loop through predators present
            for j in range(basicdata.N[i]):
                # assign location using multinomial

                tmpLoc[j] = basicdata.cellID[np.random.multinomial(1, 
                                        basicdata.thMultiNom, size = 1).flatten() == 1]

#                print("tnightsavailSession.shp", tnightsavailSession.shape)
#                print("tmpLoc[j]", tmpLoc[j])
#                print("g0Session.shp", g0Session.shape)
                
                tmpPTrapCapt = initialPPredatorTrapCaptFX(basicdata, 
                    tnightsavailSession , tmpLoc[j], g0Session, debug = False)

                # index of traps with top prob of capt
                topPCaptIndx = np.argsort(tmpPTrapCapt)[-params.nTopTraps:]
                # array of pcapt of only top traps
                topPCapt = tmpPTrapCapt[topPCaptIndx]
                # total prob capture of pred j across all top traps
                tmpPCapt[j] = 1. - np.prod(1. - topPCapt)

                
##########                 # total prob capture of pred j across all traps
##########                tmpPCapt[j] = 1. - np.prod(1. - tmpPTrapCapt)
                if tmpPCapt[j] > 0.97:
                    tmpPCapt[j] = 0.97
                # if predator j is removed then follow:
                if tmpRemove[j] == 1:
                    # ID of traps avail to capture
                    sprungTrapsStillOpen = selTrapIDCaptPredator[selTrapSprungStillOpen > 0]

#                    if i == 424:
#                        print('################################      i and j', i , j)
#                        print('left to remove', leftToRem)
#                        print('sum of captures', np.sum(trappedSession))
#                        print('sprungTrapsStillOpen', sprungTrapsStillOpen)
#                        print('selTrapSprungStillOpen', selTrapSprungStillOpen[selTrapSprungStillOpen > 0])

                    leftToRem = leftToRem - 1
                    # pcapt of traps still open
#                    if i == 424:
#                        print('iiiiiiiii ', i, 'j ', j, )
#                        print('tmpPTrapCapt[sprungTrapsStillOpen]', tmpPTrapCapt[sprungTrapsStillOpen])
                    pCaptAvailTrap = tmpPTrapCapt[sprungTrapsStillOpen]
                    diffMask = pCaptAvailTrap == np.max(pCaptAvailTrap)

                    # trap id with max pcapt captures predator j

#                    print("tmpPTrapCapt.shp", tmpPTrapCapt.shape)
#                    print("pCaptAvailTrap.shp", pCaptAvailTrap.shape)
#                    print("pCaptAvailTrap", pCaptAvailTrap )
#                    print("diffMask", diffMask)
#                    print("sprungTrapsStillOpen.shp", sprungTrapsStillOpen.shape)
#                    print("sprungTrapsStillOpen", sprungTrapsStillOpen)
#                    print("diffmask.shp, i, j", diffMask.shape, i, j)

                    captID = sprungTrapsStillOpen[diffMask]
                    
#                    print("captID.shape", captID.shape)
                    

                    if np.shape(captID)[0] == 1:
                        tmppredatorTrapID[j] = captID
                    else:
                        tmppredatorTrapID[j] = captID[0]

#                    print("tmppredatorTrapID[j]", tmppredatorTrapID[j])
                    
                    # take just 1 
#                    tmppredatorTrapID[j] = captID[0]
                    # prob that the given trap captured predator j for predator data
                    # prob that the given trap captured predator j for predator data
                    tmppredatorTrapPCapt[j] = tmpPTrapCapt[tmppredatorTrapID[j]]
                    # make all probabilities non-zero
                    tmppredatorTrapPCapt[j] = np.where(tmppredatorTrapPCapt[j] < 1.00e-20, 1.00e-20, 
                                                tmppredatorTrapPCapt[j])
                    # reduce availability of that trap to capture other predators j + ...
                    reductionResult = selTrapSprungStillOpen[selTrapIDCaptPredator == tmppredatorTrapID[j]] - 1
                    selTrapSprungStillOpen[selTrapIDCaptPredator == tmppredatorTrapID[j]] = reductionResult

            predatorLoc[predatorSession == i] = tmpLoc
            predatorPCapt[predatorSession == i] = tmpPCapt
            predatorTrapID[predatorSession == i] = tmppredatorTrapID
            predatorTrapPCapt[predatorSession == i] = tmppredatorTrapPCapt

        self.predatorSession = predatorSession
        self.predatorID = predatorID
        self.predatorPres = predatorPres
        self.predatorLoc = predatorLoc
        self.predatorRemove = predatorRemove
        self.predatorTrapID = predatorTrapID
        self.predatorPCapt = predatorPCapt
        self.predatorTrapPCapt = predatorTrapPCapt
        self.predatorPCapt_s = predatorPCapt.copy()
        self.predatorTrapPCapt_s = predatorTrapPCapt.copy()


class MCMC(object):
    def __init__(self, params, predatordata, basicdata):

        self.basicdata = basicdata
        self.params = params
        self.predatordata = predatordata
        # storage arrays for parameters
        self.bgibbs = np.zeros([self.params.ngibbs, self.params.nbcov])             # beta parameters
        self.rgibbs = np.zeros(self.params.ngibbs)                                  # pop growth
        self.rparagibbs = np.zeros([self.params.ngibbs, 2])                         # wrapped cauchy para for recruitment 
        self.igibbs = np.zeros(self.params.ngibbs)                                  # immigration
        self.siggibbs = np.zeros(self.params.ngibbs)                              # home range decay
        self.g0gibbs = np.zeros([self.params.ngibbs, self.basicdata.nTrapBait])     # beta parameters
        self.Ngibbs = np.zeros([self.params.ngibbs, self.basicdata.nsession])       # beta parameters
        self.g0Multigibbs = np.zeros([self.params.ngibbs, 3])                       # seasonal g0 multiplier 
        self.initialRepPopgibbs = np.zeros(self.params.ngibbs)

        ### g0 update sampling arrays
        self.g0_sample0 = np.arange(self.basicdata.nTrapBait, step=2)
        self.g0_sample1 = np.arange(1, self.basicdata.nTrapBait, step=2)
        self.sampleIndx = np.zeros(len(self.g0_sample0))

        # storage array for prob of trapping data
        self.pTrappingData = np.empty(self.basicdata.nsession)
        self.pTrappingData_s = np.empty(self.basicdata.nsession)

##################
#################
################

    @staticmethod
    def PPredatorTrapCaptFX(availTrapNights, eTermMat, g0Sess, debug = False):      
        """
        prob that stoat at specifid locs was capt in specified traps
        """
        availTrapNights = availTrapNights.flatten()
#        eTermMat = eTermMat.flatten()
        g0Sess = g0Sess.flatten()
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
        pcapt = 1.0 - pNoCaptNights
        pcapt[pcapt > .97] = 0.97
        if debug == True:
            print('(g0Sess * eTermMat)', (g0Sess * eTermMat).shape)
            print('pNoCapt', pNoCapt)
            print('tn',availTrapNights.shape)
            print('eterm', eTermMat.shape)
            print('g0', g0Sess.shape)
            print('pcapt.shp', pcapt.shape)
        return pcapt        # 1.0 - pNoCaptNights

    def getBetaFX(self):
        a = self.basicdata.g0 * ((self.basicdata.g0 * (1.0 - self.basicdata.g0)) / self.params.g0Sd**2.0 - 1.0)
        b = (1.0 - self.basicdata.g0) * ((self.basicdata.g0 * (1.0 - self.basicdata.g0)) / self.params.g0Sd**2.0 - 1.0)
        return np.random.beta(a, b, size = None)

########
########       Block of functions for updating N
########

    def N_predatordata_updateFX(self):
#        print('totrec', self.basicdata.totalRecruits)
#        print('reproPop', self.basicdata.reproPop)

        for i in range(self.basicdata.nsession):
                                                ####### Update N
                                                 # get proposed Ns and predatordata_s and llik
            (Ns, sessionMask, predatorLoc_s, predatorPres_s, predatorPCapt_s, llik, llik_s,
                availTrapNights, presOnlyMask, presOnlyMask_s, trapSessionMask, 
                g0Session) = self.proposeNFX(i)

#            print('i, n, npred, sessrec', i, self.basicdata.N[i], self.basicdata.Npred[i], self.basicdata.sessionRecruits[i])
#            print('npred', self.basicdata.Npred)
#            print('totRec', self.basicdata.totalRecruits)
#           print('sessrecruits', self.basicdata.sessionRecruits)


                                                # pnow pnew Npred
            presOnlyMask = self.N_PnowPnewFX(Ns, i, llik,
                llik_s, presOnlyMask, presOnlyMask_s, predatorLoc_s, predatorPres_s,
                predatorPCapt_s, sessionMask, debug = False)
                                                # returns predatorloc
                                                # returns predatorPres
                                                # returns predatorPCapt


                                                ########### update predator location
                                                # propose new predatorloc and pcapt
            (predatorLocSession, predatorPCaptSession, predatorRemoveSession, predatorPresSession,
                predatorTrapIDSession, predatorTrapPCaptSession, remMask,
                nPredatorCellSession) = self.updatePredatorLocFX(i,
                sessionMask, availTrapNights, presOnlyMask, g0Session)

                                                ########## update predators that are removed
                                                # return predatorloc and predatorPcapt
            if self.basicdata.removeDat[i] > 0:
                (predatorRemoveSession, predatorTrapIDSession,
                    predatorTrapPCaptSession,remMask) = self.updatePredatorRemoveFX(i, sessionMask,
                    presOnlyMask, predatorLocSession, predatorPCaptSession,
                    predatorRemoveSession, predatorPresSession, predatorTrapIDSession,
                    predatorTrapPCaptSession, remMask, availTrapNights, g0Session)




                                                ######### update trapid of traps that catch predators
                                                #########
            if self.basicdata.removeDat[i] > 1:
                self.updateTrapIDFX(i, sessionMask, predatorLocSession, predatorPresSession,
                    predatorTrapIDSession, predatorTrapPCaptSession, remMask,
                    availTrapNights, g0Session)

                                                 ######## calc th likelihoods by session
            self.thetaLikelihoodFX(i, predatorLocSession, presOnlyMask, sessionMask)


#                                                ######## get llik for g0 and g0_s
#            self.g0SigLLikFX(i, availTrapNights, predatorLocSession, predatorTrapIDSession,
#                predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
#                predatorRemoveSession, remMask, g0Session, g0Session_s)

                


#########
########  End updating N and predatordata
########

    def proposeNFX(self, i):    # get proposed Ns and assoc predator data
                                                                        # use FX in NupdateFX
        rDat = self.basicdata.removeDat[i]
        Ns = self.basicdata.N[i] + np.random.permutation(self.basicdata.nsample)[0]
        Ns = np.where(Ns <= 0, 1, Ns)
        Ns = np.where(Ns <= rDat, rDat + 1, Ns)
        Ns = np.where(Ns == self.basicdata.N[i], self.basicdata.N[i] + 1, Ns)
        Ns = np.where(Ns >  self.params.maxN, self.basicdata.N[i] - 5, Ns)
        Ns = np.where(Ns == self.basicdata.N[i], self.basicdata.N[i] - 1, Ns)
  
        sessionMask = self.predatordata.predatorSession == i
        predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
        predatorPresSession = self.predatordata.predatorPres[sessionMask]
        predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
        presOnlyMask = predatorPresSession == 1                    #mask of pres only
        predatorLoc_s = self.predatordata.predatorLoc[sessionMask]
        predatorPres_s = predatorPresSession.copy()
        predatorPCapt_s = predatorPCaptSession.copy()
        # mask for trap data (nsession * ntrap)
        trapSessionMask = self.basicdata.trapSession == i
        # g0 for session i
        g0Session = self.basicdata.g0All[trapSessionMask]
        availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
        # get proposed predator data for Ns
        (presOnlyMask_s, predatorLoc_s, predatorPres_s, predatorPCapt_s) = self.proposePredatorFX(Ns, i, predatorPres_s, predatorLoc_s,
            predatorPCapt_s, availTrapNights, predatorRemoveSession,
            predatorPresSession, presOnlyMask, g0Session)
        # get binomial log likelihood for N and Ns
        llik = self.NLogLikFX(self.basicdata.N[i], rDat, predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])        #(i > 11)
        llik_s = self.NLogLikFX(Ns, rDat, predatorPCapt_s[presOnlyMask_s],
            predatorRemoveSession[presOnlyMask_s])

        return (Ns, sessionMask, predatorLoc_s, predatorPres_s, predatorPCapt_s, llik, llik_s,
            availTrapNights, presOnlyMask, presOnlyMask_s, trapSessionMask, g0Session)


    def proposePredatorFX(self, Ns, i, predatorPres_s, predatorLoc_s,
        predatorPCapt_s, availTrapNights, predatorRemoveSession,
        predatorPresSession, presOnlyMask, g0Session):
        """
        determine loc, presence and pcapt for new predators
        Or, remove existing predators
        FX nested in proposeNFX
        """
        # in this block need to add individuals to proposed pop
        if Ns > self.basicdata.N[i]:
            potentialAddIndx = self.basicdata.datseq[presOnlyMask == False]
            nadd = Ns - self.basicdata.N[i]            # number predators added
            randindx = np.random.randint(0, self.basicdata.ncell, nadd)  # cell to add individual
            add_id = np.random.choice(potentialAddIndx, nadd, replace = False)  # individual id added to present list
            predatorPres_s[add_id] = 1
            presOnlyMask_s = predatorPres_s == 1
            predatorLoc_s[add_id] = randindx
           # loc_s = predatorLoc_s[presOnlyMask_s]
#            et_1 = self.basicdata.expTermMat[:,randindx]
            if nadd == 1:
                endid = randindx + 1
                et_1 = self.basicdata.expTermMat[:, randindx : endid]
            else:
                et_1 = self.basicdata.expTermMat[:, randindx]
#            et_1 = self.basicdata.expTermMat[:, loc_s]
#            if Ns == 1:
#                et_1 = np.expand_dims(et_1, 1)
            pCaptNew = self.PCaptPredatorFX(availTrapNights, et_1, g0Session)
#            predatorPCapt_s[presOnlyMask_s] = pCaptNew
            predatorPCapt_s[add_id] = pCaptNew

        # in this block need to remove individuals from pop
        if Ns < self.basicdata.N[i]:
            nrem = self.basicdata.N[i] - Ns
            removeSessionMask = predatorRemoveSession == 0
            predatorPresMask = predatorPresSession == 1
            potentialRemoveMask = predatorPresMask & removeSessionMask
            potentialRemove = self.basicdata.datseq[potentialRemoveMask]        # select from 0:maxN
            remIndx = np.random.choice(potentialRemove, nrem, replace = False)
            predatorLoc_s[remIndx] = 0
            predatorPres_s[remIndx] = 0
            predatorPCapt_s[remIndx] = 0
            presOnlyMask_s = predatorPres_s == 1
            randindx = 0
        return (presOnlyMask_s, predatorLoc_s, predatorPres_s, predatorPCapt_s)



    def N_PnowPnewFX(self, Ns, i, llik, llik_s, presOnlyMask, presOnlyMask_s,
        predatorLoc_s, predatorPres_s, predatorPCapt_s, sessionMask, debug = False):
        """
        calc Importance ratio for proposed N in session i
        use binomial for capture probabilities
        use poisson for predicted N against latent real N
        Have to calc Npred_s values for poisson probabilities
        """
        # not last session  #########################################
        if (i < self.basicdata.uSession[-1]):
            recruit = self.basicdata.sessionRecruits[i+1]
            Npred2_s = self.nPredFX(i, Ns, recruit)
            self.basicdata.Npred_s[i+1] = Npred2_s

            # get repro pop if Ns is in repro period (Nov) for the given year
            if self.basicdata.reproMask[i]:
                # get proposed repro pop by integrating the Ns value
                # recruitYearMask  is mask of repro period (Nov) of given year
                # yrIndx is index of repro period (Nov) for given year
                # reproPop_s is tot pop in repro period (Nov) in given year
                # totRecruit and yearRecruits are total recruits and number distributed to each session
                # get recruit info 
                (recruitYearMask, yrIndx, reproPop_s, totRecruits_s, 
                    yearRecruits_s) = self.recruit_NsInRepro(i, Ns)
                # mask of sessions where recruits will fall
#                nTestMask = self.basicdata.recruitWindowMask & recruitYearMask 
                # N data where recruits fall
                nInYear = self.basicdata.N[recruitYearMask]
                dpoisRec = np.sum(stats.poisson.logpmf(nInYear, self.basicdata.Npred[recruitYearMask]))
                dpoisN1 =  stats.poisson.logpmf(self.basicdata.N[i], self.basicdata.Npred[i])
                dpoisN2 =  stats.poisson.logpmf(self.basicdata.N[i + 1], self.basicdata.Npred[i + 1])
                # llik for current N
                pnow = llik + dpoisRec + dpoisN1 + dpoisN2

                # get llik for proposed N
                dpoisRec_s = np.sum(stats.poisson.logpmf(nInYear, self.basicdata.Npred_s[recruitYearMask]))
                dpoisN1_s = stats.poisson.logpmf(Ns, self.basicdata.Npred[i])
                dpoisN2_s = stats.poisson.logpmf(self.basicdata.N[i + 1], Npred2_s)
                pnew = llik_s + dpoisRec_s  + dpoisN1_s + dpoisN2_s

#                if i == 153:
#                    print('repromask i', self.basicdata.reproMask[i])
#                    print('recruitYearMask', recruitYearMask)
#                    print('self.basicdata.recruitWindowMask', self.basicdata.recruitWindowMask)
#                    print('recruitYearMask', nTestMask)

#                if (i>152) & (i<155):
#                    print('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii', i)
#                    yearIndx = self.basicdata.reproPeriodIndx[i]
#                    print('N and Ns', self.basicdata.N[i], Ns)
#                    print('nInYear', nInYear)
#                    print('reproPop and reproPop_s', self.basicdata.reproPop[yearIndx], reproPop_s)
#                    print('llik + dpoisRec + dpoisN1 + dpoisN2', llik, dpoisRec, dpoisN1, dpoisN2)
#                    print('llik_s + dpoisRec_s + dpoisN1_s + dpoisN2_s', llik_s, dpoisRec_s, dpoisN1_s, dpoisN2_s)
#                    print('pnow and pnew', pnow, pnew)


            # if Ns not in repro period and not last session
            if (self.basicdata.reproMask[i] == 0):
                # poisson for current N and then N[i+1]
                dpoisN1 =  stats.poisson.logpmf(self.basicdata.N[i], self.basicdata.Npred[i])
                dpoisN2 =  stats.poisson.logpmf(self.basicdata.N[i + 1], self.basicdata.Npred[i + 1])
                pnow = llik + dpoisN1 + dpoisN2
                dpoisN1_s = stats.poisson.logpmf(Ns, self.basicdata.Npred[i])
                dpoisN2_s = stats.poisson.logpmf(self.basicdata.N[i + 1], Npred2_s)
                pnew = llik_s + dpoisN1_s + dpoisN2_s
            #################################################
            ####################################################
        # Last session
        if i == self.basicdata.uSession[-1]:
            pnow = llik + stats.poisson.logpmf(self.basicdata.N[i], self.basicdata.Npred[i])
            pnew = llik_s + stats.poisson.logpmf(Ns, self.basicdata.Npred[i])
        # Calc IR
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0,1, size = None)
        if rValue > zValue:
            # if before last session, update next npred
            if i < self.basicdata.uSession[-1]:
                self.basicdata.Npred[i + 1] = Npred2_s
            # if i is in repro period, update recruitment details:
            if self.basicdata.reproMask[i]:
                self.basicdata.reproPop[yrIndx] = reproPop_s    # mean repro pop
                self.basicdata.totalRecruits[yrIndx] = totRecruits_s    # total recruits
                self.basicdata.sessionRecruits[recruitYearMask] = yearRecruits_s    # recruits in sess
                self.basicdata.Npred[recruitYearMask] = self.basicdata.Npred_s[recruitYearMask]
            # update if r > z
            self.basicdata.N[i] = Ns
            self.predatordata.predatorLoc[sessionMask] = predatorLoc_s
            self.predatordata.predatorPres[sessionMask] = predatorPres_s
            self.predatordata.predatorPCapt[sessionMask] = predatorPCapt_s
            presOnlyMask = presOnlyMask_s.copy()
#        if i == 0:
#            print('r -z', rValue - zValue)
#            pcaptSess = self.predatordata.predatorPCapt[sessionMask]
#            print('Nupdate predatorPCapt[presOnlyMask]', self.predatordata.predatorPCapt[presOnlyMask])
#            print('Nupdate loc', self.predatordata.predatorLoc[presOnlyMask]) 
        return presOnlyMask


    def recruit_NsInRepro(self, i, Ns):
        """
        Get Npred across sessions in a year when update Ns session is in repro period
        """
        yrIndx = self.basicdata.reproPeriodIndx[i]                                  # reproductive year
        recruitYearMask = self.basicdata.yearRecruitIndx == yrIndx                  # mask of sessions for year
        reproPop_s = self.getProposedReproPop(i, Ns, yrIndx)                        # repro pop in given year
        totRecruits_s = (reproPop_s * self.basicdata.rg) + self.basicdata.ig        # total recruits in year
        yearRecruits_s = self.proposedRecruitYear(totRecruits_s, recruitYearMask)   # recruits across sessions
        self.basicdata.reproPop_s[yrIndx] = reproPop_s
        self.basicdata.totalRecruits_s[yrIndx] = totRecruits_s
        self.basicdata.sessionRecruits_s[recruitYearMask] = yearRecruits_s
        # get nPred_s for year with updated number of recruits
        self.basicdata.Npred_s[recruitYearMask] = self.NpredYear_NsInRepro(i, Ns, recruitYearMask)
        return (recruitYearMask, yrIndx, reproPop_s, totRecruits_s, yearRecruits_s)

    def getProposedReproPop(self, i, Ns, yrIndx):
        """
        Get reproductive population for a single year
        """
        ntmp_s1 = np.sum(self.basicdata.N[self.basicdata.reproPeriodIndx == yrIndx])
        nSess = self.basicdata.nSessInRepro[yrIndx]
        rPop = (ntmp_s1 - self.basicdata.N[i] + Ns)/nSess
        return rPop

    def proposedRecruitYear(self, totRecruits, recruitYearMask):
        """
        Calculate predicted number of recruits in each week of a given year
        """
        relWrpCauchy = self.basicdata.relWrpCauchy[recruitYearMask]
        recruitYear = totRecruits * relWrpCauchy
        return recruitYear


    def NpredYear_NsInRepro(self, i, Ns, recruitYearMask):
        """
        get Npred or Npred.s for new N for a given year when change N in repro period
        """
        datseq = self.basicdata.uSession[recruitYearMask]
        Ntmp = self.basicdata.N.copy()
        Ntmp[i] = Ns
        Nstart = Ntmp[datseq - 1]
        remTmp = self.basicdata.removeDat[datseq-1]
        Ntmp2 = Nstart - remTmp
        npredout = Ntmp2 + self.basicdata.sessionRecruits_s[recruitYearMask]
        return npredout

    def nPredFX(self, i, nn, recruit):
        """
        Get npred for up one session
        """
        nStart = nn - self.basicdata.removeDat[i]
        npred1 = nStart + recruit
        return npred1

    def PCaptPredatorFX(self, availTrapNights,  eTermMat, g0Sess):  
        """
         # prob stoat capt in 1 of many traps in a given session
        """
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
        sortPNoCapt = np.sort(pNoCaptNights, axis=0)
        pNoCaptNightsTraps = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
        pcapt = 1.0 - pNoCaptNightsTraps
        pcapt[pcapt > 0.97] = .97
        return pcapt


    @staticmethod
    def NLogLikFX(n, remd, captp, sessrem, debug=False):    
        """
         # FX get binomial log lik
         # FX nested in ProposeNFX
        """
        comboTerm = gammaln(n + 1) - gammaln(n - remd + 1)
        first = captp * sessrem
        second = (1.0 - captp) * (1.0 - sessrem)
        binProball = first + second
        lProb = np.sum(np.log(binProball))
        llik_term = comboTerm + lProb
        return llik_term

#########
#########           End block of functions for updating N


#######
#######            Begin functions for updating predatorLoc
#######
    def updatePredatorLocFX(self, i, sessionMask, availTrapNights,
        presOnlyMask, g0Session):
        
#        print('iiii        ', i, self.basicdata.N[i])
        # predator loc, pres, capt and pcapt latent data
        predatorPresSession = self.predatordata.predatorPres[sessionMask]
        predatorLocSession = self.predatordata.predatorLoc[sessionMask]

#        ################### Tmp
#        self.basicdata.g0All = g0AllFX(self.basicdata.g0, self.basicdata.g0All, self.basicdata.trapBaitIndx,
#                self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll)
#        ################### End Tmp
#        if i == 0:
#            self.g0Sess_Loc = g0Session.copy()

        predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
        predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
        remMask = predatorRemoveSession == 1               # mask only predators removed
        predatorPCapt_s = predatorPCaptSession.copy()
        predatorLoc_s = predatorLocSession.copy()
        predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
        predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
        # propose to change 20% of predator locations - not all
        nChangeLoc = int(np.ceil(self.basicdata.N[i] * .20))
        potentialChangeID = self.basicdata.datseq[presOnlyMask]
        changeID =  np.random.choice(potentialChangeID, nChangeLoc, replace = False)

        changeMask = np.in1d(self.basicdata.datseq, changeID)
        Loc_s = np.random.randint(self.basicdata.ncell,size = nChangeLoc)
        predatorLoc_s[changeMask] = Loc_s
        # Prob capt of new loc by all traps     #### corrected for limited number of traps
        if nChangeLoc == 1:
            endID = Loc_s + 1
            et_2 = self.basicdata.expTermMat[:, Loc_s : endID]
        else:
            et_2 = self.basicdata.expTermMat[:, Loc_s]
#        et_2 = self.basicdata.expTermMat[:, predatorLoc_s]
#        if self.basicdata.N[i] == 1:
#            et_2 = np.expand_dims(et_2, 1)
        pCaptNewLoc = self.PCaptPredatorFX(availTrapNights, et_2, g0Session)
        predatorPCapt_s[changeMask] = pCaptNewLoc

#        pCaptNew = self.PCaptPredatorFX(availTrapNights, et_2, g0Session)

        # index of traps with top prob of capt
#        topPCaptIndx = np.argsort(tmpPTrapCapt)[-params.nTopTraps:]
        # array of pcapt of only top traps
#        topPCapt = tmpPTrapCapt[topPCaptIndx]
        # total prob capture of pred j across all top traps
#        tmpPCapt[j] = 1. - np.prod(1. - topPCapt)

#        print(changeMask.shape)
#        print(Loc_s.shape)
#        print(pCaptNew.shape)
#        print(predatorPCapt_s.shape)

#        predatorPCapt_s[changeMask] = pCaptNew
        # Likelihood of capture data given locations of predator 
        Z_llik = self.NLogLikFX(1, 1, predatorPCaptSession[changeMask],
            predatorRemoveSession[changeMask], debug = False)             # llik binom capt data (z)
        Z_llik_s = self.NLogLikFX(1, 1, predatorPCapt_s[changeMask],
            predatorRemoveSession[changeMask])
        # Multinomial likelihood of location of predators
        (Loc_llik, nPredatorCellSession) = self.locMultiNom(predatorLocSession, presOnlyMask,
            self.basicdata.thMultiNom, debug = False)
        (Loc_llik_s, nPredatorCellSession_s) = self.locMultiNom(predatorLoc_s, presOnlyMask,
            self.basicdata.thMultiNom, debug = False)
        # mask of predator with changed locations that were captured
        changeRemoveMask = remMask & changeMask
        # prob that the trap capt predator in its location
        predatorTrapPCaptChangeRemove = predatorTrapPCaptSession[changeRemoveMask]
        # New proposed location of trapped pred that changed locations 
        Loc_sChangeRemove = predatorLoc_s[changeRemoveMask]
        tid = predatorTrapIDSession[changeRemoveMask]
        #proposed exp term for prob capture for proposed location
        etermsess = self.basicdata.expTermMat[tid, Loc_sChangeRemove]
        # trap nights
        tnightsavail = availTrapNights[tid] 
        g0tid = g0Session[tid]
        # prob capt in the given trap for captured predators in proposed location
        predatorTrapPCaptChangeRemove_s = self.PPredatorTrapCaptFX(tnightsavail, etermsess, g0tid)

#            print('g0 location', self.basicdata.g0)
#            print('sum availtrap locatation update', np.sum(availTrapNights))
#            print('availtrapnights', availTrapNights[presOnlyMask])
#            print('Location update ###################')
#            print('pCapt_s', predatorPCapt_s[presOnlyMask])
#            print('pcaptold', predatorPCaptSession[presOnlyMask])

#        if i == 0:
#            test = self.PPredatorTrapCaptFX(tnightsavail, etermsess, g0tid, debug = True)
#            print('iiiiiiiiiiiiiiii = 174')
#            print('predatorTrapPCaptChangeRemove_s', predatorTrapPCaptChangeRemove_s)
#            print('tn shp, Location', tnightsavail.shape)
#            print('etermsess', etermsess, etermsess.shape)
#            print('g0tid', g0tid, g0tid.shape)
#            pc = 1 - (g0tid * etermsess)
#            pcn = 1- (pc**tnightsavail)
#            print('pc pcn and predtrapcapt', pc, pcn, predatorTrapPCaptChangeRemove_s)
#            print('tid', tid, tid.shape)
#            print('Loc_sChangeRemove', Loc_sChangeRemove, Loc_sChangeRemove.shape)
              
#        predatorTrapPCaptChangeRemove_s = pCaptTrapNew
        # likelihood of trap that captured predators captured them in present and proposed locations
        Trap_llik = np.sum(np.log(predatorTrapPCaptChangeRemove))
        Trap_llik_s = np.sum(np.log(predatorTrapPCaptChangeRemove_s))
        # total probability of data given location and proposed location latent variables 
        pnow = Z_llik + Loc_llik + Trap_llik
        pnew = Z_llik_s + Loc_llik_s + Trap_llik_s
        # importance ratio
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0,1, size = None)
        # update latent variables if accept proposed locations
 
#        if i == 0:
#            print('Loc start pres id', self.basicdata.datseq[presOnlyMask])
#            print('LOC pred Loc_s', predatorLoc_s[presOnlyMask])
#            print('LOC pred Loc', predatorLocSession[presOnlyMask])
#            print('LOC r - v', rValue - zValue)


        if rValue > zValue:
            self.predatordata.predatorLoc[sessionMask] = predatorLoc_s          # don't need to return ???
            self.predatordata.predatorPCapt[sessionMask] = predatorPCapt_s
            predatorPCaptSession = predatorPCapt_s.copy()
            predatorLocSession = predatorLoc_s.copy()
            predatorTrapPCaptSession[changeRemoveMask] = predatorTrapPCaptChangeRemove_s
            self.predatordata.predatorTrapPCapt[sessionMask] = predatorTrapPCaptSession
            nPredatorCellSession = nPredatorCellSession_s
        # return updated latent variables
#####!                      
#        if i == 0:
#            print('loc proposed pcapt_s', predatorPCapt_s[presOnlyMask])
#            pc = self.predatordata.predatorPCapt[sessionMask]
#            print('Loc pcaptold', predatorPCaptSession[presOnlyMask])
#            print('loc change pcapt', pc[presOnlyMask])
#            locTmp = predatorLocSession[presOnlyMask]
#            et_s = self.basicdata.expTermMat[:, locTmp]
#            sumeterm = np.sum(et_s, axis = 0)
#            print('eterm Loc', sumeterm)

        return (predatorLocSession, predatorPCaptSession, predatorRemoveSession, predatorPresSession,
            predatorTrapIDSession, predatorTrapPCaptSession,remMask, nPredatorCellSession)

    def locMultiNom(self, predatorLocTest, presOnlyMask, thmn, debug = False):
        """
        Calc multinom density function for habitat
        """
        nPredatorCell =  np.bincount(predatorLocTest[presOnlyMask], minlength = self.basicdata.ncell)
        log_mn = multinomial_pmf(thmn, nPredatorCell)
        return (log_mn, nPredatorCell)

#######
######      End function for updating predator location

#####
#######    Begin functions for updating predator remove
#####
    def updatePredatorRemoveFX(self, i, sessionMask,
        presOnlyMask, predatorLocSession, predatorPCaptSession, predatorRemoveSession,
        predatorPresSession, predatorTrapIDSession,
        predatorTrapPCaptSession, remMask, availTrapNights, g0Session):
        """
        update predators that are captured one by one.
        """
        tmpNotRemMask = predatorRemoveSession == 0                  # mask of predators not removed
        presNotRemMask = presOnlyMask & tmpNotRemMask               # mask of removed predators
        nrem = self.basicdata.removeDat[i]                          # number predators removed
        remID = self.basicdata.datseq[remMask]                         # id indx of removed predators
        notRemID = self.basicdata.datseq[presNotRemMask]               # present individ not removed

        # cycle through all removed predators, propose alternative
        for j in range(nrem):                                       
            currentID = remID[j]
            currentTrapID = predatorTrapIDSession[currentID]
            predatorID_s = np.random.choice(notRemID, size = None, replace = True)
            predatorLoc_s = predatorLocSession[predatorID_s]

            zLLik = np.log(predatorPCaptSession[currentID])             # prob current predator capt
            zLLik_s = np.log(predatorPCaptSession[predatorID_s])        # prob proposed is captured

            eterm2 = self.basicdata.expTermMat[currentTrapID, predatorLoc_s]
            tnightsavail = availTrapNights[currentTrapID]
            g0Trap = g0Session[currentTrapID]
            predatorTrapPCapt_s = self.PPredatorTrapCaptFX(tnightsavail, eterm2, g0Trap, debug = False)

            TrapLLik = np.log(predatorTrapPCaptSession[currentID])
            TrapLLik_s = np.log(predatorTrapPCapt_s)
            
            pnow = zLLik + TrapLLik
            pnew = zLLik_s + TrapLLik_s

            rValue = np.exp(pnew - pnow)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)


#                print('r and z #################', rValue, zValue)
#                print('zLLik and TrapLLik', zLLik, TrapLLik)
#                print('zLLik_s and TrapLLik_s', zLLik_s, TrapLLik_s)

            if rValue > zValue:
                predatorRemoveSession[currentID] = 0
                predatorRemoveSession[predatorID_s] = 1
                predatorTrapIDSession[predatorID_s] = predatorTrapIDSession[currentID]
                predatorTrapIDSession[currentID] = 0

                predatorTrapPCaptSession[predatorID_s] = predatorTrapPCapt_s
                predatorTrapPCaptSession[currentID] = 0
                remID[j] = predatorID_s
                notRemID[notRemID == predatorID_s] = currentID

            currentID = remID[j]                                      # id of current removed predator [j]
            predatorID_s = np.random.choice(notRemID, size = None, replace = True)

        remMask = predatorRemoveSession == 1
        self.predatordata.predatorRemove[sessionMask] = predatorRemoveSession
        self.predatordata.predatorTrapID[sessionMask] = predatorTrapIDSession
        self.predatordata.predatorTrapPCapt[sessionMask] = predatorTrapPCaptSession

#        if i == 0:
#            self.g0Sess_Remove = g0Session.copy()

###        if i == 0:
###            print("pcapt remove update", predatorPCaptSession[presOnlyMask])

        return (predatorRemoveSession, predatorTrapIDSession, predatorTrapPCaptSession,
            remMask)

#######
######      End function for updating predator removal

#####
#######    Begin functions for updating trap id:
#####
    def updateTrapIDFX(self, i, sessionMask, predatorLocSession, predatorPresSession,
        predatorTrapIDSession, predatorTrapPCaptSession, remMask, availTrapNights,
        g0Session):
        """
        update the trap that captures an individual predator.
        """
        nrem = self.basicdata.removeDat[i]                          # number predators removed
        remIndx = self.basicdata.datseq[remMask]                    # id indx of removed predators
        # loop through predator-traps that had captures
        for j in range(nrem):
            predatorIndx = remIndx[j]
            indxPool = remIndx[remIndx != predatorIndx]
            # cycle through all removed predators, propose alterna
            predatorIndx_s = np.random.choice(indxPool, size = None, replace = True) 
            # this block propose new trap for current predator
            # loc of current predator
            predatorLoc = predatorLocSession[predatorIndx]   
            # location of second predator 
            predatorLoc_s = predatorLocSession[predatorIndx_s]
            # trap of first predator
            trapID_now = predatorTrapIDSession[predatorIndx]    # trapID[j]
            # trap of second predator
            trapID_s = predatorTrapIDSession[predatorIndx_s]
            # pCapt first predator with its original
            pCapt = predatorTrapPCaptSession[predatorIndx]
            # pCapt of second predator with its original trap
            pCapt_s = predatorTrapPCaptSession[predatorIndx_s]
            # prob of capture of first predator with second trap - switched
            pCaptSwitch = self.PPredatorTrapCaptFX(availTrapNights[trapID_s],
                self.basicdata.expTermMat[trapID_s, predatorLoc], g0Session[trapID_s])
            # pCapt of second predator with first trap - switched
            pCaptSwitch_s = self.PPredatorTrapCaptFX(availTrapNights[trapID_now],
                self.basicdata.expTermMat[trapID_now, predatorLoc_s], g0Session[trapID_now])

#            if (i ==174) & (j<5):
#                print('iiiiiiiii', i, j)
#                print('pCaptSwitch', pCaptSwitch)
#                print('pCaptSwitch_s', pCaptSwitch_s)

            # prob of original trap/pred and second trap/pred
            pnow = np.log(pCapt) + np.log(pCapt_s)
            # prob of switched preds and traps
            pnew = np.log(pCaptSwitch) + np.log(pCaptSwitch_s)
            # calc importance ratio
            rValue = np.exp(pnew - pnow)      
            zValue = np.random.uniform(0, 1.0, size = None)
            if rValue > zValue:
                # update if switched trap/predator is better
                predatorTrapIDSession[predatorIndx] = trapID_s
                predatorTrapIDSession[predatorIndx_s] = trapID_now
                predatorTrapPCaptSession[predatorIndx] = pCaptSwitch
                predatorTrapPCaptSession[predatorIndx_s] = pCaptSwitch_s
        # update latent trapid and and pcapt
        self.predatordata.predatorTrapID[sessionMask] = predatorTrapIDSession
        self.predatordata.predatorTrapPCapt[sessionMask] = predatorTrapPCaptSession

#        if i == 0:
#            self.g0Sess_trapid = g0Session.copy()

#######
###########         begin Beta - theta update ##########
#####
    def thetaLikelihoodFX(self, i, predatorLocSession, presOnlyMask, sessionMask):
        """
        get multinomial llik for each session
        """
        # number of predators in each cell
        nPredatorCell = np.bincount(predatorLocSession[presOnlyMask], minlength = self.basicdata.ncell)
        # multinomial LL for distribution of predators in cells with varying habitat quality
        self.basicdata.llikTh[i] = multinomial_pmf(self.basicdata.thMultiNom, nPredatorCell)
        self.basicdata.llikTh_s[i] = multinomial_pmf(self.basicdata.thMultiNom_s, nPredatorCell)

    def betaUpdateFX(self):
        """
        updata betas all at same time
        """
        # likelihood of parameters given priors
        prior_pdf = stats.norm.logpdf(self.basicdata.b, self.params.bPrior, self.params.bPriorSD)
        prior_pdf_s = stats.norm.logpdf(self.basicdata.bs, self.params.bPrior, self.params.bPriorSD)
        # probability of data given current and proposed betas
        pnow = np.sum(self.basicdata.llikTh) + np.sum(prior_pdf)
        pnew = np.sum(self.basicdata.llikTh_s) + np.sum(prior_pdf_s)
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.b = self.basicdata.bs.copy()
            self.basicdata.lth = self.basicdata.lth_s.copy()
            self.basicdata.llikTh = self.basicdata.llikTh_s.copy()
            self.basicdata.thMultiNom = self.basicdata.thMultiNom_s.copy()
        self.basicdata.bs = np.random.normal(self.basicdata.b, .01)
        self.basicdata.lth_s = np.dot(self.basicdata.xdat, self.basicdata.bs)
        self.basicdata.thMultiNom_s = thProbFX(self.basicdata.lth_s)

#######                 NEW 
###########         begin g0 update ##########
###########
    def propose_g0(self, i):
        """
        propose new g0 and sigma values
        do this here, at end of multiplier update because the new multiplier will affect g0All_s
        """
        # propose 4 new values 
        self.basicdata.g0_s = self.basicdata.g0.copy()
        if i == 0:
            self.sampleIndx = np.random.choice(self.g0_sample0, 4, replace = False)
        else:
            self.sampleIndx = np.random.choice(self.g0_sample1, 4, replace = False)
        logit_g0 = logit(self.basicdata.g0[self.sampleIndx])
        self.basicdata.g0_s[self.sampleIndx] =  inv_logit(np.random.normal(logit_g0, 0.2))
        self.basicdata.g0All_s = g0AllFX(self.basicdata.g0_s, self.basicdata.g0All_s, self.basicdata.trapBaitIndx,
                self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll)

#        if (i == 0):
#            print('g0 g0update', self.basicdata.g0)
#            print('g0 - g0_s', np.sum(self.basicdata.g0 - self.basicdata.g0_s))
#            print('g0All diff', np.sum(self.basicdata.g0All - self.basicdata.g0All_s))

#    def g0Sig_PPredatorCaptFX(self, availTrapNights, eterm, g0Param, debug = False):     # prob that predator was capt in trap
#        """
#        # prob that predator was not captured at trap levle
#        # returns a matrix
#        """
#        pNoCapt = 1.0 - (g0Param * eterm)
#        pNoCaptNights = (pNoCapt**availTrapNights)
#        pNoCaptNights[pNoCaptNights >= .9999] = .9999
#        return pNoCaptNights

    @staticmethod
    def g0Sig_ProbsFX(captp, sessrem, debug=False):     
        """
        # FX get binomial log lik
        """
        first = captp * sessrem
        second = (1.0 - captp) * (1.0 - sessrem)
        binProball = first + second
        llik_g0Sig = np.sum(np.log(binProball))
        return llik_g0Sig


    def getPTrappingData(self, k, availTrapNights, eterm_s, g0Session, pNoCaptAll_s):
        """
        Get probabilities of trapping data given parameters and location
        The prob that traps catch the predators that they did - without predator id
        """
        ######  use proposed parameters
        pCaptAll_s = 1 - np.prod(pNoCaptAll_s, axis = 1)      # p Capt at unique locs in all traps
        pCaptAllSumOne_s = pCaptAll_s / np.sum(pCaptAll_s)
        trapsessmask = self.basicdata.trapSession == k
        trapTrappedSession = self.basicdata.trapTrapped[trapsessmask]
        self.pTrappingData_s[k] = multinomial_pmf(pCaptAllSumOne_s, trapTrappedSession)
        ######  use current parameters        
        pCaptAllPredators = self.PCaptAllPreds_FX(availTrapNights,  eterm_s, g0Session)
        pCaptAllSumOne = pCaptAllPredators / np.sum(pCaptAllPredators)
        self.pTrappingData[k] = multinomial_pmf(pCaptAllSumOne, trapTrappedSession)


    def g0SigLLikFX(self, i, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
        predatorRemoveSession, remMask, g0Session, g0Session_s, eTermMat):
        """
        get llik for g0 and g0_s
        """
        presLoc = predatorLocSession[presOnlyMask]
#        (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#        eterm_s = eTermMat[:, uLoc]                 # exp term of unique locs
        l_presloc = len(presLoc)
        if l_presloc == 1:
            endPresID = presLoc + 1
            eterm_s = eTermMat[:, presLoc : endPresID]
        else:
            eterm_s = eTermMat[:, presLoc]

#!        pNoCaptAll_s = self.g0Sig_PPredatorCaptFX(availTrapNights, eterm_s, g0Session_s)
#!        sortPNoCapt = np.sort(pNoCaptAll_s, axis=0)
#!        pNoCapt_s = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
#        pCaptNew = self.PCaptPredatorFX(availTrapNights, self.basicdata.expTermMat, g0Session_s)

        (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session_s)

        predatorPCaptLoc_sSession = predatorPCaptSession.copy()
###        predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
        predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
        self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
        # the following gets info only for capt predators
        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
#        remIndx = indxLoc[remPresMask]                              # locations only of captured predators
#        remIndx = indxLoc[remPresMask]                              # locations only of captured predator

        matIndx = np.arange(self.basicdata.N[j])
        remIndx = matIndx[remPresMask]

#        if (i==0) & (j ==0):
#            print('remIndx', remIndx)
#            print('pCapt_s', pCapt_s)
#            print('pNoCaptAll_s', np.shape(pNoCaptAll_s))
#            print('remPresMask', remPresMask)
#            print('matIndx', matIndx)

        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
        predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
        predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
        self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
        # binomial LL for current g0 and sigma - prob of preds being captured
        self.basicdata.llikg0Sig[j] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])
        # binomial LL for proposed g0_s and sigma_s - prob of preds being captured
        self.basicdata.llikg0Sig_s[j] = self.g0Sig_ProbsFX(pCapt_s,
            predatorRemoveSession[presOnlyMask])
        #############
        # multinomial prob of traps catching preds that they caught or did not catch.
        self.getPTrappingData(j, availTrapNights, eterm_s, g0Session, pNoCaptAll_s)
        #############

#        if (i == 0) & (j == 0):
##            print('iiiiiiiiiiiiiiiiiijjjjjjjjjjjj')
#            print('g0 pcapt', predatorPCaptSession[presOnlyMask])
#            print('g0 pcapt_s', pCapt_s)
#            sumeterm = np.sum(eterm_s, axis = 0)
#            print('eterm g0', sumeterm)
#            print('g0Sess Loc Diff', np.sum(self.g0Sess_Loc - g0Session_s))
#            print('g0Sess N Diff', np.sum(self.g0Sess_N - g0Session_s))            
#            self.g0Sess_g0 = g0Session_s.copy()
#            print('g0Sess Remove Diff', np.sum(self.g0Sess_Remove - g0Session_s))
#            print('g0Sess trapid Diff', np.sum(self.g0Sess_trapid - g0Session_s))
#            print('within g0 diff', np.sum(g0Session - g0Session_s))

    def PCapt_g0SigFX(self, availTrapNights,  eTermMat, g0Sess):
        """
         # prob stoat capt in 1 of many traps in a given session
        """
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
        sortPNoCapt = np.sort(pNoCaptNights, axis=0)
        pNoCaptNightsTraps = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
        pcapt = 1.0 - pNoCaptNightsTraps
        pcapt[pcapt > 0.97] = .97
        return (pNoCaptNights, pcapt)


    def PCaptAllPreds_FX(self, availTrapNights,  eTermMat, g0Sess):
        """
         # prob stoat capt in 1 of many traps in a given session
        """
        pNoCapt = 1.0 - (g0Sess * eTermMat)
        pNoCaptNights = pNoCapt**(availTrapNights)
        pNoCaptNights[pNoCaptNights >= .9999] = .9999
        pNoCaptNightsAll = np.prod(pNoCaptNights, axis = 1)
        pCaptAllPredators = 1.0 - pNoCaptNightsAll
        return (pCaptAllPredators)


    def g0UpdateFX(self):
        """
        update g0 in mcmc function
        """
        # loop through 2 random selection of 4 trap types = 8 types for each mcmc iteration
        for i in range(2):
            self.propose_g0(i)
            # loop through sessions
            for j in range(self.basicdata.nsession):
                sessionMask = self.predatordata.predatorSession == j
                predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
                predatorPresSession = self.predatordata.predatorPres[sessionMask]
                predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
                presOnlyMask = predatorPresSession == 1                    #mask of pres only
                predatorLocSession = self.predatordata.predatorLoc[sessionMask]
                remMask = predatorRemoveSession == 1               # mask only predators removed
                predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
                predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
                # mask for trap data (nsession * ntrap)
                trapSessionMask = self.basicdata.trapSession == j
                # g0 for session j
                g0Session = self.basicdata.g0All[trapSessionMask]
                g0Session_s = self.basicdata.g0All_s[trapSessionMask]

#                if (i == 0) & (j == 0):
#                    print('g0 diff g0 update', np.sum(g0Session - g0Session_s))
#                    ldat = len(g0Session)
#                    mat = np.zeros([ldat, 2])
#                    mat[:, 0] = g0Session.flatten()
#                    mat[:, 1] = g0Session_s.flatten()
#                    print('g0 mat', mat[0:200])

                # avail trap nights for session i
                availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
                 ######## get llik for g0 and g0_s
                self.g0SigLLikFX(i, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
                            predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
                            predatorRemoveSession, remMask, g0Session, g0Session_s, self.basicdata.expTermMat)
            self.g0_PnowPnew()

    def g0_PnowPnew(self):
        """
        calc pnow and pnew for g0 and update
        """        
        sumPTrapData = np.sum(self.pTrappingData)
        sumPTrapData_s = np.sum(self.pTrappingData_s)
#        sigmaPriorNow = stats.norm.logpdf(self.basicdata.sigma, self.params.sigma_mean, self.params.sigma_sd)
#        sigmaPriorNew = stats.norm.logpdf(self.basicdata.sigma_s, self.params.sigma_mean, self.params.sigma_sd)       
        pnow = np.sum(self.basicdata.llikg0Sig) + np.sum(np.log(stats.beta.pdf(self.basicdata.g0[self.sampleIndx],
            self.params.g0_alpha, self.params.g0_beta))) + sumPTrapData
        pnew = np.sum(self.basicdata.llikg0Sig_s) + np.sum(np.log(stats.beta.pdf(self.basicdata.g0_s[self.sampleIndx],
            self.params.g0_alpha, self.params.g0_beta))) + sumPTrapData_s
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
        # update if new parameters are better

#        print('###############  g0 update')
#        print('g0 and g0s', self.basicdata.g0, self.basicdata.g0_s)
###        print('pnow, pnew', pnow, pnew)
#        print('llik, llik_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))
#        print('sumPTrapData, sumPTrapData_s', sumPTrapData, sumPTrapData_s)
#        print('prior, prior_s', np.log(stats.beta.pdf(self.basicdata.g0[i],
#            self.params.g0_alpha, self.params.g0_beta)), np.log(stats.beta.pdf(self.basicdata.g0_s[i],
#            self.params.g0_alpha, self.params.g0_beta)))

        if rValue > zValue:
            self.basicdata.g0[self.sampleIndx] = self.basicdata.g0_s[self.sampleIndx]
            self.basicdata.g0All = self.basicdata.g0All_s.copy()
            self.predatordata.predatorPCapt = self.predatordata.predatorPCapt_s.copy()
            self.predatordata.predatorTrapPCapt = self.predatordata.predatorTrapPCapt_s.copy()
            self.pTrappingData = self.pTrappingData_s.copy()
#        if (i == 0):
#            sessMask = self.predatordata.predatorSession == 0
#            predPresSess = self.predatordata.predatorPres[sessMask]
#            pPCaptSess = self.predatordata.predatorPCapt[sessMask]
#            presOnly = predPresSess == 1                    #mask of pres only
#            print('r-v', rValue - zValue)
#            print('pPCaptSession', pPCaptSess[presOnly])


###########
################  End g0 update
#######


#######                 NEW 
###########         begin sigma update ##########
###########
    def propose_sigma(self):
        """
        propose new sigma values
        """
        # propose new values for next iteration
        self.basicdata.sigma_s = np.random.normal(self.basicdata.sigma, self.params.sigma_search_sd, size = None)
        self.basicdata.var2_s = 2.0 * (self.basicdata.sigma_s**2.0)
        self.basicdata.expTermMat_s =  np.exp(-(self.basicdata.distTrapToCell2) / self.basicdata.var2_s)

    def sigLLikFX(self, j, availTrapNights, predatorLocSession, predatorTrapIDSession,
        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
        predatorRemoveSession, remMask, g0Session, eTermMat):
        """
        get llik for g0 and g0_s
        """
#        presLoc = predatorLocSession[presOnlyMask]
#        (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#        eterm_s = eTermMat[:, uLoc]                 # exp term of unique locs
#        # pCapt with proposed sigma and g0 parameters (all of them at once)
#        pNoCaptAll_s = self.g0Sig_PPredatorCaptFX(availTrapNights, eterm_s, g0Session)
#        sortPNoCapt = np.sort(pNoCaptAll_s, axis=0)
#        pNoCapt_s = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
#        pNoCapt_s = pNoCaptAll_s.prod(axis = 0)                     # p not capt in all traps
#        pCapt_s = 1.0 - pNoCapt_s                                   # p Capt at unique locs in all traps
#        pCapt_s[pCapt_s > 0.97] = 0.97
#        pCaptLoc_s = pCapt_s[indxLoc]                               # feedback new pCapt to predator data
#        predatorPCaptLoc_sSession = predatorPCaptSession.copy()

        presLoc = predatorLocSession[presOnlyMask]
        l_presloc = len(presLoc)
        if l_presloc == 1:
            endPresID = presLoc + 1
            eterm_s = self.basicdata.expTermMat_s[:, presLoc : endPresID]
#            eterm = self.basicdata.expTermMat[:, presLoc : endPresID]
        else:
            eterm_s = self.basicdata.expTermMat_s[:, presLoc]
#            eterm = self.basicdata.expTermMat[:, presLoc]

        (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session)

        pCaptAll_s = 1 - np.prod(pNoCaptAll_s, axis = 1)                                   # p Capt at unique locs in all traps
        pCaptAllSumOne_s = pCaptAll_s / np.sum(pCaptAll_s)
        trapsessmask = self.basicdata.trapSession == j
        trapTrappedSession = self.basicdata.trapTrapped[trapsessmask]
        self.pTrappingData_s[j] = multinomial_pmf(pCaptAllSumOne_s, trapTrappedSession)

        predatorPCaptLoc_sSession = predatorPCaptSession.copy()
        predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
#        predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
        self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
        # the following gets info only for capt predators
        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
        matIndx = np.arange(self.basicdata.N[j])
        remIndx = matIndx[remPresMask]
        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
#        remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
#        remIndx = indxLoc[remPresMask]                              # locations only of captured predators
#        remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
#        predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only
        predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
        predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
        self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
###        if (j == 0):
#            print('eterm and g0 shp', eterm_s.shape, g0Session.shape)
#            print('predatorPCaptSession', predatorPCaptSession[presOnlyMask])
###            print('pCapt_s sig update', pCapt_s)
#            print('predatorRemoveSession', predatorRemoveSession[presOnlyMask])
        # binomial LL for current sigma 
        self.basicdata.llikg0Sig[j] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
            predatorRemoveSession[presOnlyMask])
        # binomial LL for proposed sigma_s
        self.basicdata.llikg0Sig_s[j] = self.g0Sig_ProbsFX(pCapt_s,
            predatorRemoveSession[presOnlyMask])

#        #############
#        # multinomial prob of traps catching preds that they caught or did not catch.
#        self.getPTrappingData(j, availTrapNights, eterm, g0Session, pNoCaptAll_s)
#        #############



    def sigUpdateFX(self):
        """
        update sigma in mcmc function
        """
        self.propose_sigma()
            # loop through sessions
        for j in range(self.basicdata.nsession):
            sessionMask = self.predatordata.predatorSession == j
            predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
            predatorPresSession = self.predatordata.predatorPres[sessionMask]
            predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
            presOnlyMask = predatorPresSession == 1                    #mask of pres only
            predatorLocSession = self.predatordata.predatorLoc[sessionMask]
            remMask = predatorRemoveSession == 1               # mask only predators removed
            predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
            predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
            # mask for trap data (nsession * ntrap)
            trapSessionMask = self.basicdata.trapSession == j
            # g0 for session j
            g0Session = self.basicdata.g0All[trapSessionMask]
#            g0Session_s = self.basicdata.g0All_s[trapSessionMask]
            # avail trap nights for session i
#            if j == 0:
#                print('g0 shape', g0Session.shape)
#                print('g0Sess sig Diff', np.sum(self.g0Sess_g0 - g0Session))

#            availTrapNights = np.expand_dims(self.basicdata.trapNightsAvail[trapSessionMask], 1)
            availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]
            ######## get llik for g0 and g0_s
            self.sigLLikFX(j, availTrapNights, predatorLocSession, predatorTrapIDSession,
                        predatorPCaptSession, predatorTrapPCaptSession, sessionMask, presOnlyMask,
                        predatorRemoveSession, remMask, g0Session, self.basicdata.expTermMat_s)
        self.sigma_PnowPnew()

    def sigma_PnowPnew(self):
        """
        calc pnow and pnew for sigma and update
        """        
        sumPTrapData = np.sum(self.pTrappingData)
        sumPTrapData_s = np.sum(self.pTrappingData_s)
        sigmaPriorNow = stats.norm.logpdf(self.basicdata.sigma, self.params.sigma_mean, self.params.sigma_sd)
        sigmaPriorNew = stats.norm.logpdf(self.basicdata.sigma_s, self.params.sigma_mean, self.params.sigma_sd)       
        pnow = np.sum(self.basicdata.llikg0Sig) + sigmaPriorNow + sumPTrapData
        pnew = np.sum(self.basicdata.llikg0Sig_s) + sigmaPriorNew + sumPTrapData_s
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)

#        print('pnow and new', pnow, pnew)
#        print('ll and ll_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))


        # update if new parameters are better
        if rValue > zValue:
            self.basicdata.sigma = self.basicdata.sigma_s
            self.predatordata.predatorPCapt = self.predatordata.predatorPCapt_s.copy()
            self.predatordata.predatorTrapPCapt = self.predatordata.predatorTrapPCapt_s.copy()
            self.basicdata.expTermMat = self.basicdata.expTermMat_s.copy()    
            self.pTrappingData = self.pTrappingData_s.copy()


#        print('Sigma###############')
#        print('LL and LL_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))
#        print('ptrap and ptrap_s', sumPTrapData, sumPTrapData_s)


###
################  End sigma update



################  Begin function to update g0Multiplier
#######

    def proposeg0Multi(self):
        """
        propose new parameters for g0Multiplier for updating
        """
        # proposed values
        self.basicdata.g0Multiplier_s[1:] = np.random.normal(self.basicdata.g0Multiplier[1:], .08)

        # make array length nSession of multiplier by season
        self.basicdata.g0MultiplierAll_s = self.basicdata.g0Multiplier_s[self.basicdata.g0_Indx_Multiplier]        
        self.basicdata.g0All_s = g0AllFX(self.basicdata.g0, self.basicdata.g0All_s, self.basicdata.trapBaitIndx, 
                    self.basicdata.nsession, self.basicdata.trapSession, self.basicdata.g0MultiplierAll_s)

    def g0MultiplierLLikFX(self):
        """
        get llik for g0Multiplier parameters
        """
        self.proposeg0Multi()
        for i in range(self.basicdata.nsession):
            sessionMask = self.predatordata.predatorSession == i
            predatorRemoveSession = self.predatordata.predatorRemove[sessionMask]
            remMask = predatorRemoveSession == 1
            predatorPresSession = self.predatordata.predatorPres[sessionMask]
            predatorPCaptSession = self.predatordata.predatorPCapt[sessionMask]
            predatorLocSession = self.predatordata.predatorLoc[sessionMask]
            predatorTrapIDSession = self.predatordata.predatorTrapID[sessionMask]
            predatorTrapPCaptSession = self.predatordata.predatorTrapPCapt[sessionMask]
            presOnlyMask = predatorPresSession == 1                    #mask of pres only
            # mask for trap data (nsession * ntrap)
            trapSessionMask = self.basicdata.trapSession == i
            # g0 for session i
            g0Session_s = self.basicdata.g0All_s[trapSessionMask]
            # avail trap nights for session i
#            availTrapNights = np.expand_dims(self.basicdata.trapNightsAvail[trapSessionMask], 1)
            availTrapNights = self.basicdata.trapNightsAvail_2D[trapSessionMask]

            presLoc = predatorLocSession[presOnlyMask]
            l_presloc = len(presLoc)
            if l_presloc == 1:
                endPresID = presLoc + 1
                eterm_s = self.basicdata.expTermMat[:, presLoc : endPresID]
            else:
                eterm_s = self.basicdata.expTermMat[:, presLoc]

            (pNoCaptAll_s, pCapt_s) = self.PCapt_g0SigFX(availTrapNights, eterm_s, g0Session_s)

            pCaptAll_s = 1 - np.prod(pNoCaptAll_s, axis = 1)                                   # p Capt at unique locs in all traps
            pCaptAllSumOne_s = pCaptAll_s / np.sum(pCaptAll_s)
            trapsessmask = self.basicdata.trapSession == i
            trapTrappedSession = self.basicdata.trapTrapped[trapsessmask]
            self.pTrappingData_s[i] = multinomial_pmf(pCaptAllSumOne_s, trapTrappedSession)


            # pCapt with proposed sigma and g0 parameters (all of them at once)
            predatorPCaptLoc_sSession = predatorPCaptSession.copy()
            predatorPCaptLoc_sSession[presOnlyMask] = pCapt_s
#           predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
            self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
            # the following gets info only for capt predators
            remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
            matIndx = np.arange(self.basicdata.N[i])
            remIndx = matIndx[remPresMask]
            remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
            predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only


#            presLoc = predatorLocSession[presOnlyMask]
#            (uLoc, indxLoc) = np.unique(presLoc, return_inverse = True) #unique Loc so limit matrix computation
                                                                    #get indices to feedback pCapt to predator data
#            eterm = self.basicdata.expTermMat[:, uLoc]                 # exp term of unique locs
            # pCapt with proposed sigma and g0 parameters (all of them at once)
#            pNoCaptAll_s = self.g0Sig_PPredatorCaptFX(availTrapNights, eterm, g0Session_s)
#            sortPNoCapt = np.sort(pNoCaptAll_s, axis=0)
#            pNoCapt_s = np.prod(sortPNoCapt[:self.params.nTopTraps], axis = 0)
#            pCapt_s = 1.0 - pNoCapt_s                                   # p Capt at unique locs in all traps
#            pCapt_s[pCapt_s > 0.97] = 0.97

#            pCaptLoc_s = pCapt_s[indxLoc]                               # feedback new pCapt to predator data
#            predatorPCaptLoc_sSession = predatorPCaptSession.copy()
#            predatorPCaptLoc_sSession[presOnlyMask] = pCaptLoc_s
#            self.predatordata.predatorPCapt_s[sessionMask] = predatorPCaptLoc_sSession #keep this in case keep new g0 and sig
#            # the following gets info only for capt predators
#            remPresMask = predatorRemoveSession[presOnlyMask] == 1         # mask of removed predators in sess i
#            remIndx = indxLoc[remPresMask]                              # locations only of captured predators
#            remtid = predatorTrapIDSession[remMask]                        # trap id of captured predators
#            predatorTrapPNoCapt_s = pNoCaptAll_s[remtid, remIndx]          # p No Capt of captured predators only

            predatorTrapPCapt_sSession = predatorTrapPCaptSession.copy()      # template
            predatorTrapPCapt_sSession[remMask] = 1.0 - predatorTrapPNoCapt_s # p capt fill in template
            self.predatordata.predatorTrapPCapt_s[sessionMask] = predatorTrapPCapt_sSession # fill in class data in case keep new params.
            # binomial LL for current g0 and sigma
            self.basicdata.llikg0Sig[i] = self.g0Sig_ProbsFX(predatorPCaptSession[presOnlyMask],
                predatorRemoveSession[presOnlyMask])
            # binomial LL for proposed g0_s and sigma_s
            self.basicdata.llikg0Sig_s[i] = self.g0Sig_ProbsFX(pCapt_s,
                predatorRemoveSession[presOnlyMask])
###            if i == 0:
###                print('predatorPCaptSession ###### g0Multi update', predatorPCaptSession[presOnlyMask])
###                print('pCaptLoc_s ###### g0Multi update', pCapt_s)
###                print('self.basicdata.g0All diff', np.sum(self.basicdata.g0All_s - self.basicdata.g0All))
#                print('LL and LL_s', self.basicdata.llikg0Sig[i], self.basicdata.llikg0Sig_s[i])
        
    def g0MultiplierUpdateFX(self):
        """
        update g0 in mcmc function
        """
        self.g0MultiplierLLikFX()
        sumPTrapData = np.sum(self.pTrappingData)
        sumPTrapData_s = np.sum(self.pTrappingData_s)
        priorNow = stats.norm.logpdf(self.basicdata.g0Multiplier[1:],self.params.g0MultiPrior[0],
                                    self.params.g0MultiPrior[1])
        priorNew = stats.norm.logpdf(self.basicdata.g0Multiplier_s[1:], self.params.g0MultiPrior[0],
                                    self.params.g0MultiPrior[1])
        pnow = np.sum(self.basicdata.llikg0Sig) + np.sum(priorNow) + sumPTrapData
        pnew = np.sum(self.basicdata.llikg0Sig_s) + np.sum(priorNew) + sumPTrapData_s
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
        # update if new parameters are better
        if rValue > zValue:
            self.basicdata.g0Multiplier = self.basicdata.g0Multiplier_s.copy()
            self.basicdata.g0MultiplierAll = self.basicdata.g0MultiplierAll_s.copy()            
            self.basicdata.g0All = self.basicdata.g0All_s.copy()
            self.predatordata.predatorPCapt = self.predatordata.predatorPCapt_s.copy()
            self.predatordata.predatorTrapPCapt = self.predatordata.predatorTrapPCapt_s.copy()

#        print('#######################   g0 multiplier')
#        print('LL and LL_s', np.sum(self.basicdata.llikg0Sig), np.sum(self.basicdata.llikg0Sig_s))
#        print('ptrap and ptrap_s', sumPTrapData, sumPTrapData_s)


#######
###########         begin r update ##########
#####

    def reproDataAllYears(self, growthrate):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        for i in self.basicdata.uYearIndx:
            self.basicdata.totalRecruits_s[i] = (self.basicdata.reproPop[i] * growthrate) + self.basicdata.ig
            relwc = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == i]
            sr = (self.basicdata.totalRecruits_s[i] * relwc)
            self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
#            # if before last recruit period (12/2014)
#            if i < self.basicdata.maxUYearIndx:
#                sr = (self.basicdata.totalRecruits_s[i] * relwc)
#                self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
#            # add this bit on to accomodate 12/2014
#            else:
#                relwc2015 = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == (i - 1)]
#                sr = (self.basicdata.totalRecruits_s[i] * relwc2015)
#                self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr[:2]
            

    def NpredAllMCMC(self, recruits):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.basicdata.nsession)
        Npred[0] = self.basicdata.reproPop[0] + recruits[0]
        Npred[1:] = self.basicdata.N[:-1] - self.basicdata.removeDat[:-1]
        Npred[1:] = Npred[1:] + recruits[1:]
        Npred[Npred < 0] = .1
        return (Npred)

    def rLLikFX(self):
        """
        get llik for population growth parameter and proposed
        """
        # get recruits for each session with proposed rs
        self.reproDataAllYears(self.basicdata.rs)
        # get Npred_s
        self.basicdata.Npred_s = self.NpredAllMCMC(self.basicdata.sessionRecruits_s)
        self.basicdata.llikR = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred)))
        self.basicdata.llikR_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s)))
#        print('llikR and llikR_s',self.basicdata.rg, self.basicdata.rs, self.basicdata.llikR, self.basicdata.llikR_s)
#        print('r prior',  np.log(gamma_pdf(self.basicdata.rg,
#            self.params.r_shape, (1.0 / self.params.r_scale))))
#        print('rs prior',  np.log(gamma_pdf(self.basicdata.rs,
#            self.params.r_shape, (1.0 / self.params.r_scale))))
#        print('Npred', self.basicdata.Npred[22:28])     
#        print('Npred_s', self.basicdata.Npred_s[22:28])

    def rUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.rLLikFX()
        pnow = self.basicdata.llikR + np.log(gamma_pdf(self.basicdata.rg,
            self.params.r_shape, (self.params.r_scale)))
        pnew = self.basicdata.llikR_s + np.log(gamma_pdf(self.basicdata.rs,
            self.params.r_shape, (self.params.r_scale)))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.rg = self.basicdata.rs
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
            self.basicdata.totalRecruits = self.basicdata.totalRecruits_s.copy()
        self.basicdata.rs = np.exp(np.random.normal(np.log(self.basicdata.rg), 0.5, size = None))

######
################### End update for rg
#####

######
################### Update for rpara
###############     wrapped cauchy parameters for distributing recruits
#####

    def getSessRecWrpCauchy(self):
        """
        get number of recruits across years and session
        with specified growth parameter
        """
        for i in self.basicdata.uYearIndx:
            relwc = self.basicdata.relWrpCauchy_s[self.basicdata.yearRecruitIndx == i]
            sr = self.basicdata.totalRecruits[i] * relwc
            self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
#            # if before last recruit period (12/2014)
#            if i < self.basicdata.maxUYearIndx:
#                relwc = self.basicdata.relWrpCauchy_s[self.basicdata.yearRecruitIndx == i]
#                sr = (self.basicdata.totalRecruits_s[i] * relwc)
#                self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
#            # add this bit on to accomodate 12/2014
#            else:
#                relwc2015 = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == (i - 1)]
#                sr = (self.basicdata.totalRecruits_s[i] * relwc2015)
#                self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr[:2]


    def wrpCauchyLLikFX(self):
        """
        get llik for population growth parameter and proposed
        """
        # get recruits for each session with proposed rs
        self.getSessRecWrpCauchy()
        # get Npred_s
        self.basicdata.Npred_s = self.NpredAllMCMC(self.basicdata.sessionRecruits_s)
        self.basicdata.llikWrpC = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred)))
        self.basicdata.llikWrpC_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s)))


    def proposeNewRpara(self):
        """
        propose new rpara values for next iteration
        """
        self.basicdata.rpara_s[0] = np.random.normal(self.basicdata.rpara[0], 0.01, size = None)
        self.basicdata.rpara_s[1] = np.exp(np.random.normal(np.log(self.basicdata.rpara[1]), 0.01, size = None))

#        self.basicdata.recruitWindowMask_s = getRecruitPeriodMask(self.basicdata.rpara_s[0], self.params.recruitHalfWindow,
#                                    self.basicdata.daypi, self.basicdata.daypiNeg)
        self.basicdata.relWrpCauchy_s = calcRelWrapCauchy(self.basicdata.rpara_s, self.basicdata.nsession, 
                           self.basicdata.uYearIndx, self.basicdata.daypi, 
                           self.basicdata.yearRecruitIndx)                  #, self.basicdata.recruitWindowMask_s)

    def rparaUpdateFX(self):
        """
        update rg in mcmc function
        """
        self.wrpCauchyLLikFX()
        pnow = (self.basicdata.llikWrpC + np.log(gamma_pdf(self.basicdata.rpara[1],
            self.params.rho_wrpc_Priors[0], (self.params.rho_wrpc_Priors[1]))) +
            np.log(stats.norm.pdf(self.basicdata.rpara[0], self.params.mu_wrpc_Priors[0], self.params.mu_wrpc_Priors[1])))
        pnew = (self.basicdata.llikWrpC_s + np.log(gamma_pdf(self.basicdata.rpara_s[1],
            self.params.rho_wrpc_Priors[0], (self.params.rho_wrpc_Priors[1]))) +
            np.log(stats.norm.pdf(self.basicdata.rpara_s[0], self.params.mu_wrpc_Priors[0], self.params.mu_wrpc_Priors[1])))
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
#        print('self.basicdata.llikWrpC and _s', self.basicdata.llikWrpC, self.basicdata.llikWrpC_s)
#        print('pnow and pnew', pnow, pnew)
#        print('recruit diff', self.basicdata.sessionRecruits - self.basicdata.sessionRecruits_s)

        if rValue > zValue:
            self.basicdata.rpara = self.basicdata.rpara_s.copy()
            self.basicdata.relWrpCauchy = self.basicdata.relWrpCauchy_s.copy()
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.sessionRecruits = self.basicdata.sessionRecruits_s.copy()
#            self.basicdata.recruitWindowMask = self.basicdata.recruitWindowMask_s.copy()
        self.proposeNewRpara()

#######
##############      Begin initial Repro pop 
#######

    def proposeRecruitYr0(self):
        """
        get number of recruits across all sessions in year 0
        with proposed intial repro pop
        """
#        for i in self.basicdata.uYearIndx:
        rp = np.random.normal(self.basicdata.reproPop[0], 7.0, size = None)
        if rp < self.params.IRR_priors[0]:
            rp = self.params.IRR_priors[0] + 5
        if rp > self.params.IRR_priors[1]:
            rp = self.params.IRR_priors[1] - 5
        self.basicdata.reproPop_s[0] = rp
        self.basicdata.totalRecruits_s[0] = (self.basicdata.reproPop_s[0] * self.basicdata.rg) + self.basicdata.ig
        relwc = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == 0]
        sr = self.basicdata.totalRecruits_s[0] * relwc
        self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == 0] = sr

    def NpredYear0(self):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.basicdata.nSessYr0)
        Npred[0] = self.basicdata.reproPop_s[0] + self.basicdata.sessionRecruits_s[0]
        Npred[1:] = (self.basicdata.N[: self.basicdata.nSessYr0 - 1] - 
                    self.basicdata.removeDat[: self.basicdata.nSessYr0 - 1])
        Npred[1:] = (Npred[1:] + self.basicdata.sessionRecruits_s[1 : self.basicdata.nSessYr0])
        Npred[Npred < 0] = 0
        self.basicdata.Npred_s[: self.basicdata.nSessYr0] = Npred

    def initialReproLLik(self):
        """
        get llik for initial repro pop and proposed
        """
        # get recruits for each session in year 0
        self.proposeRecruitYr0()
        # get Npred_s
        self.NpredYear0()
        llik = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[0 : self.basicdata.nSessYr0], 
                                self.basicdata.Npred[0 : self.basicdata.nSessYr0])))
        llik_s = np.sum(np.log(stats.poisson.pmf(self.basicdata.N[0 : self.basicdata.nSessYr0], 
                                self.basicdata.Npred_s[0 : self.basicdata.nSessYr0])))
        return(llik, llik_s)


    def initialReproUpdateFX(self):
        """
        update inital repro pop in mcmc function
        """
        (pnow, pnew) = self.initialReproLLik()
#        pnow = llik + stats.norm.logpdf(np.log(self.basicdata.reproPop[0]), self.params.IRR_priors[0],
#                        self.params.IRR_priors[1])
#        pnew = llik_s + stats.norm.logpdf(np.log(self.basicdata.reproPop_s[0]), self.params.IRR_priors[0],
#                        self.params.IRR_priors[1])
        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.reproPop[0] = self.basicdata.reproPop_s[0]
            self.basicdata.Npred[: self.basicdata.nSessYr0] = self.basicdata.Npred_s[: self.basicdata.nSessYr0]
            self.basicdata.sessionRecruits[: self.basicdata.nSessYr0] = self.basicdata.sessionRecruits_s[: self.basicdata.nSessYr0]
            self.basicdata.totalRecruits[0] = self.basicdata.totalRecruits_s[0]



#######
###########         begin I update ##########
#####
    def immLLikFX(self):
        """
        get llik for ig and is
        i in [0, nsession - 1]
        """
        self.basicdata.i_s = np.random.normal(self.basicdata.ig, 13.0, size = None)
        # make uniform prior within bounds
        if (self.basicdata.i_s > 300.0):
            self.basicdata.i_s = 280.0
        elif (self.basicdata.i_s < 20):
            self.basicdata.i_s = 30
        # get proposed total number of recruits per year, and sesion recruits
        self.RecruitsAllYears() 
        self.basicdata.Npred_s = self.NpredAllMCMC(self.basicdata.sessionRecruits_s)
#        print('n', self.basicdata.N) 
#        print('npred', self.basicdata.Npred_s)
#        print('sessrecru', self.basicdata.sessionRecruits_s)
#        print('totrecru', self.basicdata.totalRecruits_s)
        self.basicdata.llikImm = np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred))
        self.basicdata.llikImm_s = np.log(stats.poisson.pmf(self.basicdata.N, self.basicdata.Npred_s))

    def RecruitsAllYears(self):
        """
        Calculate total number of recruits for all years when updating 
        recruitment for reproduction parameter
        """
        for i in self.basicdata.uYearIndx[1:]:
            popi = self.basicdata.reproPop[i]
            totrec = (popi * self.basicdata.rg) + self.basicdata.i_s
            self.basicdata.totalRecruits_s[i]  = totrec
            relwc = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == i]
            sr = (self.basicdata.totalRecruits_s[i] * relwc)
            self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
        ####################################
#        # add in this to accomodate 12/2014
#            # if before last recruit period (12/2014)
#            if i < self.basicdata.maxUYearIndx:
#                relwc = self.basicdata.relWrpCauchy_s[self.basicdata.yearRecruitIndx == i]
#                sr = (self.basicdata.totalRecruits_s[i] * relwc)
#                self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr
#            else:
#                relwc2015 = self.basicdata.relWrpCauchy[self.basicdata.yearRecruitIndx == (i - 1)]
#                sr = (self.basicdata.totalRecruits_s[i] * relwc2015)
#                self.basicdata.sessionRecruits_s[self.basicdata.yearRecruitIndx == i] = sr[:2]

    def immUpdateFX(self):
        """
        update ig in mcmc function
        """
        self.immLLikFX()
        pnow = np.sum(self.basicdata.llikImm) 
            # + stats.norm.logpdf(self.basicdata.ig,
            # self.params.imm_mean, self.params.imm_sd)
        pnew = np.sum(self.basicdata.llikImm_s) 
            # + stats.norm.logpdf(self.basicdata.i_s,
            # self.params.imm_mean, self.params.imm_sd)

        rValue = np.exp(pnew - pnow)        # calc importance ratio
        zValue = np.random.uniform(0.0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.ig = self.basicdata.i_s
            self.basicdata.Npred = self.basicdata.Npred_s.copy()
            self.basicdata.totalRecruits_s
            self.basicdata.sessionRecruits_s

########            Main mcmc function
########
    def mcmcFX(self):
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

            self.N_predatordata_updateFX()

            self.betaUpdateFX()

            self.g0UpdateFX()

            self.sigUpdateFX()

            self.g0MultiplierUpdateFX()

            self.rUpdateFX()

            self.rparaUpdateFX()

            self.initialReproUpdateFX()

            self.immUpdateFX()

            if g in self.params.keepseq:
                self.Ngibbs[cc] = self.basicdata.N
                self.bgibbs[cc] = self.basicdata.b
                self.rgibbs[cc] = self.basicdata.rg
                self.igibbs[cc] = self.basicdata.ig
                self.g0gibbs[cc] = self.basicdata.g0.T
                self.siggibbs[cc] = self.basicdata.sigma
                self.rparagibbs[cc] = self.basicdata.rpara                         # wrapped cauchy para for recruitment
                self.g0Multigibbs[cc] = self.basicdata.g0Multiplier                       # seasonal g0 multiplier
                self.initialRepPopgibbs[cc] = self.basicdata.reproPop[0]

                cc = cc + 1

        return (self.bgibbs, self.Ngibbs, self.rgibbs, self.g0gibbs, self.igibbs,
                self.siggibbs, self.rparagibbs, self.g0Multigibbs, self.initialRepPopgibbs)


########            Pickle results to directory
########

class Gibbs(object):
    def __init__(self, mcmcobj, basicdata):
        self.Ngibbs = mcmcobj.Ngibbs
        self.bgibbs = mcmcobj.bgibbs
        self.rgibbs = mcmcobj.rgibbs
        self.igibbs = mcmcobj.igibbs
        self.g0gibbs = mcmcobj.g0gibbs
        self.siggibbs = mcmcobj.siggibbs
        self.rparagibbs = mcmcobj.rparagibbs
        self.g0Multigibbs = mcmcobj.g0Multigibbs
        self.initialRepPopgibbs = mcmcobj.initialRepPopgibbs

        self.nsession = basicdata.nsession
        self.removeDat = basicdata.removeDat
        self.sessionYear = basicdata.sessionYear
        self.sessionMonth = basicdata.sessionMonth
        self.sessionDay = basicdata.sessionDay
        self.trapSession = basicdata.trapSession
        self.trapNightsAvail_2D = basicdata.trapNightsAvail_2D
        self.startJulSess = basicdata.startJulSess
        self.fortnightMask = basicdata.fortnightMask
        self.sessionIntervalMask = basicdata.sessionIntervalMask
        # for calc npred 
        self.uYearIndx = basicdata.uYearIndx
        self.reproPeriodIndx = basicdata.reproPeriodIndx
        self.nYear = basicdata.nYear
        self.daypi = basicdata.daypi
        self.yearRecruitIndx = basicdata.yearRecruitIndx


class PredatorTrapData(object):
    def __init__(self, rawdata):
        """
        Pickle structure from manipCats.py 
        """
        # data associated with capture data
        self.week = rawdata.week
        self.captTrapID = rawdata.captTrapID
        self.julianStart = rawdata.julianStart
        self.julianYear = rawdata.julianYear
        self.ttypebait = rawdata.ttypebait
        self.ttype = rawdata.ttype
        self.cattrap = rawdata.cattrap
        self.ferretTrap = rawdata.ferretTrap
        self.hedgehogTrap = rawdata.hedgehogTrap
        self.stoweaTrap = rawdata.stoweaTrap
        self.nDays = rawdata.nDays
        self.year = rawdata.year
        self.fortnightMask = rawdata.fortnightMask
        self.sessionIntervalMask = rawdata.sessionIntervalMask
        # data associated with trap location data
        self.trapX = rawdata.trapX
        self.trapY = rawdata.trapY
        self.trapTrapid = rawdata.trapTrapid
        self.trapBaitIndx = rawdata.trapBaitIndx
        self.nTraps = rawdata.nTraps 
        self.sessionYear = rawdata.sessionYear
        self.sessionMonth = rawdata.sessionMonth
        self.sessionDay = rawdata.sessionDay

########            Main function
#######
def main(params, basicfile=None, predfile=None):

    predatorpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    #predatorDatFile = os.path.join(predatorpath,'catCapt4.csv')
    covDatFile = os.path.join(predatorpath,'covDat.csv')
    #trapDatFile = os.path.join(predatorpath,'trapDat3.csv')

    # initiate basicdata class and object when do not read in previous results
    if basicfile is None:
        # read in the pickled capt and trap data from 'manipCats.py'
        predatorTrapFile = os.path.join(predatorpath,'out_manipdata2.pkl')  
        fileobj = open(predatorTrapFile, 'rb')
        predatortrapdata = pickle.load(fileobj)
        fileobj.close()
        # initiate basicdata from script
        basicdata = BasicData(predatortrapdata, covDatFile, params)
    # pickled basicdata specified in system variable to be imported
    else:
        # read in pickled results (basicdata) from a previous run
        inputBasicdata = os.path.join(predatorpath, basicfile)
        fileobj = open(inputBasicdata, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()

    # Exploratory function only     ##########
#    getTrapSummary(basicdata.trapNightsAvail, basicdata.trapTrapped, basicdata.trapBaitIndx, basicdata.nsession)
    ##########################################


    if predfile is None:
        # initiate predatordata class
        predatordata = PredatorData(basicdata, params)
    # read in pickled predatordata class
    else:
        inputPreddata = os.path.join(predatorpath, predfile)
        fileobj = open(inputPreddata, 'rb')
        predatordata = pickle.load(fileobj)
        fileobj.close()

    # initiate mcmc class and object
    mcmcobj = MCMC(params, predatordata, basicdata)

    # run mcmcFX - gibbs loop
    mcmcobj.mcmcFX()

    gibbsobj = Gibbs(mcmcobj, basicdata)

    # pickle basic data from present run to be used to initiate new runs
    outBasicdata = os.path.join(predatorpath,'out_basicdata.pkl')
    fileobj = open(outBasicdata, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    # pickle predator data from present run to be used to initiate new runs
    outPredatordata = os.path.join(predatorpath,'out_predatordata.pkl')
    fileobj = open(outPredatordata, 'wb')
    pickle.dump(predatordata, fileobj)
    fileobj.close()


    # pickle mcmc results for post processing in gibbsProcessing.py
    outGibbs = os.path.join(predatorpath,'out_gibbs.pkl')
    fileobj = open(outGibbs, 'wb')
    pickle.dump(gibbsobj, fileobj)
    fileobj.close()

if __name__ == '__main__':
    main(params)
